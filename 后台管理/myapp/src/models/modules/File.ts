
import {  fileList } from "@/services"
import { Fileitem } from "@/types"
import { makeAutoObservable,runInAction } from "mobx"

class File{
    FileItemList:Fileitem[]=[]
    getcountNumber:number = 0
  constructor() {
      makeAutoObservable(this)
  }
  //文件管理
  async getfileList(page=1,params: { [key: string]: string | boolean }){
    let result=await fileList(page,params)
    if(result.data){
        runInAction(()=>{
          this.getcountNumber=result.data[1]
           this.FileItemList= result.data[0] 
        })
    }
  } 
 
 
}
export default File