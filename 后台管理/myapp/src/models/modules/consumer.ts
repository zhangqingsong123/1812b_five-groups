import {getconsumerlist,updateconsumer} from '@/services';
import {ConsumerList} from '@/types';
import { makeAutoObservable, runInAction } from "mobx"
import {message} from 'antd';

class Consumer{
    // 用户管理的 表格内容
    consumerList: ConsumerList[] = [];
    consumerCount: number = 0;
    constructor(){
        makeAutoObservable(this);
    }
    // 用户管理的 表格内容
    async getconsumerlist(page=1, params: {[key:string]:string|boolean},pageSize:number){
        let result = await getconsumerlist(page,params,pageSize);
        if (result.data){
            runInAction(()=>{
                this.consumerList = result.data[0];
                this.consumerCount = result.data[1];
            })
        }
    }
    // 用户管理 更改状态
    async updateconsumer(data:ConsumerList,page:number, params: {[key:string]:string|boolean},pageSize:number){
        let res=await updateconsumer(data);
        if(res){
            message.success('操作成功');
            this.getconsumerlist(page,params,pageSize);
        }
    }
}
export default Consumer;
