import { PageData } from '@/types';
import { makeAutoObservable, runInAction } from 'mobx';
import { message } from 'antd';
import { getPage, deletePage, updatePage } from '@/services/modules/Page';

class Page {
  pageList: PageData[] = [];
  pageCount: number = 0;
  constructor() {
    makeAutoObservable(this);
  }

  // 分页获取所有页面
  async getPage(
    page: number,
    pageSize: number,
    params: { [key: string]: string | boolean },
  ) {
    let result = await getPage(page, params, pageSize);
    if (result.data) {
      runInAction(() => {
        this.pageList = result.data[0];
        this.pageCount = result.data[1];
      });
    }
  }


  // 批量更改页面状态
  async updatePage(
    data: PageData[],
    params: { [key: string]: string | boolean },
    pageSize: number,
    page: number,
  ) {
    message.loading('操作中', 0);
    Promise.all(data.map((item) => updatePage(item)))
      .then((res) => {
        message.destroy();
        message.success('操作成功');
        this.getPage(page, pageSize, params);
      })
      .catch((err) => {
        message.destroy();
        message.error('操作失败');
      });
  }

  // 批量更改页面状态
  async updatePage(
    ids: string[],
    params: { [key: string]: string | boolean },
    pageSize: number,
    page: number,
  ) {
    message.loading('操作中', 0);
    Promise.all(ids.map((id) => updatePage(id)))
      .then((res) => {
        message.destroy();
        message.success('操作成功');
        this.getPage(page, pageSize, params);
      })
      .catch((err) => {
        message.destroy();
        message.error('操作失败');
      });
  }

  // 批量删除页面
  async deletePage(
    ids: string[],
    params: { [key: string]: string | boolean },
    pageSize: number,
    page: number,
  ) {
    message.loading('操作中', 0);
    Promise.all(ids.map((id) => deletePage(id)))
      .then((res) => {
        message.destroy();
        message.success('删除成功');
        this.getPage(page, pageSize, params);
      })
      .catch((err) => {
        message.destroy();
        message.error('删除失败');
      });
  }
}

export default Page;
