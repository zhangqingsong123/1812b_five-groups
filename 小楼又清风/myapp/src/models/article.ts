import { ArticleDetail, ArticlehostCommet, ArticleItem,  BannerItem, GetArticleTag, IRootState, TagItem } from '@/types';
import { Effect, ImmerReducer, Reducer, Subscription } from 'umi';
import {  getArticleCommet,   getArticleList,  getArticleTag, getArticleTagList, getcoverbanner, getRecommend } from '@/services'
export interface ArticleModelState {
  recommend: ArticleItem[];
  articleList: ArticleItem[];
  articleCount: number;
  coverbanner: BannerItem[];
  articleComment: ArticlehostCommet[];
  atricleCommentcount: number;
  articleDetail: Partial<ArticleDetail>

  ArticleTag: TagItem[];
  ArticleTagList: GetArticleTag[];
  page: number;
  pageSize: number,
  status: string
}

export interface ArticleModelType {
  namespace: 'article';
  state: ArticleModelState;
  //处理异步action
  effects: {
    getRecommend: Effect;
    getArticleList: Effect;
    getcoverbanner: Effect;
    getArticleCommet: Effect;
    getArticleTag: Effect;
    getArticleTagList: Effect;
  };
  //   同步action
  reducers: {
    save: Reducer<ArticleModelState>;
    // 启用 immer 之后
    // save: ImmerReducer<IndexModelState>;
  };
  //   subscriptions: { setup: Subscription };
}

const ArticleModel: ArticleModelType = {
  namespace: 'article',

  state: {
    recommend: [],
    articleList: [],
    articleCount: 0,
    coverbanner: [],
    articleComment: [], //评论接口
    atricleCommentcount: 0,
    articleDetail: {},
    ArticleTag: [],
    ArticleTagList: [],
    page: 1,
    pageSize: 12,
    status: "publish"
  },

  effects: {
    *getRecommend({ payload }, { call, put }) {
      let result = yield call(getRecommend)
      if (result.success === true) {
        yield put({
          type: "save",
          payload: { recommend: result.data }
        })
      }
    },
    *getArticleList({ payload }, { put, call }) {
      let result = yield call(getArticleList, payload)

      if (result.success === true) {
        yield put({
          type: "save",
          payload: {
            articleList: result.data[0],
            articleCount: result.data[1]
          }
        })
      }
    },
    *getcoverbanner({ payload }, { put, call }) {
      let res = yield call(getcoverbanner)
      if (res.success == true) {
        yield put({
          type: "save",
          payload: {
            coverbanner: res.data
          }
        })
      }
    },
    //详情
  
    //获取评论列表
    *getArticleCommet({ payload }, { put, call, select }) {
      let prearticleComment = yield select((state: IRootState) => state.article.articleComment)
      let result = yield call(getArticleCommet, payload.id, payload.page)

      if (result.success === true) {
        let articleComment = result.data[0]
        if (payload.page !== 1) {
          articleComment = [...prearticleComment, ...articleComment]
        }
        yield put({
          type: "save",
          payload: {
            articleComment,
            atricleCommentcount: result.data[1]
          }
        })
      }
    },
   
    *getArticleTag({ payload }, { call, put }) {
      let result = yield call(getArticleTag)
   

      if (result.statusCode == 200) {
        yield put({
          type: "save",
          payload: {
            ArticleTag: result.data
          }
        })
      }
    },
 
    *getArticleTagList({ payload }, { call, put }) {
      let result = yield call(getArticleTagList,payload)
   
      if (result.success === true) {
        yield put({
          type: "save",
          payload: {
            ArticleTagList: result.data[0]
          }
        })
      }

    }
  },
  reducers: {
    save(state, action) {
      return {
        ...state,
        ...action.payload,
      };
    },
    // 启用 immer 之后
    // save(state, action) {
    //   state.name = action.payload;
    // },
  },
};

export default ArticleModel;

function* getArticleDetai(getArticleDetai: any): any {
  throw new Error('Function not implemented.');
}
