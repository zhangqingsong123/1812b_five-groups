import React, { useEffect, useState } from 'react'
import { IRouteComponentProps, useDispatch, useSelector, RegardsModelState, NavLink } from 'umi'
import { fomatTime } from '@/utils';
import HighLight from '@/components/HighLight';
import RecomMended from '../../components/recommended/index';
import styles from './regards.less'
import ImageView from '@/components/ImageView';
import { RootState } from '@/types';
import {BackTop} from 'antd'
import Comment from '../../components/Comment'
import CommentList from '../../components/CommentList/CommentList'
import GiveLike from '../../components/GiveLike/index'

const Detail: React.FC<IRouteComponentProps<{ id: string, }>> = (props) => {
    console.log("....id", props.match.params.id);
    let [list] = useState([]);
    let dispatch = useDispatch()
    const pigeonhole = useSelector((state: RootState) => state.pigeonhole)
    let { regardsDetail } = useSelector((state: RegardsModelState) => state.regards)

    useEffect(() => {
        dispatch({
            type: "pigeonhole/getPigRecommend"
        })
    }, [])

    useEffect(() => {
        dispatch({
            type: "regards/getRegardsDetail",
            payload: props.match.params.id
        })
    }, [props.match.params.id])
    return (
        <div className={styles.regards}>
            <BackTop />
            <div className={styles.container}>
                <div className={styles.center}>
                    <GiveLike />
                    <div className={styles.left}>
                        <ImageView>
                            {regardsDetail.cover && <img src={regardsDetail.cover} alt="" />}
                            <h1 className={styles.DetailTitle}>{regardsDetail.title}</h1>
                            <p>发布于{fomatTime(regardsDetail.publishAt!)}·阅读量{regardsDetail.views}</p>
                            <HighLight>
                                <div dangerouslySetInnerHTML={{ __html: regardsDetail.html! }}></div>
                            </HighLight>
                        </ImageView>
                        <p className={styles._1hXJ8FkJwAyuM2}>评论</p>
                        <Comment id={props.match.params.id} url="/page/msgboard"/>
                    </div>
                    <div className={styles.right}>
                        <div className={styles.sticky}>
                            <RecomMended />
                            <div className={styles.below}>
                                <p>
                                    <b>目录</b>
                                </p>
                                <div className={styles.directory_title}>
                                    {
                                        list.map((item: any, index) => {
                                            return <div key={index}>
                                                {
                                                    item.level === "2" ? <div style={{ paddingLeft: "12px" }} className="level3box"><li>{item.text}</li> </div> : ""
                                                }
                                                {
                                                    item.level === "3" ?
                                                        <div className="level_left">
                                                            <div style={{ paddingLeft: "20px" }}>
                                                                <li className={styles.small}>{item.text}</li>
                                                            </div>
                                                        </div> : ""
                                                }
                                            </div>
                                        })
                                    }
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <strong className="site-back-top-basic"></strong>
        </div>
    )
}

export default Detail;