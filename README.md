# 张庆松
## 2021.09.12
1. 文章阅读
 - [es6](https://es6.ruanyifeng.com/)
 - [ hooks 系列四：useRefs ](https://segmentfault.com/a/1190000040402752?utm_source=sf-similar-article
2. LeetCode 刷题
    - [只出现一次的数字](https://leetcode-cn.com/problems/single-number/)
3. 源码阅读
- [vue 源码--vue-router 根据源码实现方式](https://www.jianshu.com/p/3baa76ebbce0)
4. 项目进度

## 2021.09.10
1. 文章阅读
 - [es6](https://es6.ruanyifeng.com/)
 - [ hooks 系列四：useRefs ](https://segmentfault.com/a/1190000040402752?utm_source=sf-similar-article
2. LeetCode 刷题
    - [存在重复元素](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x248f5/)
3. 源码阅读
- [vue 源码--vue-router 根据源码实现方式](https://www.jianshu.com/p/3baa76ebbce0)
4. 项目进度
   
## 2021.9.9

1. 文章阅读

- [Vue：知道什么时候使用计算属性并不能提高性能吗？](https://juejin.cn/post/7005336858049642527)

1. 源码阅读

- [原生实现一个 react-redux 的代码示例](https://www.jb51.net/article/141664.htm)

3. LeeCode 刷题

- [子集](https://leetcode-cn.com/leetbook/read/tencent/x5bzb3/)
- [全排列](https://leetcode-cn.com/leetbook/read/tencent/x5j6cm/)

4. 项目进度
   个人中心
   表格渲染
   表格分页
## 2021.09.08

1. 文章阅读

- [es6](https://link.juejin.cn/?target=https%3A%2F%2Fgithub.com%2Fmqyqingfeng%2FBlog%2Fissues%2F32)
- [ hooks 系列四：useRefs ](https://segmentfault.com/a/1190000040402752?utm_source=sf-similar-article

2. LeetCode 刷题

- [只出现一次的数字](https://leetcode-cn.com/problems/single-number/)

3. 源码阅读

- [封装自定义 Hooks](https://jasonandjay.github.io/study/zh/standard/Hooks.html#%E5%B0%81%E8%A3%85%E8%87%AA%E5%AE%9A%E4%B9%89hooks)

4. 项目进度
   个人中心
   表格渲染
   表格分页

## 2021.09.06

1. 文章阅读

- [es6](https://link.juejin.cn/?target=https%3A%2F%2Fgithub.com%2Fmqyqingfeng%2FBlog%2Fissues%2F32)
- [ hooks 系列四：useRefs ](https://segmentfault.com/a/1190000040402752?utm_source=sf-similar-article

2. LeetCode 刷题

- [只出现一次的数字](https://leetcode-cn.com/problems/single-number/)

3. 源码阅读

- [封装自定义 Hooks](https://jasonandjay.github.io/study/zh/standard/Hooks.html#%E5%B0%81%E8%A3%85%E8%87%AA%E5%AE%9A%E4%B9%89hooks)

4. 项目进度
   个人中心
   表格渲染
   表格分页

## 2021.09.03

1. 文章阅读

- [es6](https://es6.ruanyifeng.com/)
- [ hooks 系列四：useRefs ](https://segmentfault.com/a/1190000040402752?utm_source=sf-similar-article

2. LeetCode 刷题

- [只出现一次的数字](https://leetcode-cn.com/problems/single-number/)

3. 源码阅读

- [封装自定义 Hooks](https://jasonandjay.github.io/study/zh/standard/Hooks.html#%E5%B0%81%E8%A3%85%E8%87%AA%E5%AE%9A%E4%B9%89hooks)

4. 项目进度
   个人中心
   表格渲染
   表格分页

## 2021.09.02

1. 文章阅读

- [es6](https://es6.ruanyifeng.com/)
- [ hooks 系列四：useRefs ](https://segmentfault.com/a/1190000040402752?utm_source=sf-similar-article

2. LeetCode 刷题
   - [存在重复元素](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x248f5/)
3. 源码阅读

- [封装自定义 Hooks](https://jasonandjay.github.io/study/zh/standard/Hooks.html#%E5%B0%81%E8%A3%85%E8%87%AA%E5%AE%9A%E4%B9%89hooks)

4. 项目进度
   个人中心

## 2021.09.01

1. 文章阅读

- [es6](https://es6.ruanyifeng.com/)
- [ hooks 系列四：useRefs ](https://segmentfault.com/a/1190000040402752?utm_source=sf-similar-article

2. LeetCode 刷题
   - [存在重复元素](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x248f5/)
3. 源码阅读

- [封装自定义 Hooks](https://jasonandjay.github.io/study/zh/standard/Hooks.html#%E5%B0%81%E8%A3%85%E8%87%AA%E5%AE%9A%E4%B9%89hooks)

4. 项目进度
   个人中心

## 2021.08.31

1. 文章阅读

- [es6](https://es6.ruanyifeng.com/)
- [ hooks 系列四：useRefs ](https://segmentfault.com/a/1190000040402752?utm_source=sf-similar-article)

2. LeetCode 刷题

3. 源码阅读

- [封装自定义 Hooks](https://jasonandjay.github.io/study/zh/standard/Hooks.html#%E5%B0%81%E8%A3%85%E8%87%AA%E5%AE%9A%E4%B9%89hooks)

4. 项目进度
   注册页面
   退出登录
   表格渲染

## 2021.08.30

1. 文章阅读

- [es6](https://es6.ruanyifeng.com/)
- [ hooks 系列四：useRefs ](https://segmentfault.com/a/1190000040402752?utm_source=sf-similar-article)

2. LeetCode 刷题

3. 源码阅读

- [封装自定义 Hooks](https://jasonandjay.github.io/study/zh/standard/Hooks.html#%E5%B0%81%E8%A3%85%E8%87%AA%E5%AE%9A%E4%B9%89hooks)

4. 项目进度
   登录页面
   注册页面排版

## 2021.08.29

1. 文章阅读

- [es6](https://es6.ruanyifeng.com/)
  - [ hooks 系列四：useRefs ](https://segmentfault.com/a/1190000040402752?utm_source=sf-similar-article)

2. LeetCode 刷题

3. 源码阅读

- [封装自定义 Hooks](https://jasonandjay.github.io/study/zh/standard/Hooks.html#%E5%B0%81%E8%A3%85%E8%87%AA%E5%AE%9A%E4%B9%89hooks)

4. 项目进度
   搭建框架
   配置路由

## 2021.8.27

1. 文章阅读
   - [es6](https://es6.ruanyifeng.com/)
   - [ hooks 系列四：useRefs ](https://segmentfault.com/a/1190000040402752?utm_source=sf-similar-article)
2. 源码阅读
   - [封装自定义 Hooks](https://jasonandjay.github.io/study/zh/standard/Hooks.html#%E5%B0%81%E8%A3%85%E8%87%AA%E5%AE%9A%E4%B9%89hooks)
3. LeeCode 刷题

4. 项目进度

- [x] 关于页面布局
- [x] 搜索页面 模糊搜索
- [x] 回复功能
- [x] 发布评论

## 2021.8.26

1. 文章阅读
   - [es6](https://es6.ruanyifeng.com/)
2. 源码阅读
   - [封装自定义 Hooks](https://jasonandjay.github.io/study/zh/standard/Hooks.html#%E5%B0%81%E8%A3%85%E8%87%AA%E5%AE%9A%E4%B9%89hooks)
3. LeeCode 刷题
   - [存在重复元素](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x248f5/)
4. 项目进度

- [x] 关于页面布局
- [x] 搜索页面 模糊搜索
- [x] 回复功能
- [x] 发布评论

## 2021.8.25

1. 文章阅读
   - [es6](https://es6.ruanyifeng.com/)
2. 源码阅读
   - [封装自定义 Hooks](https://jasonandjay.github.io/study/zh/standard/Hooks.html#%E5%B0%81%E8%A3%85%E8%87%AA%E5%AE%9A%E4%B9%89hooks)
3. LeeCode 刷题

4. 项目进度

- [x] 关于页面布局
- [x] 搜索页面 模糊搜索
- [x] 回复功能
- [x] 发布评论

## 2021.08.29

1. 文章阅读

- [es6](https://es6.ruanyifeng.com/)
  - [ hooks 系列四：useRefs ](https://segmentfault.com/a/1190000040402752?utm_source=sf-similar-article)

2. LeetCode 刷题

3. 源码阅读

- [封装自定义 Hooks](https://jasonandjay.github.io/study/zh/standard/Hooks.html#%E5%B0%81%E8%A3%85%E8%87%AA%E5%AE%9A%E4%B9%89hooks)

4. 项目进度
   搭建框架
   配置路由

## 2021.8.27

1. 文章阅读
   - [es6](https://es6.ruanyifeng.com/)
   - [ hooks 系列四：useRefs ](https://segmentfault.com/a/1190000040402752?utm_source=sf-similar-article)
2. 源码阅读
   - [封装自定义 Hooks](https://jasonandjay.github.io/study/zh/standard/Hooks.html#%E5%B0%81%E8%A3%85%E8%87%AA%E5%AE%9A%E4%B9%89hooks)
3. LeeCode 刷题

4. 项目进度

- [x] 关于页面布局
- [x] 搜索页面 模糊搜索
- [x] 回复功能
- [x] 发布评论

## 2021.8.26

1. 文章阅读
   - [es6](https://es6.ruanyifeng.com/)
2. 源码阅读
   - [封装自定义 Hooks](https://jasonandjay.github.io/study/zh/standard/Hooks.html#%E5%B0%81%E8%A3%85%E8%87%AA%E5%AE%9A%E4%B9%89hooks)
3. LeeCode 刷题
   - [存在重复元素](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x248f5/)
4. 项目进度

- [x] 关于页面布局
- [x] 搜索页面 模糊搜索
- [x] 回复功能
- [x] 发布评论

## 2021.8.25

1. 文章阅读
   - [es6](https://es6.ruanyifeng.com/)
2. 源码阅读
   - [封装自定义 Hooks](https://jasonandjay.github.io/study/zh/standard/Hooks.html#%E5%B0%81%E8%A3%85%E8%87%AA%E5%AE%9A%E4%B9%89hooks)
3. LeeCode 刷题

4. 项目进度

- [x] 关于页面布局
- [x] 搜索页面 模糊搜索
- [x] 回复功能
- [x] 发布评论

## 2021.8.24

1. 文章阅读

- [es6](https://es6.ruanyifeng.com/)

2. 源码阅读

- [vue 源码--vue-router 根据源码实现方式](https://www.jianshu.com/p/3baa76ebbce0)

3. LeeCode 刷题

4. 项目进度

- [x] 关于页面布局
- [x] 搜索页面 模糊搜索
- [x] 回复功能
- [x] 发布评论

## 2021.8.23

## 2021.8.24

1. 文章阅读

- [什么是 hooks](https://jasonandjay.github.io/study/zh/standard/Hooks.html#%E4%BB%80%E4%B9%88%E6%98%AFhooks)

2. 源码阅读

- [vue 源码--vue-router 根据源码实现方式](https://www.jianshu.com/p/3baa76ebbce0)

3. LeeCode 刷题

4. 项目进度

- [x] 关于页面布局
- [x] 搜索页面 模糊搜索
- [x] 回复功能
- [x] 发布评论

## 2021.8.22

1. 文章阅读
   - [响应式布局](https://blog.csdn.net/qfguan/article/details/109364290)
2. 源码阅读

- [Vue 中的虚拟 DOM](https://vue-js.com/learn-vue/virtualDOM/#_3-vue%E4%B8%AD%E7%9A%84%E8%99%9A%E6%8B%9Fdom)

3. LeeCode 刷题

4. 项目进度
   - [x] 回复功能

## 2021.8.20

1. 文章阅读

- [响应式布局]https://blog.csdn.net/qfguan/article/details/109364290)

2. 源码阅读

- [Vue 中的虚拟 DOM](https://vue-js.com/learn-vue/virtualDOM/#_3-vue%E4%B8%AD%E7%9A%84%E8%99%9A%E6%8B%9Fdom)

3. LeeCode 刷题

4. 项目进度

- [x] 封装回复组件
- [x] 优化代码

## 2021.8.19

1. 文章阅读

- [响应式布局]https://blog.csdn.net/qfguan/article/details/109364290)

2. 源码阅读

- [Vue 中的虚拟 DOM](https://vue-js.com/learn-vue/virtualDOM/#_3-vue%E4%B8%AD%E7%9A%84%E8%99%9A%E6%8B%9Fdom)

3. LeeCode 刷题

- [只出现一次的数字](https://leetcode-cn.com/problems/single-number/)

4. 项目进度

- [x] 模糊搜索
- [x] 遮罩

## 2021.8.18

1. 文章阅读

- [跨域](https://jasonandjay.github.io/study/zh/book/Ajax%E7%AF%87.html#%E5%A6%82%E4%BD%95%E8%A7%A3%E5%86%B3%E8%B7%A8%E5%9F%9F%E9%97%AE%E9%A2%98)
- [交互](https://jasonandjay.github.io/study/zh/standard/Cooperation.html#_6-%E5%89%8D%E7%AB%AF%E5%BA%94%E8%AF%A5%E5%91%8A%E7%9F%A5%E5%90%8E%E5%8F%B0%E5%93%AA%E4%BA%9B%E6%9C%89%E6%95%88%E4%BF%A1%E6%81%AF-%E5%90%8E%E5%8F%B0%E6%89%8D%E8%83%BD%E8%BF%94%E5%9B%9E%E5%89%8D%E7%AB%AF%E6%83%B3%E7%9A%84%E6%95%B0%E6%8D%AE%E7%9A%84%E5%91%A2)

2. 源码阅读

- [Vue 中的虚拟 DOM](https://vue-js.com/learn-vue/virtualDOM/#_3-vue%E4%B8%AD%E7%9A%84%E8%99%9A%E6%8B%9Fdom)

3. LeeCode 刷题

- [只出现一次的数字](https://leetcode-cn.com/problems/single-number/)

4. 项目进度

- [x] 关于页面留言分页
- [x] 跳转详情页面
- [x] 图片放大
- [x] 详情页面排版

## 2021.8.17

1. 文章阅读

- [hooks](https://www.jianshu.com/p/89f2cf94a7c2)
- [umijs](https://umijs.org/zh/guide/)

2. 源码阅读

- [Vue 中的虚拟 DOM](https://vue-js.com/learn-vue/virtualDOM/#_3-vue%E4%B8%AD%E7%9A%84%E8%99%9A%E6%8B%9Fdom)

3. LeeCode 刷题

- [反转字符串](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xnhbqj/)

4. 项目进度\*

- [x] 完成推荐阅读模块
- [x] 完成留言模块渲染

## 2021.8.16

1. 文章阅读

- [hooks](https://www.jianshu.com/p/89f2cf94a7c2)
- [umijs](https://umijs.org/zh/guide/)

2. 源码阅读

- [Vue 中的虚拟 DOM](https://vue-js.com/learn-vue/virtualDOM/#_3-vue%E4%B8%AD%E7%9A%84%E8%99%9A%E6%8B%9Fdom)

3. LeeCode 刷题

- [删除排序数组中的重复项](https://leetcode-cn.com/leetbook/read/tencent/xxynuj/)

4. 项目进度\*

- [x] 请求数据
- [x] 渲染页面

## 2021.8.15

1. 文章阅读

- [hooks](https://www.jianshu.com/p/89f2cf94a7c2)
- [umijs](https://umijs.org/zh/guide/)

2. 源码阅读

- [Vue 中的虚拟 DOM](https://vue-js.com/learn-vue/virtualDOM/#_3-vue%E4%B8%AD%E7%9A%84%E8%99%9A%E6%8B%9Fdom)

3. LeeCode 刷题

- [反转字符串](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xnhbqj/)

4. 项目进度\*

- [x] 基本配置
- [x] 搭建框架

## 2021.8.13

1. 文章阅读

- [你不知道的 Chrome 调试技巧](https://juejin.cn/book/6844733783166418958/section/6844733783187390477)
- [umijs](https://umijs.org/zh/guide/)

2. 源码阅读

- [原生实现一个 react-redux 的代码示例](https://www.jb51.net/article/141664.htm)

3. LeeCode 刷题

4. 项目进度\*

- [x] 搭建框架

## 2021.8.12

1. 文章阅读

- [你不知道的 Chrome 调试技巧](https://juejin.cn/book/6844733783166418958/section/6844733783187390477)

2. 源码阅读
3. LeeCode 刷题

- [只出现一次的数字](https://leetcode-cn.com/problems/single-number/)
- [寻找两个正序数组的中位数](https://leetcode-cn.com/problems/median-of-two-sorted-arrays/)

4. 项目进度

## 2021.8.11

1. 文章阅读
   - [集合、列表和数组](https://leetcode-cn.com/leetbook/read/array-and-string/yjcir/)
2. LeeCode 刷题
   - [两数之和](https://leetcode-cn.com/leetbook/read/tencent/xxqfy5/)

---

# 马运龙
## 2021.9.12

1. 文章阅读

- [Vue：知道什么时候使用计算属性并不能提高性能吗？](https://juejin.cn/post/7005336858049642527)

1. 源码阅读

- [原生实现一个 react-redux 的代码示例](https://www.jb51.net/article/141664.htm)

3. LeeCode 刷题

- [全排列](https://leetcode-cn.com/leetbook/read/tencent/x5j6cm/)
- [两数之和 II - 输入有序数组](https://leetcode-cn.com/problems/two-sum-ii-input-array-is-sorted/)

1. 项目进度

## 2021.9.10

1. 文章阅读

- [Vue：知道什么时候使用计算属性并不能提高性能吗？](https://juejin.cn/post/7005336858049642527)

1. 源码阅读

- [原生实现一个 react-redux 的代码示例](https://www.jb51.net/article/141664.htm)

3. LeeCode 刷题

- [子集](https://leetcode-cn.com/leetbook/read/tencent/x5bzb3/)
- [全排列](https://leetcode-cn.com/leetbook/read/tencent/x5j6cm/)

1. 项目进度

## 2021.9.9

1. 文章阅读

- [Vue：知道什么时候使用计算属性并不能提高性能吗？](https://juejin.cn/post/7005336858049642527)

1. 源码阅读

- [原生实现一个 react-redux 的代码示例](https://www.jb51.net/article/141664.htm)

3. LeeCode 刷题

- [子集](https://leetcode-cn.com/leetbook/read/tencent/x5bzb3/)
- [全排列](https://leetcode-cn.com/leetbook/read/tencent/x5j6cm/)

1. 项目进度

## 2021.9.8

1. 文章阅读

- [Vue：知道什么时候使用计算属性并不能提高性能吗？](https://juejin.cn/post/7005336858049642527)

1. 源码阅读

- [原生实现一个 react-redux 的代码示例](https://www.jb51.net/article/141664.htm)

3. LeeCode 刷题

- [爬楼梯](https://leetcode-cn.com/leetbook/read/tencent/x5vk4t/)
- [最大子序和](https://leetcode-cn.com/leetbook/read/tencent/x5w3sr/)

4. 项目进度

## 2021.9.3

1. 文章阅读

- [5 分钟搞懂面试官必问 React 题（二）](https://juejin.cn/post/6997609116159967269)

- [你真的弄懂 HTTP 了吗？（四）](https://juejin.cn/post/6994584318731354142)

2. 源码阅读

- [实现一个双向绑定](https://juejin.cn/post/6844903989083897870#heading-8)

3. LeeCode 刷题

- [合并两个有序链表](https://leetcode-cn.com/leetbook/read/tencent/x59tp7/)
- [合并 K 个排序链表](https://leetcode-cn.com/leetbook/read/tencent/x5ode6/)

4. 项目进度

## 2021.9.2

1. 文章阅读

- [5 分钟搞懂面试官必问 React 题（二）](https://juejin.cn/post/6997609116159967269)

- [你真的弄懂 HTTP 了吗？（四）](https://juejin.cn/post/6994584318731354142)

2. 源码阅读

- [实现一个双向绑定](https://juejin.cn/post/6844903989083897870#heading-8)

3. LeeCode 刷题

- [二叉搜索树的最近公共祖先](https://leetcode-cn.com/leetbook/read/tencent/x5qkvc/)
- [二叉树的最近公共祖先](https://leetcode-cn.com/leetbook/read/tencent/x563ni/)

4. 项目进度

## 2021.9.1

1. 文章阅读

- [5 分钟搞懂面试官必问 React 题（二）](https://juejin.cn/post/6997609116159967269)

- [你真的弄懂 HTTP 了吗？（四）](https://juejin.cn/post/6994584318731354142)

2. 源码阅读

- [实现一个双向绑定](https://juejin.cn/post/6844903989083897870#heading-8)

3. LeeCode 刷题

- [两个数组的交集](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2y0c2/)
- [删除排序数组中的重复项](https://leetcode-cn.com/leetbook/read/tencent/xxynuj/)

4. 项目进度

## 2021.8.31

1. 文章阅读

- [5 分钟搞懂面试官必问 React 题（一）](https://juejin.cn/post/6997269945394397197)

- [你真的弄懂 HTTP 了吗？（三）](https://juejin.cn/post/6994242927991980040)

2. 源码阅读

- [实现一个双向绑定](https://juejin.cn/post/6844903989083897870#heading-8)

3. LeeCode 刷题

- [两个数组的交集](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2y0c2/)
- [删除排序数组中的重复项](https://leetcode-cn.com/leetbook/read/tencent/xxynuj/)

4. 项目进度

## 2021.8.30

1. 文章阅读

- [5 分钟搞懂面试官必问 React 题（一）](https://juejin.cn/post/6997269945394397197)

- [你真的弄懂 HTTP 了吗？（三）](https://juejin.cn/post/6994242927991980040)

2. 源码阅读

- [实现一个双向绑定](https://juejin.cn/post/6844903989083897870#heading-8)

3. LeeCode 刷题

- [存在重复元素](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x248f5/)
- [字符串转换整数](https://leetcode-cn.com/leetbook/read/tencent/xxgggc/)

1. 项目进度

## 2021.8.29

1. 文章阅读

- [5 分钟搞懂面试官必问 React 题（一）](https://juejin.cn/post/6997269945394397197)

- [你真的弄懂 HTTP 了吗？（三）](https://juejin.cn/post/6994242927991980040)

2. 源码阅读

- [实现一个双向绑定](https://juejin.cn/post/6844903989083897870#heading-8)

3. LeeCode 刷题

- [只出现一次的数字](https://leetcode-cn.com/problems/single-number/)
- [反转字符串](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xnhbqj/)

1. 项目进度

## 2021.8.27

1. 文章阅读

- [你真的弄懂 React 了吗？（六）](https://juejin.cn/post/6996931408153296932)

- [你真的弄懂 HTTP 了吗？（二）](https://juejin.cn/post/6993881978818789389)

2. 源码阅读

- [实现一个双向绑定](https://juejin.cn/post/6844903989083897870#heading-8)

3. LeeCode 刷题

- [字符串转换整数](https://leetcode-cn.com/leetbook/read/tencent/xxgggc/)

4. 项目进度

## 2021.8.26

1. 文章阅读

- [你真的弄懂 React 了吗？（六）](https://juejin.cn/post/6996931408153296932)

- [你真的弄懂 HTTP 了吗？（二）](https://juejin.cn/post/6993881978818789389)

2. 源码阅读

- [常用 Hooks](https://jasonandjay.github.io/study/zh/standard/Hooks.html#hooks%E4%BA%A7%E7%94%9F%E7%9A%84%E8%83%8C%E6%99%AF)

3. LeeCode 刷题

- [只出现一次的数字](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x21ib6/)
- [零矩阵](https://leetcode-cn.com/leetbook/read/array-and-string/ciekh/)

4. 项目进度

- [x] 下载 umijs 框架
- [x] 实现归档的数据渲染
- [x] 实现跳详情
- [x] 回到顶部
- [x] 详情页左边点赞
- [x] 详情页的排版
- [x] 实现归档页面的排版
- [x] 路由
- [x] 完成首页

## 2021.8.25

1. 文章阅读

- [你真的弄懂 React 了吗？（六）](https://juejin.cn/post/6996931408153296932)

- [你真的弄懂 HTTP 了吗？（一）](https://juejin.cn/post/6993585284096983076)

2. 源码阅读

- [常用 Hooks](https://jasonandjay.github.io/study/zh/standard/Hooks.html#hooks%E4%BA%A7%E7%94%9F%E7%9A%84%E8%83%8C%E6%99%AF)

3. LeeCode 刷题

- [只出现一次的数字](https://leetcode-cn.com/leetbook/read/tencent/x51e9r/)
- [零矩阵](https://leetcode-cn.com/leetbook/read/array-and-string/ciekh/)

4. 项目进度

- [x] 下载 umijs 框架
- [x] 实现归档的数据渲染
- [x] 实现跳详情
- [x] 回到顶部
- [x] 详情页左边点赞
- [x] 详情页的排版
- [x] 实现归档页面的排版
- [x] 路由
- [x] 完成首页

## 2021.8.24

1. 文章阅读

- [你真的弄懂 React 了吗？（五）](https://juejin.cn/post/6996478115488727053)

- [你真的弄懂 HTTP 了吗？（一）](https://juejin.cn/post/6993585284096983076)

2. 源码阅读

- [常用 Hooks](https://jasonandjay.github.io/study/zh/standard/Hooks.html#hooks%E4%BA%A7%E7%94%9F%E7%9A%84%E8%83%8C%E6%99%AF)

3. LeeCode 刷题

- [旋转矩阵](https://leetcode-cn.com/leetbook/read/array-and-string/clpgd/)
- [零矩阵](https://leetcode-cn.com/leetbook/read/array-and-string/ciekh/)

4. 项目进度

- [x] 下载 umijs 框架
- [x] 实现归档的数据渲染
- [x] 实现跳详情
- [x] 回到顶部
- [x] 详情页左边点赞
- [x] 详情页的排版
- [x] 实现归档页面的排版
- [x] 路由
- [x] 完成首页

## 2021.8.23

1. 文章阅读

- [你真的弄懂 React 了吗？（五）](https://juejin.cn/post/6996478115488727053)

- [你真的弄懂 HTTP 了吗？（一）](https://juejin.cn/post/6993585284096983076)

2. 源码阅读

- [vue 源码--vue-router 根据源码实现方式](https://www.jianshu.com/p/3baa76ebbce0)

3. LeeCode 刷题

- [2 的幂](https://leetcode-cn.com/leetbook/read/tencent/x5yjhd/)
- [有效的括号](https://leetcode-cn.com/leetbook/read/tencent/xx1d8v/)

4. 项目进度

- [x] 下载 umijs 框架
- [x] 实现归档的数据渲染
- [x] 实现跳详情
- [x] 回到顶部
- [x] 详情页左边点赞
- [x] 详情页的排版
- [x] 实现归档页面的排版
- [x] 路由
- [x] 完成首页

## 2021.8.22

1. 文章阅读

- [面试官：“宝子，setState 是同步还是异步的呀？”](https://juejin.cn/post/6996846391108567077)
- [ES6 的 symbol 数据类型](https://juejin.cn/post/6997335989425274911)

2. 源码阅读

- [vue 源码--vue-router 根据源码实现方式](https://www.jianshu.com/p/3baa76ebbce0)

3. LeeCode 刷题

- [2 的幂](https://leetcode-cn.com/leetbook/read/tencent/x5yjhd/)
- [有效的括号](https://leetcode-cn.com/leetbook/read/tencent/xx1d8v/)

1. 项目进度

- [x] 下载 umijs 框架
- [x] 实现归档的数据渲染
- [x] 实现跳详情
- [x] 回到顶部
- [x] 详情页的排版
- [x] 实现归档页面的排版
- [x] 路由
- [x] 完成首页

## 2021.8.20

1. 文章阅读

- [你真的弄懂 React 了吗？（三）](https://juejin.cn/post/6995786534926417950)
- [可以用它来面试潜在候选人的 5 个 JavaScript 面试题（三）](https://juejin.cn/post/6992749248391413773)

2. 源码阅读

- [React 源码分析系列](https://www.cnblogs.com/iyulang/p/6586749.html)

3. LeeCode 刷题

- [删除排序数组中的重复项](https://leetcode-cn.com/leetbook/read/tencent/xxynuj/)
- [两个数组的交集 II](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2y0c2/)

4. 项目进度

- [x] 下载 umijs 框架
- [x] 实现归档的数据渲染
- [x] 实现跳详情
- [x] 回到顶部
- [x] 详情页的排版
- [x] 实现归档页面的排版
- [x] 路由
- [x] 完成首页

## 2021.8.19

1. 文章阅读

- [你真的弄懂 React 了吗？（三）](https://juejin.cn/post/6995786534926417950)
- [可以用它来面试潜在候选人的 5 个 JavaScript 面试题（三）](https://juejin.cn/post/6992749248391413773)

2. 源码阅读

- [React 源码分析系列](https://www.cnblogs.com/iyulang/p/6586749.html)

3. LeeCode 刷题

- [合并两个有序链表](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xnnbp2/)
- [爬楼梯](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xn854d/)

4. 项目进度

- [x] 下载 umijs 框架
- [x] 实现归档的数据渲染
- [x] 实现跳详情
- [x] 详情页的排版
- [x] 实现归档页面的排版
- [x] 路由
- [x] 完成首页

## 2021.8.18

1. 文章阅读

- [你真的弄懂 React 了吗？（三）](https://juejin.cn/post/6995786534926417950)
- [可以用它来面试潜在候选人的 5 个 JavaScript 面试题（三）](https://juejin.cn/post/6992749248391413773)

2. 源码阅读

- [React 源码分析系列](https://www.cnblogs.com/iyulang/p/6586749.html)

3. LeeCode 刷题

- [合并两个有序链表](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xnnbp2/)
- [爬楼梯](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xn854d/)

4. 项目进度

- [x] 下载 umijs 框架
- [x] 实现归档的数据渲染
- [x] 实现跳详情
- [x] 实现归档页面的排版
- [x] 路由
- [x] 完成首页

## 2021.8.17

1. 文章阅读

- [你真的弄懂 React 了吗？（三）](https://juejin.cn/post/6995786534926417950)
- [可以用它来面试潜在候选人的 5 个 JavaScript 面试题（三）](https://juejin.cn/post/6992749248391413773)

2. 源码阅读

- [React 源码分析系列](https://www.cnblogs.com/iyulang/p/6586749.html)

3. LeeCode 刷题

- [反转字符串](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xnhbqj/)
- [两数之和](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2jrse/)

4. 项目进度

- [x] 下载 umijs 框架
- [x] 实现归档的数据渲染
- [x] 实现归档页面的排版
- [x] 路由
- [x] 完成首页

## 2021.8.16

1. 文章阅读

- [你真的弄懂 React 了吗？（二）](https://juejin.cn/post/6995376730139541512)
- [可以用它来面试潜在候选人的 5 个 JavaScript 面试题（二）](https://juejin.cn/post/6992389793334165518)

2. 源码阅读

- [React 源码分析系列](https://www.cnblogs.com/iyulang/p/6586749.html)

3. LeeCode 刷题

- [移动零](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2ba4i/)

4. 项目进度

- [x] 下载 umijs 框架
- [x] 实现归档的数据渲染
- [x] 路由
- [x] 完成首页

## 2021.8.15

1. 文章阅读

- [你真的弄懂 React 了吗？（二）](https://juejin.cn/post/6995376730139541512)
- [可以用它来面试潜在候选人的 5 个 JavaScript 面试题（二）](https://juejin.cn/post/6992389793334165518)

2. 源码阅读

- [React 源码分析系列](https://www.cnblogs.com/iyulang/p/6586749.html)

3. LeeCode 刷题

- [加一](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2cv1c/)

4. 项目进度

- [x] 下载 umijs 框架
- [x] 路由
- [x] 完成首页

## 2021.8.13

1. 文章阅读

- [你真的弄懂 React 了吗？（一）](https://juejin.cn/post/6995084523100700703)
- [可以用它来面试潜在候选人的 5 个 JavaScript 面试题（一）](https://juejin.cn/post/6992034249574449165)

2. 源码阅读

- [原生实现一个 react-redux 的代码示例](https://www.jb51.net/article/141664.htm)

3. LeeCode 刷题

- [只出现一次的数字](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x21ib6/)

4. 项目进度

- [x] 下载 umijs 框架
- [x] 完成首页

## 2021.8.12

1. 文章阅读

- [你真的弄懂 React 了吗？（一）](https://juejin.cn/post/6995084523100700703)
- [可以用它来面试潜在候选人的 5 个 JavaScript 面试题（一）](https://juejin.cn/post/6992034249574449165)

2. 源码阅读

- [原生实现一个 react-redux 的代码示例](https://www.jb51.net/article/141664.htm)

3. LeeCode 刷题

- [存在重复元素](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x248f5/)

4. 项目进度

- [x] 下载 umi

## 2021.8.11

1. 文章阅读

- [vue 的路由权限控制，如何从后台获取数据呀~来瞧瞧~](https://juejin.cn/post/6994979622060294151)
- [es6 解构赋值 [a,b] = [b,a]的几个问题](https://juejin.cn/post/6994634734634696734)

2. 源码阅读

- [Vue 中的虚拟 DOM](https://vue-js.com/learn-vue/virtualDOM/#_3-vue%E4%B8%AD%E7%9A%84%E8%99%9A%E6%8B%9Fdom)

3. LeeCode 刷题

- [旋转数组](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2skh7/)

4. 项目进度

---

# 黄文丽
## 2021.9.12
1. 1. 文章阅读
- [【JS】深入理解事件循环](https://www.jianshu.com/p/096d4c3f79dc)
2. 源码阅读
- [vue 源码 vue-router 根据源码实现方式](https://www.jianshu.com/p/3baa76ebbce0)
3. LeeCode刷题
- [最小栈](https://leetcode-cn.com/leetbook/read/tencent/x5l233/)
4. 项目进度
   - [x] 小程序配置路由
   - [x] 渲染swiper
## 2021.9.10
1. 1. 文章阅读
- [Vue --- 组件化](https://www.jianshu.com/p/59c2d7735f42)
2. 源码阅读
- [vue 源码 vue-router 根据源码实现方式](https://www.jianshu.com/p/3baa76ebbce0)
3. LeeCode刷题
- [爬楼梯](https://leetcode-cn.com/leetbook/read/tencent/x5vk4t/)
4. 项目进度
   - [x] 用户管理
   - [x] 统计页面
   - [x] 搜索记录
## 2021.9.9

1. 1. 文章阅读

- [v-if 与 v-show 的区别你了解吗](https://juejin.cn/post/7005850379011227678)
- [Undefined 与 Null 的区别](https://juejin.cn/post/6844903777506426893)

2. 源码阅读

- [vue 源码 vue-router 根据源码实现方式](https://www.jianshu.com/p/3baa76ebbce0)

3. LeeCode 刷题

- [ 加一 ](https://leetcode-cn.com/problems/plus-one/solution/)

4. 项目进度
   - [x] 获取用户管理页面分页接口，完成分页功能
   - [x] 获取访问统计页面分页接口，完成分页功能
   - [x] 获取搜索记录页面分页接口，完成分页功能

## 2021.9.8

1. 文章阅读

- [详解 vue 中的 ref 和$refs 的使用](https://juejin.cn/post/6844903731058704397)

2. 源码阅读

- [vue 源码 vue-router 根据源码实现方式](https://www.jianshu.com/p/3baa76ebbce0)

3. LeeCode 刷题

- [两数之和 II - 输入有序数组](https://leetcode-cn.com/problems/two-sum-ii-input-array-is-sorted/)

4. 项目进度
   - [x] 获取用户管理页面搜索接口，完成搜索功能
   - [x] 获取访问统计页面搜索接口，完成搜索功能
   - [x] 获取搜索记录页面搜索接口，完成搜索功能

## 2021.9.3

1. 文章阅读

- [HTTP 强缓存和协商缓存](https://segmentfault.com/a/1190000008956069)

2. 源码阅读

- [vue 源码 vue-router 根据源码实现方式](https://www.jianshu.com/p/3baa76ebbce0)

3. LeeCode 刷题

- [字符串相乘](https://leetcode-cn.com/leetbook/read/tencent/xxbolr/)

4. 项目进度
   - [x] 获取搜索记录页面删除接口，实现删除功能
   - [x] 获取访问统计页面删除接口，实现删除功能
   - [x] 获取用户管理页面删除接口，实现删除功能

## 2021.9.2

1. 文章阅读

- [闭包](https://juejin.cn/post/6995710760928149535)

2. 源码阅读

- [ 除自身以外数组的乘积 ](https://leetcode-cn.com/leetbook/read/tencent/xxfcwh/)

3. LeeCode 刷题

- [字符串相乘](https://leetcode-cn.com/leetbook/read/tencent/xxbolr/)

4. 项目进度
   - [x] 获取搜索记录页面删除接口，实现删除功能
   - [x] 获取访问统计页面删除接口，实现删除功能
   - [x] 获取用户管理页面删除接口，实现删除功能

## 2021.9.1

1. 文章阅读

- [HTTP 强缓存和协商缓存](https://segmentfault.com/a/1190000008956069)

2. 源码阅读

- [ReactElement](https://react.jokcy.me/book/api/react-element.html)

3. LeeCode 刷题

- [字符串相乘](https://leetcode-cn.com/leetbook/read/tencent/xxbolr/)

4. 项目进度
   - [x] 获取搜索记录页面接口，排版
   - [x] 获取访问统计页面接口，排版
   - [x] 获取用户管理页面接口，排版

## 2021.8.31

1. 文章阅读

## 2021.9.3

1. 文章阅读
   - [类(Class)](https://juejin.cn/book/6844733813021491207/section/6844733813117943821)
2. 源码阅读

- [组件的类型与生命周期](https://juejin.cn/post/6844903508798357511)

3. LeeCode 刷题
   - [格雷编码](https://leetcode-cn.com/leetbook/read/tencent/x5f811/)
   - [爬楼梯](https://leetcode-cn.com/leetbook/read/tencent/x5vk4t/)
   - [最大子序和](https://leetcode-cn.com/leetbook/read/tencent/x5w3sr/)
4. 项目进度
   - [x] 配置后台管理路由
   - [x] 获取搜索管理页面接口，排版

## 2021.8.30

1. 文章阅读
   - [接口(interface)](https://juejin.cn/book/6844733813021491207/section/6844733813117943816)
2. 源码阅读

- [组件的类型与生命周期](https://juejin.cn/post/6844903508798357511)

3. LeeCode 刷题
   - [括号生成](https://leetcode-cn.com/leetbook/read/tencent/x5ku2e/)
   - [子集](https://leetcode-cn.com/leetbook/read/tencent/x5bzb3/)
   - [全排列](https://leetcode-cn.com/leetbook/read/tencent/x5j6cm/)
4. 项目进度
   - [x] 评论管理
   - [x] 项目管理

## 2021.9.1

1. 文章阅读
   - [深入理解枚举类型](https://juejin.cn/book/6844733813021491207/section/6844733813117943822)
2. 源码阅读

- [组件的实现与挂载](https://juejin.cn/post/6844903504528556040)

3. LeeCode 刷题

   - [相交链表](https://leetcode-cn.com/leetbook/read/tencent/x52ycv/)
   - [删除链表中的节点](https://leetcode-cn.com/leetbook/read/tencent/x5ns1j/)

4. 项目进度

   - [x] 评论管理
   - [x] 项目管理

5. 文章阅读

- [任务队列的执行顺序](https://juejin.cn/post/6995066873305890847)

2. 源码阅读

- [vue 源码--vue-router 根据源码实现方式](https://www.jianshu.com/p/3baa76ebbce0)

3. LeeCode 刷题

- [删除排序数组中的重复项](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2gy9m/)

4. 项目进度

---

# 王宇
## 2021.09.12

1. 文章阅读

- [ 原型与原型链详解 ](https://www.jianshu.com/p/ddaa5179cda6)
- [ JavaScript对象 ](https://www.jianshu.com/p/1a89512c970a)

2. LeetCode 刷题

- [ 加一 ](https://leetcode-cn.com/problems/plus-one/solution/)

3. 源码阅读

4. 项目进度

- [x] 渲染所有文章表格数据、完成排版
- [x] 渲染页面管理表格数据、完成排版
- [x] 完成分类管理、标签管理页面排版
## 2021.09.10

1. 文章阅读

- [ React面试题总结 ](https://www.jianshu.com/p/f0cfa277c00a)

2. LeetCode 刷题

- [ 加一 ](https://leetcode-cn.com/problems/plus-one/solution/)

3. 源码阅读

4. 项目进度

- [x] 渲染所有文章表格数据、完成排版
- [x] 渲染页面管理表格数据、完成排版
- [x] 完成分类管理、标签管理页面排版

## 2021.09.09

1. 文章阅读

- [ React 合成事件和原生事件的区别 ](https://www.jianshu.com/p/8d8f9aa4b033)

2. LeetCode 刷题

- [ 加一 ](https://leetcode-cn.com/problems/plus-one/solution/)

3. 源码阅读

4. 项目进度

- [x] 渲染所有文章表格数据、完成排版
- [x] 渲染页面管理表格数据、完成排版
- [x] 完成分类管理、标签管理页面排版

## 2021.09.08

1. 文章阅读

- [ JS 面试题 ](https://zhuanlan.zhihu.com/p/90612666)

2. LeetCode 刷题

- [ 加一 ](https://leetcode-cn.com/problems/plus-one/solution/)

3. 源码阅读

4. 项目进度

- [x] 渲染所有文章表格数据、完成排版
- [x] 渲染页面管理表格数据、完成排版
- [x] 完成分类管理、标签管理页面排版

## 2021.09.03

1. 文章阅读

- [Socket 原理与基本操作](https://www.jianshu.com/p/6de6aead3908)
- [Socket 原理讲解](https://www.jianshu.com/p/02ae5c8d837e)

2. LeetCode 刷题

- [ 除自身以外数组的乘积 ](https://leetcode-cn.com/leetbook/read/tencent/xxfcwh/)

3. 源码阅读

4. 项目进度

## 2021.09.02

1. 文章阅读

- [Socket 原理与基本操作](https://www.jianshu.com/p/6de6aead3908)
- [Socket 原理讲解](https://www.jianshu.com/p/02ae5c8d837e)

2. LeetCode 刷题

- [ 最大子序和 ](https://leetcode-cn.com/problems/maximum-subarray/)

3. 源码阅读

4. 项目进度

## 2021.09.01

1. 文章阅读

- [Socket 技术详解](https://www.jianshu.com/p/066d99da7cbd)
- [React umi](https://www.jianshu.com/p/f82b92e6d135)

2. LeetCode 刷题

- [ 实现一个双向绑定 ](https://juejin.cn/post/6844903989083897870#heading-8)

3. 源码阅读

4. 项目进度

---
# 任永亮
## 2021.9.12
1. 文章阅读
   - [可辨识联合类型](https://juejin.cn/book/6844733813021491207/section/6844733813126332423)
2. 源码阅读
  - [详解事务与更新队列](https://juejin.cn/post/6844903511478697998)
3. LeeCode刷题
 
4. 项目进度
    - [x] 评论管理
    - [x] 项目管理
## 2021.9.10
1. 文章阅读
   - [高级类型之交叉类型、联合类型、类型别名](https://juejin.cn/book/6844733813021491207/section/6844733813126348814)
2. 源码阅读
  - [详解事务与更新队列](https://juejin.cn/post/6844903511478697998)
3. LeeCode刷题
 
4. 项目进度
    - [x] 评论管理
    - [x] 项目管理
## 2021.9.9
1. 文章阅读
   - [类型兼容性](https://juejin.cn/book/6844733813021491207/section/6844733813126332430)
2. 源码阅读
  - [详解事务与更新队列](https://juejin.cn/post/6844903511478697998)
3. LeeCode刷题
 
4. 项目进度
    - [x] 评论管理
    - [x] 项目管理
## 2021.9.8
1. 文章阅读
   - [类型断言与类型守卫](https://juejin.cn/book/6844733813021491207/section/6844733813122138119)
2. 源码阅读
  - [组件的类型与生命周期](https://juejin.cn/post/6844903508798357511)
3. LeeCode刷题
   - [最小栈](https://leetcode-cn.com/leetbook/read/tencent/x5l233/)
   - [Nim 游戏](https://leetcode-cn.com/leetbook/read/tencent/x5c1fh/)
4. 项目进度
    - [x] 评论管理
    - [x] 项目管理
## 2021.9.6
1. 文章阅读
   - [泛型（generic）的妙用](https://juejin.cn/book/6844733813021491207/section/6844733813122154504)
2. 源码阅读
  - [组件的类型与生命周期](https://juejin.cn/post/6844903508798357511)
3. LeeCode刷题
   - [子集](https://leetcode-cn.com/leetbook/read/tencent/x5zwpv/)
   - [LRU缓存机制](https://leetcode-cn.com/leetbook/read/tencent/x57w6h/)
4. 项目进度
    - [x] 评论管理
    - [x] 项目管理
## 2021.9.5
1. 文章阅读
   - [函数(Function)](https://juejin.cn/book/6844733813021491207/section/6844733813122138120)
2. 源码阅读
  - [组件的类型与生命周期](https://juejin.cn/post/6844903508798357511)
3. LeeCode刷题
   - [买卖股票的最佳时机](https://leetcode-cn.com/leetbook/read/tencent/x5dugs/)
   - [买卖股票的最佳时机 II](https://leetcode-cn.com/leetbook/read/tencent/x507kd/)
   - [不同路径](https://leetcode-cn.com/leetbook/read/tencent/x5g7mg/)
4. 项目进度
    - [x] 评论管理
    - [x] 项目管理
## 2021.9.3
1. 文章阅读
   - [类(Class)](https://juejin.cn/book/6844733813021491207/section/6844733813117943821)
2. 源码阅读
  - [组件的类型与生命周期](https://juejin.cn/post/6844903508798357511)
3. LeeCode刷题
   - [格雷编码](https://leetcode-cn.com/leetbook/read/tencent/x5f811/)
   - [爬楼梯](https://leetcode-cn.com/leetbook/read/tencent/x5vk4t/)
   - [最大子序和](https://leetcode-cn.com/leetbook/read/tencent/x5w3sr/)
4. 项目进度
    - [x] 评论管理
    - [x] 项目管理
## 2021.9.2
1. 文章阅读
   - [接口(interface)](https://juejin.cn/book/6844733813021491207/section/6844733813117943816)
2. 源码阅读
  - [组件的类型与生命周期](https://juejin.cn/post/6844903508798357511)
3. LeeCode刷题
   - [括号生成](https://leetcode-cn.com/leetbook/read/tencent/x5ku2e/)
   - [子集](https://leetcode-cn.com/leetbook/read/tencent/x5bzb3/)
   - [全排列](https://leetcode-cn.com/leetbook/read/tencent/x5j6cm/)
4. 项目进度
    - [x] 评论管理
    - [x] 项目管理
## 2021.9.1
1. 文章阅读
   - [深入理解枚举类型](https://juejin.cn/book/6844733813021491207/section/6844733813117943822)
2. 源码阅读
  - [组件的实现与挂载](https://juejin.cn/post/6844903504528556040)
3. LeeCode刷题
   - [相交链表](https://leetcode-cn.com/leetbook/read/tencent/x52ycv/)
   - [删除链表中的节点](https://leetcode-cn.com/leetbook/read/tencent/x5ns1j/)
  
4. 项目进度
    - [x] 评论管理
    - [x] 项目管理

1. 文章阅读

- [响应式布局](https://blog.csdn.net/qfguan/article/details/109364290)
- [闭包](https://zhuanlan.zhihu.com/p/22486908)

2. LeetCode 刷题

- [ 实现一个双向绑定 ](https://juejin.cn/post/6844903989083897870#heading-8)

3. 源码阅读

4. 项目进度

## 2021.08.30

1. 文章阅读

- [ call、apply、bind 三者的用法和区别 ](https://blog.csdn.net/hexinyu_1022/article/details/82795517)
- [ 重绘和回流 ](https://zhuanlan.zhihu.com/p/52076790)

2. LeetCode 刷题

- [ 实现一个双向绑定 ](https://juejin.cn/post/6844903989083897870#heading-8)

3. 源码阅读

4. 项目进度

## 2021.08.29

1. 文章阅读

- [ ES6：遍历对象的键的所有方法 ](https://segmentfault.com/a/1190000014459289?utm_source=sf-similar-article)
- [ 属性的可枚举性与不可枚举性 ](https://segmentfault.com/a/1190000014745723?utm_source=sf-similar-article)

2. LeetCode 刷题

- [两数相加](https://leetcode-cn.com/problems/add-two-numbers/solution/liang-shu-xiang-jia-by-leetcode-solution/)

3. 源码阅读

- [ 实现一个双向绑定 ](https://juejin.cn/post/6844903989083897870#heading-8)

4. 项目进度

## 2021.08.27

1. 文章阅读

- [ hooks 系列九：hooks 实战 ](https://segmentfault.com/a/1190000040479927)
- [ hooks 系列四：useRefs ](https://segmentfault.com/a/1190000040402752?utm_source=sf-similar-article)

2. LeetCode 刷题

- [ 除自身以外数组的乘积 ](https://leetcode-cn.com/leetbook/read/tencent/xxfcwh/)

3. 源码阅读

- [ 实现一个双向绑定 ](https://juejin.cn/post/6844903989083897870#heading-8)

4. 项目进度

## 2021.08.26

1. 文章阅读

- [单页面应用路由的两种实现方式](https://www.cnblogs.com/zhuzhenwei918/p/7421430.html)
- [闭包](https://juejin.cn/post/6995710760928149535)

2. LeetCode 刷题

- [两数之和 II - 输入有序数组](https://leetcode-cn.com/problems/two-sum-ii-input-array-is-sorted/)

3. 源码阅读

- [vue 源码 vue-router 根据源码实现方式](https://www.jianshu.com/p/3baa76ebbce0)

4. 项目进度

## 2021.08.25

1. 文章阅读

- [HTTP 强缓存和协商缓存](https://segmentfault.com/a/1190000008956069)
- [前端面试题 -- webpack](https://segmentfault.com/a/1190000018704305)

2. LeetCode 刷题

- [两数之和 II - 输入有序数组](https://leetcode-cn.com/problems/two-sum-ii-input-array-is-sorted/)

3. 源码阅读

- [实现一个双向绑定](https://juejin.cn/post/6844903989083897870#heading-8)

4. 项目进度

## 2021.08.24

1. 文章阅读

- [react 高阶使用](https://segmentfault.com/a/1190000023615992?utm_source=sf-similar-article)
- [react 组件传值](https://segmentfault.com/a/1190000019365645?utm_source=sf-similar-article)

2. LeetCode 刷题

- [两数之和 II - 输入有序数组](https://leetcode-cn.com/problems/two-sum-ii-input-array-is-sorted/)

3. 源码阅读

- [实现一个双向绑定](https://juejin.cn/post/6844903989083897870#heading-8)

4. 项目进度

## 2021.08.23

1. 文章阅读

- [详解 react hooks(含高阶组件)](https://blog.csdn.net/kellywong/article/details/106430977)
- [React 中组件间通信的几种方式](https://segmentfault.com/a/1190000016647850?utm_source=sf-similar-article)

2. LeetCode 刷题

- [获取生成数组中的最大值](https://leetcode-cn.com/problems/get-maximum-in-generated-array/)

3. 源码阅读

- [实现一个双向绑定](https://juejin.cn/post/6844903989083897870#heading-8)

4. 项目进度

## 2021.08.22

1. 文章阅读

- [React+Ts](https://segmentfault.com/a/1190000019962867)
- [React 优化子组件 render](https://segmentfault.com/a/1190000019141226?utm_source=sf-similar-article)

2. LeetCode 刷题

- [整数反转](https://leetcode-cn.com/problems/reverse-integer/)

3. 源码阅读

- [实现一个双向绑定](https://juejin.cn/post/6844903989083897870#heading-8)

4. 项目进度

## 2021.08.20

1. 文章阅读

- [CSS 单位及其需要注意的地方](https://www.jianshu.com/p/1ae437f68dcf)
- [最后一个单词的长度](https://leetcode-cn.com/problems/length-of-last-word/)

2. LeetCode 刷题

- [存在重复元素](https://leetcode-cn.com/leetbook/read/tencent/x5h4n3/)

3. 源码阅读

- [实现一个双向绑定](https://juejin.cn/post/6844903989083897870#heading-8)

4. 项目进度

## 2021.08.19

1. 文章阅读

- [【JS】深入理解事件循环](https://www.jianshu.com/p/096d4c3f79dc)
- [typescript 泛型](https://www.jianshu.com/p/02a437d60ea4)

2. LeetCode 刷题

- [两个数组的交集](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2y0c2/)

3. 源码阅读

- [实现一个双向绑定](https://juejin.cn/post/6844903989083897870#heading-8)

4. 项目进度

## 2021.08.18

1. 文章阅读

- [前端跨域的常见方式](https://www.jianshu.com/p/bebd84f22054)
- [不可错过的 react 面试题](https://www.jianshu.com/p/4357c4ae99eb)

2. LeetCode 刷题

- [实现 strStr()](https://leetcode-cn.com/problems/implement-strstr/)

3. 源码阅读

- [实现一个双向绑定](https://juejin.cn/post/6844903989083897870#heading-8)

4. 项目进度

## 2021.08.17

1. 文章阅读

- [Vue-router 中 hash 模式和 history 模式的区别](https://www.jianshu.com/p/6edac32ae647)
- [知识点整理之---跨域](https://www.jianshu.com/p/827b041be446)

2. LeetCode 刷题

- [字符串转换整数](https://leetcode-cn.com/leetbook/read/tencent/xxgggc/)

3. 源码阅读

- [实现一个双向绑定](https://juejin.cn/post/6844903989083897870#heading-8)

4. 项目进度

## 2021.08.16

1. 文章阅读

- [Vue --- 组件化](https://www.jianshu.com/p/59c2d7735f42)
- [知识点整理之---跨域](https://www.jianshu.com/p/827b041be446)

2. LeetCode 刷题

- [三数之和](https://leetcode-cn.com/leetbook/read/tencent/xxst6e/)

3. 源码阅读

- [reduce 实现原理](https://juejin.cn/post/6844903989083897870#heading-4)

4. 项目进度

## 2021.08.15

1. 文章阅读

- [web 前端面试](https://www.jianshu.com/p/a59874dbee58)
- [Vue 基础 g 知识(一) - 插值语法](https://www.jianshu.com/p/5286c9577cdd)

2. LeetCode 刷题

- [合并两个有序数组](https://leetcode-cn.com/leetbook/read/tencent/x5mohi/)

3. 源码阅读

- [reduce 实现原理](https://juejin.cn/post/6844903989083897870#heading-4)

4. 项目进度

## 2021.08.13

1. 文章阅读

- [12 个有用的 JavaScript 数组技巧](https://www.jianshu.com/p/651338c88bb4)
- [Vue2 和 Vue3 的区别](https://www.jianshu.com/p/d3f973433274)

2. LeetCode 刷题

- [寻找两个正序数组的中位数](https://leetcode-cn.com/leetbook/read/tencent/xx6c46/)

3. 源码阅读

- [bind 实现](https://juejin.cn/post/6844903989083897870#heading-1)

4. 项目进度

- [x] 完成首页

## 2021.08.12

1. 文章阅读

- [js 中的回调函数](https://juejin.cn/post/6995428262155386887)
- [创建虚拟 DOM 的两种方式](https://juejin.cn/post/6994971529045868580)

2. LeetCode 刷题

- [删除排序数组中的重复项](https://leetcode-cn.com/leetbook/read/tencent/xxynuj/)

3. 源码阅读

- [Object.create 的基本实现原理](https://juejin.cn/post/6844903989083897870#heading-9)

4. 项目进度

## 2021.08.11

1. 文章阅读

- [前端常用面试问题的疑难解惑](https://juejin.cn/post/6993719240029110309)
- [【vue】生命周期详解](https://juejin.cn/post/6993718053062049805)

2. 源码阅读
3. LeetCode 刷题

- [两数之和](https://leetcode-cn.com/leetbook/read/tencent/xxqfy5/)

4. 项目进度
