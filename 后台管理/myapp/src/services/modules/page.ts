import { PageData } from '@/types';
import { request } from 'umi';

// 获取页面管理数据
export function getPage(page = 1,params:{[key: string]: string|boolean}, pageSize = 12) {
  return request(`/api/page?page=${page}&pageSize=${pageSize}`, { params });
}

// 更改页面状态
export function updatePage(data:PageData) {
  return request(`/api/page`, {
    method: 'PATCH',
    data,
  });
}

// 删除页面
export function deletePage(id: string) {
  return request(`/api/page/${id}`, {
    method: 'DELETE',
  });
}
