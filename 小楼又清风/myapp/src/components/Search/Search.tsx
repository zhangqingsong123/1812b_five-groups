import React, { useState } from 'react';
import { useDispatch, useSelector, NavLink } from 'umi';
import { RegardsState } from '@/types';
import styles from './Search.less';
interface Search {
  Flag: boolean;
  setFlag: any;
}

const Search: React.FC<Search> = (props) => {
  let dispatch = useDispatch();

  const [keyword, setvalue] = useState('');

  function onChangeSearch(e: React.ChangeEvent<HTMLInputElement>) {
    setvalue(e.target.value);
  }
  let { searchItem } = useSelector((state: RegardsState) => state.regards);
  console.log(searchItem);

  function onChangeButton() {
    dispatch({
      type: 'regards/getSearch',
      payload: keyword,
    });
  }

  return (
    <div className={styles.Search}>
      <div className={styles.SearchBody}>
        {
          <div>
            <div className={styles.SearchHeader}>
              <span>文章搜索</span>
              <span onClick={(e) => props.setFlag(false)}>Esc X</span>
            </div>
            <div>
              <input
                type="text"
                className={styles.SearchInput}
                value={keyword}
                onChange={(e) => onChangeSearch(e)}
              />
              <button
                className={styles.SearchButton}
                onClick={(e) => onChangeButton()}
              >
                搜索
              </button>
            </div>
          </div>
        }
        <ul>
          {searchItem.length
            ? searchItem.map((item) => {
                return (
                  <NavLink to={`/regards/${item.id}`}>
                    <li onClick={() => props.setFlag(false)} className={styles.SearchItem}>{item.title}</li>
                  </NavLink>
                );
              })
            : ''}
        </ul>
      </div>
    </div>
  );
};

export default Search;
