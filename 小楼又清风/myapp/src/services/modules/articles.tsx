import { request } from 'umi';
export function getArticleList(page: number,pageSize=12,status="publish"){
    return request("/api/article",{
         params:{
             page,
             pageSize,
             status,
         },
         data:{},
    })
}
//获取推荐文章
export function getRecommend(id?: string){
    let queryString=""
    id && (queryString =`?articleId=${id}`)
   return request(`/api/article/recommend${queryString}`) 
}
export function getcoverbanner(){
    return request("/api/article/all/recommend")
}
// 详情接口

// export function getArticleDetail(id:string){
//     return request(`/api/article/${id}/views`,{
//        method: 'POST', 
//     })
// }
//获取文章评论
export function getArticleCommet(id:string,page=1,pageSize=6){
    return request(`/api/comment/host/${id}?page=${page}&pageSize=${pageSize}`)
}

 //获取git接口
 export function getArticleTag(articleStatus='publish'){
    return request('/api/tag',{
        params:{
            articleStatus
        }
    })
}
export function getArticleTagList(type:string,page=1,pageSize=12,status="publish"){
    return request(`/api/article/tag/${type}`,{
        params: {
            pageSize,
            page,
            status,
        }
    })
}


