import { request } from 'umi';

// 分页获取评论
export function getComment(page=1, params:{[key: string]: string|boolean}, pageSize=12){
    return request(`/api/comment?page=${page}&pageSize=${pageSize}`, { params})
}

// 更改评论状态（通过或拒绝）
export function updateComment(id:string, data:{[key:string]:boolean}={}){
    return request(`/api/comment/${id}`, {
        method: 'PATCH',
        data
    })
}

// 删除评论
export function deleteComment(id:string){
    return request(`/api/comment/${id}`, {
        method: 'DELETE'
    })
}
