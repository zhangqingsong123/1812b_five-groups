import React, { useState, useEffect } from 'react'
import { useDispatch, useSelector, useParams, NavLink } from 'umi'
import { withRouter, RouteComponentProps } from 'react-router-dom'
import styles from './index.less'

import moment from '../../../components/moment'
import { HeartOutlined, EyeOutlined, DeploymentUnitOutlined } from '@ant-design/icons';
import { IRootState } from '@/types'
import { Affix } from 'antd';
import classNames from 'classnames'
import RecomMended from '@/components/recommended'
import CategoryList from '@/components/CategoryList'





const Tag: React.FC<RouteComponentProps<{ tag: string }>> = (props) => {
  const dispatch = useDispatch()
  let tag = props.match.params.tag;

  const { ArticleTagList } = useSelector((state: IRootState) => state.article)
  console.log(ArticleTagList);

  useEffect(() => {
    dispatch({
      type: 'article/getArticleTagList',
      payload: tag
    })
  }, [tag])
  const { ArticleTag } = useSelector((state: IRootState) => state.article)
  useEffect(() => {
    dispatch({
      type: 'article/getArticleTag'
    })
  }, []);

  const [active] = useState(0)
  return (
    <div className={styles.home}>

      <div>
        <div>
          <div className={styles.left}>
            <div className={styles.lefttop}>
              <p>与 <span>{ArticleTag.filter(item => item.value === tag)[0]?.label}</span> 标签有关的文章</p>
              <p>共搜索到 <span>{ArticleTagList.length}</span> 篇</p>
            </div>
            <div className={styles.leftcenter}>
              <div className={styles.top}>
                <span>文章标签</span>
              </div>
              <ul className={styles.bottom}>
                {
                  ArticleTag.map((item, index) => {
                    return <li className={styles.li} key={item.id}>
                      <NavLink className={index === active ? 'active' : ''} to={'/article/tag/' + item.value}>{item.label} [{item.articleCount}]</NavLink>
                    </li>
                  })
                }
              </ul>
            </div>
            <div className={styles.leftbottom}>
              {
                ArticleTagList.map(item => {
                  return <div key={item.id} className={styles.bottomlist}>
                    <header className={styles.listtop}>
                      <NavLink to={`/regards/${item.id}`}>{item.title}</NavLink>
                      <div><span>超过{moment(item.createAt).fromNow()}</span></div>
                    </header>
                    <div className={styles.listbottom}>
                      <div className={styles.img}>
                        <img src={item.cover} alt="" />
                      </div>
                      <div className={styles.size}>
                        <p>{item.summary}</p>
                        <p className={styles.p}><span> <HeartOutlined /> {item.likes} ·</span><span> <EyeOutlined /> {item.views} ·</span><span> <DeploymentUnitOutlined /> 分享</span></p>
                      </div>
                    </div>
                  </div>
                })
              }

            </div>
          </div>


          <div className={styles.right}>
            <Affix>
              {/* 中间右边 */}
              <div className={classNames(styles.middle_right)}  >
                <div>
                  <RecomMended />
                </div>
                {/* 右侧列表 */}
                <div className={classNames(styles.rightlist)} >
                  <CategoryList />
                </div>
              </div>
            </Affix>
          </div>
        </div>
      </div>

    </div>
  )
}

export default withRouter(Tag)