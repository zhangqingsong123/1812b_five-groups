
import React, { useEffect } from 'react'
import { Link } from 'umi';
import styles from './index.less'
import { Layout, Row, Col, Slider } from 'antd';
import Tables from '../Tables';
import { observer } from 'mobx-react-lite'
import useStore from "@/context/useStore"
const Workbench: React.FC = (props) => {
    const DemoBox = (props: { value: any; children: boolean | React.ReactChild | React.ReactFragment | React.ReactPortal | null | undefined; }) => <p className={`height-${props.value}`}>{props.children}</p>;
    const { Header, Footer } = Layout;
    let store = useStore();
    console.log(store);

    useEffect(() => {
        store.Workbench.WorkbenchList()
    }, [])
    return (
        <div className={styles.canvas}>
            <div className={styles.nav_list}>
                <div className={styles.header}>
                    <div className={styles.nav}>快速导航</div>
                    <div className={styles.footer_navlink}>
                        <div>
                            <span><Link to="/visit/All">文章管理</Link></span>
                        </div>
                        <div>
                            <span><Link to="/comment">评论管理</Link></span>
                        </div>
                        <div>
                            <span><Link to="/file">文件管理</Link></span>
                        </div>
                        <div>
                            <span><Link to="/user">用户管理</Link></span>
                        </div>
                        <div>
                            <span><Link to="/article">访问管理</Link></span>
                        </div>
                        <div>
                            <span><Link to="/system">系统设置</Link></span>
                        </div>
                    </div>
                </div>
            </div>
             {/* 最新文章 */}
            <div className={styles.content_list}>
                <div className={styles.content}>
                    <div className={styles.article}>
                        <p>最新文章</p>
                        <p><Link to="/visit/All">全部文章</Link></p>
                    </div>
                    <div>
                        <div className={styles.rwo_list}>
                            {
                                store.Workbench.workbenchS.map(item => {
                                    return <div className={styles.rwo_lists} key={item.id}>
                                        <div>
                                            <h6>文章封面</h6>
                                            {item.cover && <img src={item.cover} alt="" />}
                                            <span>{item.title}</span>
                                        </div>

                                    </div>


                                })
                            }
                        </div>
                    </div>
                </div>
            </div>
            <div className={styles.footer}>
                <div className={styles.comment}>
                    <p>最新评论</p>
                    <p><Link to="/comment">全部评论</Link></p>
                </div>
                <div>
                    <Tables></Tables>
                </div>
            </div>
        </div>
    )
}

export default observer(Workbench)

