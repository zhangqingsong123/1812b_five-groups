import { RequestConfig } from 'umi';
import { message } from 'antd';
import { createLogger } from 'redux-logger';
import NProgress from 'nprogress';
import "../node_modules/nprogress/nprogress.css"
NProgress.configure({showSpinner: false ,trickleSpeed: 500 ,});
export const dva = {
  config: {
    onAction: createLogger(),
    onError(e: Error) {
      message.error(e.message, 3);
    },
  },
};

export function onRouteChange({ matchedRoutes }: any) {
  NProgress.start()
  let time = setTimeout(() => { //定时器
    clearTimeout(time)
    NProgress.done() //结束
  }, 2000)
}

const baseUrl = 'https://api.blog.wipi.tech';
export const request: RequestConfig = {
  timeout: 5000,
  errorConfig: {},
  middlewares: [],
  requestInterceptors: [
    (url, options) => {
      return {
        url: `${baseUrl}${url}`,
        options,
      };
    },
  ],
  responseInterceptors: [
    (response) => {
      const codeMaps: { [key: number]: string } = {
        400: '错误的请求',
        403: '禁止访问',
        404: '找不到资源',
        500: '服务器错误',
        502: '网关错误。',
        503: '服务不可用，服务器暂时过载或维护。',
        504: '网关超时。',
      };
      //处理网络请求错误
      if (Object.keys(codeMaps).indexOf(String(response.status)) != -1) {
        message.error(codeMaps[response.status]);
      }
      //处理业务逻辑错误
      // if(response.data.success!=true){
      //     message.error(response.data. )
      // }
      return response;
    },
  ],
};
