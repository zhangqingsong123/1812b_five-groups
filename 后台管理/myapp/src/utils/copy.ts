import copy from 'copy-to-clipboard';
import {message} from 'antd';

export function copyText(text:string){
    copy(text);
    message.success('内容已复制到剪切板');
}