import React, { useState } from 'react'
import { useLocation } from 'umi'
import { Layout } from 'antd';
import './index.less';
import Menus from '@/components/Menu';
import HeaderDefault from '@/components/Header';
import { getToken } from '@/utils';

const { Redirect } = require("dva").router;
const { Header, Content } = Layout;
const Layouts: React.FC = (props) => {

    let location = useLocation();
    let whileList = ["/login", "/redirect", "/sign", "/NewArticles"]
    if (whileList.indexOf(location.pathname) !== -1) {
        return <div className="body">{props.children}</div>
    } else {
        return (
            getToken() ? <Layout>
                <Menus />
                <Layout className="site-layout">
                    <Header className="site-layout-background" style={{ padding: 0 }}>
                        <HeaderDefault />
                    </Header>
                    <div className="content">
                        {props.children}
                    </div>
                </Layout>
            </Layout> : <Redirect to="/login" />
        )
    }
}

export default Layouts;