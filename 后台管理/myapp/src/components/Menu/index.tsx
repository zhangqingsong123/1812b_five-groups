import React, { useState } from 'react';
import { Layout, Menu, Dropdown, Button, Space } from 'antd';
import { NavLink } from 'umi';
import styles from './index.less';
import {
    FormOutlined,
    DashboardOutlined,
    CopyOutlined,
    TagsOutlined,
    DiffOutlined,
    SnippetsOutlined,
    FireOutlined,
    MessageOutlined,
    MailOutlined,
    FolderOpenOutlined,
    SearchOutlined,
    ProjectOutlined,
    UserOutlined,
    SettingOutlined,
} from '@ant-design/icons';

const { SubMenu } = Menu;
const { Sider } = Layout;

const Menus: React.FC = () => {
    let [router, setRouter] = useState([
        { name: "工作台", link: "/workbench", icon: DashboardOutlined },
        {
            name: "文章管理", icon: FormOutlined,
            children: [
                { name: "所有文章", link: "/article", icon: FormOutlined },
                { name: "分类管理", link: "/article/classify", icon: CopyOutlined },
                { name: "标签管理", link: "/article/label", icon: TagsOutlined }
            ]
        },
        { name: "页面管理", link: "/pageManagement", icon: DiffOutlined },
        { name: "知识小册", link: "/knowledge", icon: SnippetsOutlined },
        { name: "海报管理", link: "/poster", icon: FireOutlined },
        { name: "评论管理", link: "/comment", icon: MessageOutlined },
        { name: "邮件管理", link: "/mail", icon: MailOutlined },
        { name: "文件管理", link: "/file", icon: FolderOpenOutlined },
        { name: "搜索记录", link: "/search", icon: SearchOutlined },
        { name: "访问统计", link: "/visit", icon: ProjectOutlined },
        { name: "用户管理", link: "/user", icon: UserOutlined },
        { name: "系统设置", link: "/system", icon: SettingOutlined },
    ])
    const menu = (
        <Menu>
            <Menu.Item>
                <a target="_blank" rel="noopener noreferrer" href="https://www.antgroup.com">
                    1st menu item
                </a>
            </Menu.Item>
            <Menu.Item>
                <NavLink to="/NewArticles">新建文章</NavLink>
            </Menu.Item>
            <Menu.Item>
                <NavLink to="/NewPages">新建页面</NavLink>
            </Menu.Item>
        </Menu>
    );
    let [collapsed, setCollapsed] = useState(false)
    return (
        <Sider trigger={null} collapsible collapsed={collapsed} className={styles.Menu}>
            <Menu theme="dark" mode="inline" defaultSelectedKeys={['1']}>
                <Menu.Item className={styles.Header}><img src="https://bwcreation.oss-cn-beijing.aliyuncs.com/2021-07-26/logo2.4a842ec4.png" alt="" /><h2 className={styles.LayoutsHeader}>管理后台</h2></Menu.Item>
                <Space direction="vertical">
                    <Space wrap>
                        <Dropdown overlay={menu} placement="bottomCenter">
                            <Button className={styles.HeaderButton}>+ 新建</Button>
                        </Dropdown>
                    </Space>
                </Space>
                {
                    router.map((item, index) => {
                        return item.children ? <SubMenu key={index} title={item.name} icon={<item.icon />} className="subone">
                            {item.children.map((item2, index2) => {
                                return <NavLink to={item2.link} key={index2}><Menu.Item icon={<item2.icon />} className="suboneChildren">{item2.name}</Menu.Item></NavLink>
                            })}
                        </SubMenu>
                            : <NavLink to={item.link} key={index}><Menu.Item icon={<item.icon />}>
                                {item.name}
                            </Menu.Item></NavLink>
                    })
                }
            </Menu>
        </Sider>
    )
}

export default Menus;
