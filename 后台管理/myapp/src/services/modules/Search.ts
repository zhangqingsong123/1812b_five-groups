import {request} from 'umi'

//搜索内容列表
export function getsearchlist( page=1 ,params:{[key: string]: string|boolean}, pageSize = 12){
    return request(`/api/search?page=${page}&pageSize=${pageSize}`, { params})
}
// 搜索页面  删除搜索记录
export function deleteSearch(id:string){
    return request(`/api/search/${id}`,{
        method: 'DELETE',
    })
}
