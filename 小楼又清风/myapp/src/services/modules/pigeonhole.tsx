import { request } from "umi"

//归档首页左边数据
export function getPigeonhole() {
    return request('/api/article/archives')
}

//归档首页右下数据
export function getRight(articleStatus = "publish") {
    return request("/api/category", {
        params: {
            articleStatus
        },
        data: {

        }
    })
}

//详情页数据
export function getPigId(id: string) {
    return request(`/api/article/${id}/views`, {
        method: "POST"
    });
}

//归档首页右上数据
export function getPigRecommend() {
    return request("/api/article/recommend");
}