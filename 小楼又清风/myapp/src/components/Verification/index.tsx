import React, { useState } from 'react';
import './index.less';
import { Modal, Form, Select, Input } from 'antd';

interface visibleI {
  visible: boolean;
  setVisible: any;
}

const Verification: React.FC<visibleI> = (props) => {
  const { visible, setVisible } = props;
  const [username,setUsername]=useState("")
  const [userEmail,setEmail]=useState("")
  function Ok(){
    setVisible(false);
    localStorage.setItem("token",username);
    localStorage.setItem("email",userEmail);
  }
  function Cancle(){
    setVisible(false);
  }
  return (
    <div>
      <Modal
        title="请设置你的信息"
        visible={visible}
        onOk={() => Ok()}
        onCancel={() => Cancle()}
        okText="设置"
        cancelText="取消"
      >
        <Form>
          <Form.Item
            label="名称："
            name="username"
            rules={[{ required: true, message: '请输入您的称呼' }]}
          >
            <Input onChange={(e)=>{setUsername(e.target.value),console.log(e.target.value)}}/>
          </Form.Item>
          <Form.Item
            label="邮箱："
            name="email"
            rules={[
              {
                required: true,
                type: 'email',
                message: '输入合法邮箱地址，以便在收到回复时邮件通知',
              },
            ]}
          >
            <Input onChange={(e)=>{setEmail(e.target.value),console.log(e.target.value);
            }}/>
          </Form.Item>
        </Form>
      </Modal>
    </div>
  );
};

export default Verification;
