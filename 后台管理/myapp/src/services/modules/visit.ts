import {request} from 'umi'

//访问统计页面 内容列表
export function getvisitlist( page=1 , pageSize = 12){
    return request(`/api/view?page=${page}&pageSize=${pageSize}`)
}

// 访问统计页面 删除搜索记录
export function deleteVisit(id:string){
    return request(`/api/view/${id}`,{
        method: 'DELETE',
    })
}
