


 //文最新文章接口
export interface articleItem {
  id: string;
  title: string;
  cover?: string;
  summary?: any;
  content: string;
  html: string;
  toc: string;
  status: string;
  views: number;
  likes: number;
  isRecommended: boolean;
  needPassword: boolean;
  totalAmount?: string;
  isPay: boolean;
  isCommentable: boolean;
  publishAt: string;
  createAt: string;
  updateAt: string;
  tags: any[];
  category?: any;
}
//最新评论ts接口
export interface commentsItem  {
  id: string;
  name: string;
  email: string;
  content: string;
  html: string;
  pass: boolean;
  userAgent: string;
  hostId: string;
  url: string;
  parentCommentId: string;
  replyUserName: string;
  replyUserEmail: string;
  createAt: string;
  updateAt: string;
}

export interface commentitemList {
  id: string;
  name: string;
  email: string;
  content: string;
  html: string;
  pass: boolean;
  userAgent: string;
  hostId: string;
  url: string;
  parentCommentId: string;
  replyUserName: string;
  replyUserEmail: string;
  createAt: string;
  updateAt: string;
}
 //回复接口
export interface commentcontentItem {
  name: string;
  email: string;
  content: string;
  html: string;
  pass: boolean;
  userAgent: string;
  hostId: string;
  url: string;
  parentCommentId: string;
  replyUserName: string;
  replyUserEmail: string;
}