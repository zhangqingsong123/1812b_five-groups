

import useStore from '@/context/useStore'
import React, { useEffect, useState } from 'react'
import styles from './index.less'
import { Card, Pagination, Form, Input, Button, Drawer } from 'antd';
import { observer } from 'mobx-react-lite'
import moment from '../../utils/moment';
import { Fileitem } from '@/types';
import ImageView from '../ImageView';
import { copyText } from '../../utils/copy'
interface IForm {
    [key: string]: string | boolean
}
const File: React.FC = () => {
    const [visible, setVisible] = useState(false);
    let [page, setPage] = useState(1)
    const [params, setParams] = useState<IForm>({})
    let [direction, setDirection] = useState({} as Fileitem)  //传给useSate
    let [form] = Form.useForm()
    let store = useStore()
    const { Meta } = Card;
    useEffect(() => {
        store.file.getfileList(page, params)
    }, [page, params])

    function changePage(value: number) {
        setPage(value)
    }
    function submit() {
        let values = form.getFieldsValue();
        let params: IForm = {};
        for (let key in values) {
            values[key] && (params[key] = values[key]);
        }
        setParams(params);
    }

    function showDrawer(item: Fileitem) {
        console.log(item);
        setDirection(item)   //通过这个传值到Drawer

        setVisible(true);
    }
    function onClose() {
        setVisible(false);
    }
    function changeURL(direction: any) { //复制路径
        copyText(direction.url)
    }
    return (
        <div>

            <div className={styles.search}>
                <div className={styles.top}>
                    <Form className={styles.Form}
                        form={form}
                        onFinish={submit}
                    >
                        <Form.Item className={styles.formitem} name="originalname" label="文件名称">
                            <Input className={styles.Input} type="text" placeholder="请输入文件名称" />
                        </Form.Item>

                        <Form.Item className={styles.formitem} name="type" label="文件类型">
                            <Input className={styles.Input} type="text" placeholder="请输入文件类型" />
                        </Form.Item>

                        <Button className={styles.button} htmlType="submit">搜索</Button>
                        <Button className={styles.button1} htmlType="reset">重置</Button>
                    </Form>
                </div>
                <div className={styles.button1}>

                </div>
            </div>
            <div className={styles.search_list}>
                <div className={styles.searchImg}>
                    {
                        store.file.FileItemList.map((item, index) => {
                            return <Card
                                key={index}
                                className={styles.shezhi}
                                hoverable
                                style={{ width: 300 }}
                                onClick={() => showDrawer(item)} //传值 传他的每一项
                                cover={<img alt="example" src={item.url} />}
                            >

                                <Meta title={item.originalname} description={"上传于" + moment(item.createAt).format("YYYY, h:mm:ss")} />

                            </Card>


                        })
                    }

                    <Drawer title="文件信息" placement="right" onClose={onClose} visible={visible} width="600px">
                        <ImageView>
                            <div className={styles.Img}> <img src={direction.url} alt="" /></div>
                        </ImageView>
                        <div>
                            <p>文件名称:{direction.originalname}</p>
                            <p>存储路径:{direction.filename}</p>
                            <p>文件类型:{direction.type}</p>
                            <div>
                                <Form.Item className={styles.formitem} label="访问链接" >
                                    <Input onClick={() => changeURL(direction.url)} className={styles.Input} value={direction.url} type="text" placeholder="请输入文件名称" />
                                </Form.Item>
                                <button>复制</button>
                            </div>


                        </div>
                    </Drawer>


                </div>
                <div>   <Pagination defaultCurrent={page} total={store.file.getcountNumber} onChange={changePage} pageSize={12} /></div>
            </div>

        </div>



    )
}

export default observer(File)
