import { request } from "umi";

//路由页面
export function getArticleLabel(articleStatus='publish'){
    return request('/api/category',{
        params:{
            articleStatus
        }
    })
}

export function getArticleAllList(type:string,page=1,pageSize=12,status="publish"){
    return request(`/api/article/category/${type}`,{
        params: {
            pageSize,
            page,
            status,
        }
    })
}
