import useStore from '@/context/useStore';
import React, { Key, useEffect, useState } from 'react'
import { ConsumerList } from '@/types'
import { Form, Select, Input, Button, Table, Badge } from 'antd';
import { observer } from 'mobx-react-lite'
import { ReloadOutlined } from '@ant-design/icons';
import moment from '../../components/Moment/index'
import styles from "./index.less"
const { Option } = Select;

const { useForm } = Form;
interface IForm {
    [key: string]: string | boolean
}
const User: React.FC = () => {
    const [form] = useForm();
    const store = useStore();
    const [page, setPage] = useState(1);
    const [pageSize, setpageSize] = useState(12)
    const [params, setParams] = useState<IForm>({});
    const [selectedRowKeys, setSelectedRowKeys] = useState<Key[]>([]);
    const [selectedRows, setselectedRows] = useState<ConsumerList[]>([]);

    useEffect(() => {
        store.consumer.getconsumerlist(page, params, pageSize)
    }, [page, params, pageSize])
    
    function onSelectChange(selectedRowKeys: Key[], selectedRows: ConsumerList[]) {
        setselectedRows(selectedRows);
    }
    const rowSelection = {
        selectedRows,
        onChange: onSelectChange
    }
    const onPageChange = (pageNumber: number,pagesize: number) =>{
        setPage(pageNumber)
        setpageSize(pagesize)
    }

    function submit() {
        let values = form.getFieldsValue();
        console.log(values);
        let params: IForm = {};
        for (let key in values) {
            values[key] && (params[key] = values[key]);
        }
        setParams(params);
    }

    const columns = [
        {
            title: '账户',
            dataIndex: 'name',
        },
        {
            title: '邮箱',
            dataIndex: 'email',
        },
        {
            title: '角色',
            render: (row: ConsumerList) => {
                return row.role === 'admin' ? <span>管理员</span> : <span>访客</span>
            }
        },
        {
            title: '状态',
            render: (row: ConsumerList) => {
                return row.status === 'locked' ? <Badge status="success" text="已锁定" /> : <Badge status="warning" text="可用" />
            }
        },
        {
            title: '注册日期',
            render:(row: ConsumerList)=>{
                return <span>{moment(row.updateAt).format('YYYY/MM/DD hh:mm:ss')}</span>
            }
        },
        {
            title: '操作',
            render: (row: ConsumerList) => {
                return <p>
                    <Button onClick={() => { store.consumer.updateconsumer({ ...row, status: row.status === 'locked' ? 'active' : 'locked' }, page, params, pageSize) }}>
                        {row.status === 'locked' ? '启用' : '禁用'}
                    </Button>
                    <Button onClick={() => { store.consumer.updateconsumer({ ...row, role: row.role === 'admin' ? 'visitor' : 'admin' }, page, params, pageSize) }}>
                        {row.role === 'admin' ? '解除授权' : '授权'}
                    </Button>
                </p>
            }
        }
    ]
    return (
        <div>
            <div className={styles.userBox}>
                <div className={styles.userbox}>
                    <div className={styles.usertop}>
                        <Form form={form} onFinish={submit}>
                            <div className={styles.userleft}>
                                <Form.Item name="user" label="账户：" className={styles.user}>
                                    <Input type="text" placeholder="请输入用户账户" />
                                </Form.Item>
                                <Form.Item name="email" label="邮箱：" className={styles.user}>
                                    <Input type="text" placeholder="请输入账户邮箱" />
                                </Form.Item>
                                <Form.Item name="role" label="角色：" className={styles.user}>
                                    <Select className={styles.users}>
                                        <Option value="admin">管理员</Option>
                                        <Option value="visitor">访客</Option>
                                    </Select>
                                </Form.Item>
                                <Form.Item name="state" label="状态：" className={styles.user}>
                                    <Select className={styles.users}>
                                        <Option value="warning">锁定</Option>
                                        <Option value="success">可用</Option>
                                    </Select>
                                </Form.Item>
                            </div>
                            <div className={styles.userright}>
                                <Button htmlType="submit">搜索</Button>
                                <Button htmlType="reset">重置</Button>
                            </div>
                        </Form>
                    </div>
                    <div className={styles.userbottom}>
                        <div className={styles.userbottombox}>
                            {selectedRows.length ? <section>
                                <Button onClick={() => {selectedRows.forEach(item=>item.status='active')}}>启用</Button>
                                <Button onClick={() => {selectedRows.forEach(item=>item.status='locked')}}>禁用</Button>
                                <Button onClick={() => {selectedRows.forEach(item=>item.status='admin')}}>授权</Button>
                                <Button onClick={() => {selectedRows.forEach(item=>item.status='visitor')}}>解除授权</Button>
                            </section> : null}
                            <span></span>
                            <ReloadOutlined />
                        </div>
                        <Table 
                        rowSelection={rowSelection} 
                        columns={columns} 
                        dataSource={store.consumer.consumerList} 
                        rowKey="id"
                        pagination={{
                            total:store.consumer.consumerCount,
                            defaultPageSize:pageSize,
                            pageSizeOptions:['8','12','24','36'], 
                            showTotal:total=>`共${total}条`,
                            onChange:(pageNumber,pagesize)=>{
                                onPageChange(pageNumber,pagesize!)
                            }
                        }}
                         ></Table>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default observer(User)
