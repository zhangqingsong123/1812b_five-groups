import HighLight from '@/components/HighLight';
import ImageView from '@/components/ImageView';
import { IRootState } from '@/types';
import { fomatTime } from '@/utils';
import React, { useEffect, useState } from 'react';
import {IRouteComponentProps, useDispatch, useSelector} from 'umi'

const ArticleDetail: React.FC<IRouteComponentProps<{id:string}>> = props=>{
    let id = props.match.params.id;
    console.log('id..', id);
    const dispatch = useDispatch();
    const {articleDetail, articleComment} = useSelector((state:IRootState)=>state.article)
    const [commentPage, setCommentPage] = useState(1);

    useEffect(()=>{
        dispatch({
            type: 'article/getRegardsRecommend',
            payload: id
        })
    }, [])

    useEffect(()=>{
        dispatch({
            type: 'article/getArticleComment',
            payload: {
                id,
                page: commentPage
            }
        })
    }, [commentPage]);
    if (!Object.keys(articleDetail).length){
        return null;
    }
    return <div>
        <ImageView>
            {articleDetail.cover && <img src={articleDetail.cover} />}
            <h1>{articleDetail.title}</h1>
            <p>
                发布于{fomatTime(articleDetail.publishAt!)}•阅读量{articleDetail.views} 
            </p>
            <HighLight>
                <div dangerouslySetInnerHTML={{__html: articleDetail.html!}}></div>
            </HighLight>
            <p>发布于{fomatTime(articleDetail.publishAt!)} | 版权信息：非商用-署名-自由转载</p>
        </ImageView>
    </div>
}

export default ArticleDetail;
