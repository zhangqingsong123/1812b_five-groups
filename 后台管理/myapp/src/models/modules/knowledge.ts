import { getKnowledgeList, deleteKnowledgeCard } from "@/services"
import { IKnowledgeItem } from "@/types"
import { makeAutoObservable } from "mobx"
import { message } from "antd"
class Knowledge {
    knowledgeList: Array<IKnowledgeItem> = [];
    knowledgeNum: number = 0
    constructor() {
        makeAutoObservable(this)
    }

    async getKnowledgeList(page: number, pageSize: number,params:Partial<IKnowledgeItem>={}) {
        let result = await getKnowledgeList(page, pageSize,params)
        if (result.statusCode === 200) {
            this.knowledgeList = result.data[0]
            this.knowledgeNum = result.data[1]
        }
    }
    //删除
    async deleteKnowledgeCard(id: string[]) {
        Promise.all(id.map(id => deleteKnowledgeCard(id))).then(res => {
            message.success('删除成功');
            this.getKnowledgeList(1, 12);
        })
    }
}
export default Knowledge