import React from 'react';
import './index.less';
import { Row, Card, Input, Button, Col } from 'antd';

const Classify: React.FC = () => {
  return (
      <Row className="classify">
        <Col className="ant-col-md-9">
          <Card title="添加分类" bordered={false} style={{ width: 300 }}>
            <div className="ipt">
              <Input type="text" placeholder="输入分类名称" />
            </div>
            <div className="ipt">
              <Input type="text" placeholder="输入分类值(请输入英文,作为路由使用)"/>
            </div>
            <Button type="primary">保存</Button>
          </Card>
        </Col>
        <Col className="ant-col-md-15">
          <Card title="所有分类" bordered={false}>
            <Button size="small">Toggle</Button>
          </Card>
        </Col>
      </Row>
  );
};

export default Classify;
