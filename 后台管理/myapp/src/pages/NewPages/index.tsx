import React, { useState } from 'react'
import Editor from 'for-editor'
import styles from "./index.less"
import { message } from "antd"
const NewArticles: React.FC = () => {
    const [content, setContent] = useState("")
    const [title, setTitle] = useState("")
    function submit() {
        if (!title) {
            message.warn("请输入文章标题");
            return
        }

    }
    const toolbar = {
        h1: true, // h1
        h2: true, // h2
        h3: true, // h3
        h4: true, // h4
        img: true, // 图片
        link: true, // 链接
        code: true, // 代码块
        preview: true, // 预览
        expand: true, // 全屏
        /* v0.0.9 */
        undo: true, // 撤销
        redo: true, // 重做
        save: true, // 保存
        /* v0.2.3 */
        subfield: true, // 单双栏模式
    }
    return (
        <div>
            <header>
                <button>删除</button>
                <input type="text" value={title} onChange={e => setTitle(e.target.value)} />
                <button onClick={submit}>发布</button>
            </header>
            <section className={styles.Editor}>
                <Editor
                    value={content}
                    onChange={value => setContent(value)}
                    toolbar={toolbar}
                />
            </section>
        </div>
    )
}

export default NewArticles