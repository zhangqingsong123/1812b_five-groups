export interface SystemSetting {
  id: string;
  i18n: string;
  systemUrl: string;
  systemTitle: string;
  systemLogo: string;
  systemFavicon: string;
  systemFooterInfo: string;
  adminSystemUrl: string;
  baiduAnalyticsId: string;
  googleAnalyticsId?: any;
  seoKeyword: string;
  seoDesc: string;
  oss: string;
  smtpHost: string;
  smtpPort: string;
  smtpUser: string;
  smtpPass: string;
  smtpFromUser: string;
  createAt: string;
  updateAt: string;
}