import { IBrochureItem ,IRecommendRead,BrochureDetail,BrochureLeftDetail,BrochureLeftDetailBottom,KonwledgeDetail} from '@/types';
import { Effect, Reducer } from 'umi';
import { getBrochureList, getRecommendRead ,getrecommend,getBrochureDetail,getBrochureLeftDetailList,getKonwledgeDetail} from '@/services';

export interface BrochureModelState {
  recommend: IBrochureItem[];
  brochureList: IBrochureItem[];
  recommendread: IRecommendRead[];
  brochureCount: number;
  brochureDetail:BrochureDetail[];
  BrochureLeftDetailListBottom:BrochureLeftDetailBottom[];
  knowledgedetails:Partial<KonwledgeDetail>;
}

export interface BrochureModelType {
  namespace: 'brochure';
  state: BrochureModelState;
  // 异步action，相当于vuex中的action
  effects: {
    getBrochureList: Effect;
    getrecommend: Effect;
    getRecommendRead: Effect;
    getBrochureDetail:Effect;
    getBrochureLeftDetailList:Effect;
    getKonwledgeDetail:Effect;
  };
  // 同步action，相当于vuex中的mutation
  reducers: {
    save: Reducer<BrochureModelState>;
  };
}

const BrochureModel: BrochureModelType = {
  namespace: 'brochure',
  state: {
    // 知识小册 --> 右侧列表
    brochureList: [],
    // 知识小册 --> 左侧列表 第一个列表（文章分类）
    recommend: [],
    // 知识小册 --> 左侧列表 第二个列表（推荐阅读）
    recommendread: [],
    brochureCount: 0,

    // 知识小册 -->详情右侧列表
    brochureDetail:[],
    // 知识小册 -->详情左侧列表 下面列表
    BrochureLeftDetailListBottom:[],
    // 开始阅读详情列表
    knowledgedetails:{},
  },

  effects: {
    *getrecommend({payload}, { call, put }) {
      let result = yield call(getrecommend);
      console.log('result...', result);
      if (result.success === true) {
        yield put({
          type: 'save',
          payload: {recommend: result.data}
        })
      }
    },
    *getBrochureList({ payload }, { call, put }) {
      let result = yield call(getBrochureList, payload);
      console.log('result...', result);
      if (result.success === true) {
        yield put({
          type: 'save',
          payload: {
            brochureList: result.data[0],
            brochureCount: result.data[1]
          }
        })
      }
    },

    *getRecommendRead({ payload }, { call, put }) {
      let result = yield call(getRecommendRead, payload);
      console.log(result)
      if (result.success === true) {
        yield put({
          type: 'save',
          payload: { recommendread: result.data }
        })
      }
    },
    *getBrochureDetail({ payload }, { call, put }) {
      let result = yield call(getBrochureDetail, payload);
      console.log('result...', result);
      if (result.success === true) {
        yield put({
          type: 'save',
          payload: {
            brochureDetail: result.data[0],
          }
        })
      }
    },
    *getBrochureLeftDetailList({ payload }, { call, put }) {
      let result = yield call(getBrochureLeftDetailList, payload);
      console.log('result...', result);
      if (result.success === true) {
        yield put({
          type: 'save',
          payload: {
            BrochureLeftDetailListBottom:result.data.children,
          }
        })
      }
    },
    // 第二层跳详情
    *getKonwledgeDetail({ payload }, { call, put }) {
      let result = yield call(getKonwledgeDetail, payload);
      console.log('result...', result);
      if (result.success === true) {
        yield put({
          type: 'save',
          payload: {
            knowledgedetails: result.data,
          }
        })
      }
    },
  },
  reducers: {
    save(state, action) {
      return {
        ...state,
        ...action.payload,
      };
    },
  }
};

export default BrochureModel;