import React, { useState, useEffect, createRef, Children } from 'react';
import styles from './index.less';
import { IRootState, RegardsState } from '@/types';
import moment from 'moment';
import CommentIpt from '../Comment';
import { useDispatch, useSelector } from 'umi';
import { Pagination, message } from 'antd';
import classNames from 'classnames';
import Expression from '../Expression';

interface Component {
  id: string;
  count?: number;
  url: string;
}

const Comment: React.FC<Component> = (props) => {
  console.log(props);
  let [page, setPage] = useState(1);
  let dispatch = useDispatch();
  let [CommentValue, setCommentValue] = useState('');
  const { messageList } = useSelector((state: IRootState) => state.message);
  const { IsComment } = useSelector((state: RegardsState) => state.regards);
  const [emoticon, setemoticon] = useState(false)

  let ref = createRef<HTMLUListElement>();


  useEffect(() => {
    console.log(props.id);
    dispatch({
      type: 'message/getMessageList',
      payload: { page, id: props.id },
    });    
  }, [page]);

  function changeComment(e: React.ChangeEvent<HTMLTextAreaElement>) {
    setCommentValue(e.target.value);
  }

  function WriteBack(
    e: React.MouseEvent<HTMLSpanElement, MouseEvent>,
    index: number,
    index2?: number,
  ) {
    if (typeof index2 !== 'number') {
      ref.current?.children[
        index
      ].children[0].lastElementChild?.children[2].classList.toggle(
        styles.msgboard_banner_hidden,
      );
    } else {
      ref.current?.children[
        index
      ].children[1].children[1].children[2].classList.toggle(
        styles.msgboard_banner_hidden,
      );
    }
  }

  function Answer(index: number, index2?: number) {
    let name = localStorage.getItem('token');
    let email = localStorage.getItem('email');
    if (typeof index2 !== 'number') {
      console.log(messageList[index], '111111111111111');
      dispatch({
        type: 'regards/postComment',
        payload: {
          content: CommentValue,
          email: email,
          name: name,
          hostId: Number(new Date()) * 1,
          url: props.url,
          parentCommentId: messageList[index].id,
          replyUserEmail: messageList[index].email,
          replyUserName: messageList[index].name,
        },
      });
      console.log(IsComment);
      if (JSON.stringify(IsComment) != "{}") {
        message.success("发布成功,等待管理员审核");
        setCommentValue("")
      }
    } else {
      dispatch({
        type: 'regards/postComment',
        payload: {
          content: CommentValue,
          email: email,
          name: name,
          hostId: Number(new Date()) * 1,
          url: props.url,
          parentCommentId: messageList[index].children[index2].id,
          replyUserEmail: messageList[index].children[index2].email,
          replyUserName: messageList[index].children[index2].name,
        },
      });
      if (JSON.stringify(IsComment) != "{}") {
        message.success("发布成功,等待管理员审核");
        setCommentValue("")
      }
    }
  }

  return (
    <div className={styles.msgboard}>
      <div className={styles.msgboard_banner}>
        <div className={styles.msgboard_banner_padding}>
          <div className={styles.msgboard_banner_title}>
            <span>评论</span>
          </div>
          <CommentIpt url={props.url} />
          <ul className={styles.msgboard_banner_comment} ref={ref}>
            {messageList &&
              messageList.map((item, index) => {
                return (
                  <li
                    key={index}
                    className={styles.msgboard_banner_comment_item}
                  >
                    <div
                      className={styles.msgboard_banner_comment_item_comment}
                    >
                      <div className={styles.msgboard_banner_comment_title}>
                        <span
                          className={
                            styles.msgboard_banner_comment_item_two_icon
                          }
                          style={{
                            backgroundColor:
                              '#' +
                              Math.floor(Math.random() * 0xffffff).toString(16),
                          }}
                        >
                          {item.name.slice(0, 1)}
                        </span>
                        <span>{item.name}</span>
                      </div>
                      <div className={styles.msgboard_banner_comment_main}>
                        <p
                          dangerouslySetInnerHTML={{ __html: item.content }}
                        ></p>
                        <p
                          className={
                            styles.msgboard_banner_comment_main_content
                          }
                        >
                          {item.userAgent} · {moment(item.createAt).fromNow()}{' '}
                          <span
                            className={
                              styles.msgboard_banner_comment_main_content_icon
                            }
                            onClick={(e) => WriteBack(e, index)}
                          >
                            {' '}
                            <svg
                              viewBox="64 64 896 896"
                              focusable="false"
                              data-icon="message"
                              width="1em"
                              height="1em"
                              fill="currentColor"
                              aria-hidden="true"
                            >
                              <path d="M464 512a48 48 0 1096 0 48 48 0 10-96 0zm200 0a48 48 0 1096 0 48 48 0 10-96 0zm-400 0a48 48 0 1096 0 48 48 0 10-96 0zm661.2-173.6c-22.6-53.7-55-101.9-96.3-143.3a444.35 444.35 0 00-143.3-96.3C630.6 75.7 572.2 64 512 64h-2c-60.6.3-119.3 12.3-174.5 35.9a445.35 445.35 0 00-142 96.5c-40.9 41.3-73 89.3-95.2 142.8-23 55.4-34.6 114.3-34.3 174.9A449.4 449.4 0 00112 714v152a46 46 0 0046 46h152.1A449.4 449.4 0 00510 960h2.1c59.9 0 118-11.6 172.7-34.3a444.48 444.48 0 00142.8-95.2c41.3-40.9 73.8-88.7 96.5-142 23.6-55.2 35.6-113.9 35.9-174.5.3-60.9-11.5-120-34.8-175.6zm-151.1 438C704 845.8 611 884 512 884h-1.7c-60.3-.3-120.2-15.3-173.1-43.5l-8.4-4.5H188V695.2l-4.5-8.4C155.3 633.9 140.3 574 140 513.7c-.4-99.7 37.7-193.3 107.6-263.8 69.8-70.5 163.1-109.5 262.8-109.9h1.7c50 0 98.5 9.7 144.2 28.9 44.6 18.7 84.6 45.6 119 80 34.3 34.3 61.3 74.4 80 119 19.4 46.2 29.1 95.2 28.9 145.8-.6 99.6-39.7 192.9-110.1 262.7z"></path>
                            </svg>
                            回复
                          </span>
                        </p>
                        <div
                          className={classNames(
                            styles.msgboard_banner_inp,
                            styles.msgboard_banner_hidden,
                          )}
                        >
                          <CommentIpt url={props.url} />
                        </div>
                      </div>
                    </div>
                    {item.children !== null
                      ? item.children.map((item2, index2) => {
                        return (
                          <div
                            key={index}
                            className={
                              styles.msgboard_banner_comment_item_comment_two
                            }
                          >
                            <div
                              className={
                                styles.msgboard_banner_comment_title_two
                              }
                            >
                              <span
                                style={{ display: 'block' }}
                                onClick={(e) => WriteBack(e, index, index2)}
                              >
                                <span style={{ fontWeight: 700 }}>
                                  <span
                                    className={
                                      styles.msgboard_banner_comment_item_three_icon
                                    }
                                    style={{
                                      backgroundColor:
                                        '#' +
                                        Math.floor(
                                          Math.random() * 0xffffff,
                                        ).toString(16),
                                      marginRight: '10px',
                                    }}
                                  >
                                    {item2.name.slice(0, 1)}
                                  </span>
                                  {item2.name}
                                </span>
                                  &ensp;回复&ensp;
                                  <span style={{ fontWeight: 700 }}>
                                  {item.name}
                                </span>
                              </span>
                            </div>
                            <div
                              className={
                                styles.msgboard_banner_comment_main_two
                              }
                            >
                              <p
                                className={
                                  styles.msgboard_banner_comment_main_two_content
                                }
                                dangerouslySetInnerHTML={{
                                  __html: item2.content,
                                }}
                              ></p>
                              <p
                                className={
                                  styles.msgboard_banner_comment_main_content_two
                                }
                              >
                                {item2.userAgent} ·{' '}
                                {moment(item2.createAt).fromNow()}
                                <span
                                  className={
                                    styles.msgboard_banner_comment_main_content_icon_two
                                  }
                                  onClick={(e) => WriteBack(e, index, index2)}
                                >
                                  <Expression/>
                                </span>
                              </p>
                              <div
                                className={classNames(
                                  styles.msgboard_banner_inp,
                                  styles.msgboard_banner_hidden,
                                )}
                              >
                                <textarea
                                  name=""
                                  id=""
                                  placeholder={' 回复 ' + item2.name}
                                  onChange={(e) => changeComment(e)}
                                ></textarea>
                                <footer>
                                  <Expression/>
                                  <button
                                    onClick={() => Answer(index, index2)}
                                  >
                                    发布
                                    </button>
                                </footer>
                              </div>
                            </div>
                          </div>
                        );
                      })
                      : ''}
                  </li>
                );
              })}
          </ul>
        </div>
      </div>
      {/* 分页 */}
      <div className={styles.PaginationI}>
        <Pagination
          defaultCurrent={1}
          total={props.count}
          onChange={(page) => {
            setPage(page), console.log(page);
          }}
        />
      </div>
    </div>
  );
};
export default Comment;