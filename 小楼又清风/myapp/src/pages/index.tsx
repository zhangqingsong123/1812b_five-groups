import styles from './index.less';
import React from 'react';
import {useDispatch} from 'umi';

export default function IndexPage() {
  let dispatch=useDispatch()
  return (
    <div>
      <h1 className={styles.title}>Page index</h1>
    </div>
  );
}
