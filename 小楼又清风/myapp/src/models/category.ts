import { getArticleAllList, getArticleLabel } from '@/services/modules/categoty';
import { ArticlecategoryItem, ArticleLabel, } from '@/types';
import { Effect, ImmerReducer, Reducer, Subscription } from 'umi';

export interface categoryModelState {
    ArticleLabel: ArticleLabel[];
    ArticleAll: ArticlecategoryItem[];
    page: number;
    pageSize: number,
    status: string
}

export interface categoryModelType {
    namespace: 'category';
    state: categoryModelState;
    //处理异步action
    effects: {
        getArticleLabel: Effect;
        getArticleAllList: Effect;
    };
    //   同步action
    reducers: {
        save: Reducer<categoryModelState>;
        // 启用 immer 之后
        // save: ImmerReducer<IndexModelState>;
    };
    //   subscriptions: { setup: Subscription };
}

const categoryModel: categoryModelType = {
    namespace: 'category',

    state: {
        ArticleLabel: [],
        ArticleAll: [],
        page: 1,
        pageSize: 12,
        status: "publish"
    },

    effects: {
        *getArticleLabel({ payload }, { call, put }) {
            let result = yield call(getArticleLabel)
            if (result.success === true) {
                yield put({
                    type: "save",
                    payload: {
                        ArticleLabel: result.data
                    }
                })
            }
        },
        *getArticleAllList({ payload }, { call, put }) {
            let result = yield call(getArticleAllList, payload)
            if (result.success === true) {
                yield put({
                    type: "save",
                    payload: {
                        ArticleAll: result.data[0]
                    }
                })
            }
        }
    },
    reducers: {
        save(state, action) {
            return {
                ...state,
                ...action.payload,
            };
        },
        // 启用 immer 之后
        // save(state, action) {
        //   state.name = action.payload;
        // },
    },
};

export default categoryModel;

function* getArticleDetai(getArticleDetai: any): any {
    throw new Error('Function not implemented.');
}
