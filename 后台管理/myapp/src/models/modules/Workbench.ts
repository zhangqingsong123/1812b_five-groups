import { commentsItemList, DeleteComment, Updatecomment, WorkbenchList } from "@/services"
import { articleItem, commentcontentItem, commentsItem } from "@/types"
import { makeAutoObservable,runInAction } from "mobx"
import {message} from 'antd';
class Workbench{
    workbenchS:articleItem[]=[];
    commentsItem:commentsItem[]=[]
    commentCount: number = 0;
  constructor() {
      makeAutoObservable(this)
  }
  async WorkbenchList(){
    let result=await WorkbenchList()
    if(result.data){
      runInAction(()=>{

        this.workbenchS=result.data[0]
      })
    }
  }
  async commentsItemList(page=1){
    let result=await commentsItemList(page)
    console.log(result,'11111111111111111111111111111111');
    if(result.data){
      runInAction(()=>{
        this.commentCount=result.data[1]
        this.commentsItem=result.data[0]
      })
    }
  }
   //通过和拒绝
   async Updatecomment(ids:string[],status:{[key:string]:boolean}){
    message.loading("操作中",0) 
   Promise.all(ids.map(id=>Updatecomment(id,status))) 
   .then(res=>{
    //  console.log(res);
     
       message.destroy()
       message.success("操作成功")
      
     this.commentsItemList() //每次点击页面的时候更新一次

   }) 
   .catch(error=>{
       message.destroy()
       message.error("操作失败")
   })
 }
 //删除接口
 async DeleteComment(ids:string[]){
   message.loading("操作中",0) 
  Promise.all(ids.map(id=>DeleteComment(id))) 
  .then(res=>{
      message.destroy()
      message.success("操作成功")
      this.commentsItemList() //每次点击页面的时候更新一次
  }) 
  .catch(error=>{
      message.destroy()
      message.error("操作失败")
  })
}
// async addcomment(id:string,content:{[key:string]:boolean}){
//    let result=await addcomment(id,content)
//    console.log(result,'111111111111111111111111111111111');
   
//     if(result.data){
//     this.commentcontent=result.data
//     }
// }




}
export default Workbench