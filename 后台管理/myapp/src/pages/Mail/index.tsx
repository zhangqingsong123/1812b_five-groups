import React, { useEffect, useState, Key } from 'react'
import styles from "./index.less"
import { Form, Input, Button, Table, TablePaginationConfig } from 'antd'
import useStore from "@/context/useStore"
import { observer } from 'mobx-react-lite';
import { MailListItem } from '@/types/mail';
const columns = [
  {
    title: '发件人',
    dataIndex: 'from',
  },
  {
    title: '收件人',
    dataIndex: 'to',
  },
  {
    title: '主题',
    dataIndex: 'subject',
  },
  {
    title: '发送时间',
    dataIndex: 'createAt',
  },
  {
    title: '操作',
    render: () => {
      return <span>删除</span>;
    },
  }
];
const { useForm } = Form;
const Mail: React.FC = () => {
  const [form] = useForm();
  const store = useStore();
  let [page, setPage] = useState("1");
  let [pageSize, setPageSize] = useState("12");
  const [selectedRowKeys, setSelectedRowKeys] = useState<Key[]>([]);
  function onReset() {
    form.resetFields();
  };
  function submit() {
    let values = form.getFieldsValue();
    store.mail.getMailList({ page, pageSize, from: values.from, to: values.to, subject: values.subject })
    console.log(values)
  }
  useEffect(() => {
    store.mail.getMailList({ page, pageSize })
  }, [page])

  function onSelectChange(selectedRowKeys: Key[], items: MailListItem[]) {
    setSelectedRowKeys(selectedRowKeys);
  }
  const rowSelection = {
    selectedRowKeys,
    onChange: onSelectChange
  }

  return (
    <div>
      <div className={styles.MailHeader}>
        <span>工作台 / 邮件管理</span>
      </div>
      <main className={styles.MailMain}>
        {/* <div className={styles.MailCenter}>
                    <span>系统检测到SMTP配置未完善，当收到评论时，无法进行邮件通知。点我立即完善</span>
                </div> */}
        <div className={styles.MailSearch}>
          <Form form={form} onFinish={submit}>
            <div className={styles.MailSearchTop}>
              <Form.Item label="发件人" name="from" className={styles.MailSearchTopItem}>
                <Input type="text" placeholder="请输入发件人" />
              </Form.Item>
              <Form.Item label="收件人" name="to" className={styles.MailSearchTopItem}>
                <Input type="text" placeholder="请输入收件人" />
              </Form.Item>
              <Form.Item label="主题" name="subject" className={styles.MailSearchTopItem}>
                <Input type="text" placeholder="请输入主题" />
              </Form.Item>
            </div>
            <div className={styles.MailSearchBottom}>
              <Button type="primary" htmlType="submit">
                搜索
                            </Button>
              <Button htmlType="button" onClick={onReset}>
                重置
                            </Button>
            </div>
          </Form>
        </div>
        <div className={styles.MailTable}>
          {selectedRowKeys.length == store.mail.MailList.length ? <Button danger>删除</Button> : ""}
          <Table rowSelection={rowSelection}
            columns={columns}
            dataSource={store.mail.MailList}
            rowKey="id"
            pagination={{ pageSize: 12, total: store.mail.count }}
            onChange={(e) => { setPage(String(e.current)) }}
          />
        </div>
      </main>
    </div>
  );
};

export default observer(Mail);
