
import {request} from 'umi'
//获取搜索数据
export function fileList(page=1,params:{[key:string]:string | boolean},pageSize=12){
    return request(`/api/file?page=${page}&pageSize=${pageSize}`,{params})
}

