import { RegardsModelState } from '@/models/regards';
import { PigeonholeModelState } from '@/models/pigeonhole'
import { BrochureModelState } from "@/models/brochure";
import { ArticleModelState } from "@/models/article";
import { MessageModelState } from '@/models/message';
import { categoryModelState } from '@/models/category';

export interface IRootState {
    messageList: any;
    brochure: BrochureModelState
}
export interface RootState {
    pigeonhole: PigeonholeModelState
}
export interface RegardsState {
    regards: RegardsModelState
    
}

export interface IRootState {
    article: ArticleModelState
}
export interface IRootState {
    message: MessageModelState;
  }
  
export interface essaystate{
   category: categoryModelState
}
