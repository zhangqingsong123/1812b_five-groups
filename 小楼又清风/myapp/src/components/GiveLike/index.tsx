import { useSelector } from '@/.umi/plugin-dva/exports'
import { RootState } from '@/types'
import React from 'react'
import styles from '../../pages/regards/regards.less'

export default function GiveLike() {
    const pigeonhole = useSelector((state: RootState) => state.pigeonhole)

    return (
        <div className={styles.headline}>
            <div>
                <span>
                    <div className={styles._1qU35tL8S}>
                        <b>♥</b>
                    </div>
                    <div className={styles.ants}>
                        <span>
                            <p>{pigeonhole.recommend[0].likes}</p>
                        </span>
                    </div>
                </span>
            </div>
            <div>
                <span>
                    <div className={styles._1qU35tL8S}>
                        <b>✉</b>
                    </div>
                    <div className={styles.ants}>

                    </div>
                </span>
            </div>
            <div>
                <span>
                    <div className={styles._1qU35tL8S}>
                        <b>✈</b>
                    </div>
                    <div className={styles.ants}>

                    </div>
                </span>
            </div>
        </div>
    )
}
