import React from 'react'
import styles from './index.less'
import { Tabs, Form, Input } from 'antd'
const { TabPane } = Tabs;
let { useForm } = Form
const System: React.FC = () => {
    const [form] = useForm();
    return (
        <div>
            <div className={styles.container}>
                <div className={styles.tab}>
                    <Tabs tabPosition="left">
                        <TabPane tab="系统设置" key="1">
                            <Form
                            form={form}
                            >
                                <Form.Item>
                                    <p>系统设置</p>
                                    <Input placeholder="请输入系统地址" />
                                </Form.Item>
                                <Form.Item>
                                <p>后台地址</p>
                                    <Input placeholder="请输入后台地址" />
                                </Form.Item>
                                <Form.Item>
                                <p>系统标题</p>
                                    <Input placeholder="请输入系统标题" />
                                </Form.Item>
                                <Form.Item>
                                <p>Logo</p>
                                    <Input placeholder="请输入Logo" />
                                </Form.Item>
                                <Form.Item>
                                <p>Favicon</p>
                                    <Input placeholder="请输入Favicon" />
                                </Form.Item>
                                <Form.Item>
                                <p>页脚信息</p>
                                    <Input.TextArea rows={8} placeholder="请输入页脚信息" />
                                </Form.Item>
                                <Button></Button>
                            </Form>
                        </TabPane>
                        <TabPane tab="国际化设置" key="2">
                            Content of Tab 2
                        </TabPane>
                        <TabPane tab="SEO设置" key="3">
                        <Form
                            form={form}
                            >
                                <Form.Item>
                                    <p>关键词</p>
                                    <Input placeholder="请输入关键词" />
                                </Form.Item>
                                <Form.Item>
                                <p>描述信息</p>
                                <Input.TextArea rows={8} placeholder="请输入页脚信息" />
                                </Form.Item>
                               
                            </Form>
                        </TabPane>
                        <TabPane tab="数据统计" key="4">
                        <Form
                            form={form}
                            >
                                <Form.Item>
                                    <p>百度统计</p>
                                    <Input placeholder="请输入百度统计" />
                                </Form.Item>
                                <Form.Item>
                                <p>谷歌分析</p>
                                <Input placeholder="请输入谷歌分析" />
                                </Form.Item>
                               
                            </Form>
                        </TabPane>
                        <TabPane tab="OSS设置" key="5">
                            Content of Tab 2
                        </TabPane>
                        <TabPane tab="SMTP服务" key="6">
                        <Form
                            form={form}
                            >
                                <Form.Item>
                                    <p>SMTP 地址</p>
                                    <Input placeholder="请输入系统地址" />
                                </Form.Item>
                                <Form.Item>
                                <p>SMTP 端口（强制使用 SSL 连接）</p>
                                    <Input placeholder="请输入后台地址" />
                                </Form.Item>
                                <Form.Item>
                                <p>SMTP 用户</p>
                                    <Input placeholder="请输入系统标题" />
                                </Form.Item>
                                <Form.Item>
                                <p>SMTP 密码</p>
                                    <Input placeholder="请输入Logo" />
                                </Form.Item>
                                <Form.Item>
                                <p>发件人</p>
                                    <Input placeholder="请输入Favicon" />
                                </Form.Item>
                                
                            </Form>
                        </TabPane>
                    </Tabs>
                </div>
            </div>

        </div>
    )
}

export default System
