/* eslint valid-jsdoc: "off" */

'use strict';

/**
 * @param {Egg.EggAppInfo} appInfo app info
 */
module.exports = appInfo => {
  /**
   * built-in config
   * @type {Egg.EggAppConfig}
   **/
  const config = exports = {};

  // use for cookie sign key, should change to your own and keep security
  config.keys = appInfo.name + '_1631083001651_9383';

  // add your middleware config here
  config.middleware = [];

  // add your user config here
  const userConfig = {
    // myAppName: 'egg',
    security: {
      csrf: {
        enable: false
      }
    },
    cors: {
      origin: '*',
      allowMethods: 'GET,HEAD,PUT,POST,DELETE,PATCH,OPTIONS'
<<<<<<< HEAD
<<<<<<< HEAD
    },
    multipart:{
      whitelist:['.*','.js','.ts','.png','.jpg']
=======
>>>>>>> 411520e5822646c6c4d7393142610ebd5c528d64
=======
    },
    multipart:{
      whitelist:[".*",".js",".ts",".png",".pdf"]
>>>>>>> 75cc6e59369b6e352361eaeb9703e809670d1961
    }
  };

  return {
    ...config,
    ...userConfig,
  };
};
