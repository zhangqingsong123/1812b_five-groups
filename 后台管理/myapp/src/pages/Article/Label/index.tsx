import React from 'react';
import './index.less';
import { Row, Card, Input, Button, Col } from 'antd';

const Label: React.FC = () => {
    return (
      <Row className="label">
        <Col className="ant-col-md-9">
          <Card title="添加标签" bordered={false} style={{ width: 300 }}>
            <div className="ipt">
              <Input type="text" placeholder="输入标签名称" />
            </div>
            <div className="ipt">
              <Input
                type="text"
                placeholder="输入标签值(请输入英文,作为路由使用)"
              />
            </div>
            <Button type="primary">保存</Button>
          </Card>
        </Col>
        <Col className="ant-col-md-15">
          <Card title="所有标签" bordered={false}>
            <Button size="small">Toggle</Button>
          </Card>
        </Col>
      </Row>
    )
}

export default Label;
