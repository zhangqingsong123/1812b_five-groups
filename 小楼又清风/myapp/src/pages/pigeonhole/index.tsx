import React from 'react'
import { RootState } from '@/types/models'
import styles from "./pigeonhole.less"
import RecomMended from '../../components/recommended/index'
import { useState, useEffect } from 'react'
import { NavLink, useDispatch, useSelector } from 'umi'
import { BackTop } from 'antd'

export default function Brouchure() {
    let [page, setPage] = useState(1);
    const dispatch = useDispatch();
    const pigeonhole = useSelector((state: RootState) => state.pigeonhole)
    console.log(pigeonhole.pigeonholeList)
    let [arr, serarr] = useState(["fe", "be", "reading", "linux", "leetcode", "news"])

    useEffect(() => {
        dispatch({
            type: 'pigeonhole/getPigeonhole'
        })
    }, []);
    useEffect(() => {
        dispatch({
            type: 'pigeonhole/getPigRecommend',
            payload: page
        })
    }, [page]);
    useEffect(() => {
        dispatch({
            payload: page,
            type: 'pigeonhole/getRight'
        })
    }, [page]);


    return (
        <div className={styles.pigeonhole}>
            <BackTop />
            <div className={styles.container}>
                <div className={styles.center}>
                    <div className={styles.left}>
                        <div className={styles.connact}>
                            <div className={styles.toop}>
                                <h1 className={styles.h1}>归档</h1>
                                <h3 className={styles.h3}>共计<b>33</b>篇</h3>
                            </div>
                            {
                                Object.keys(pigeonhole.pigeonholeList).sort((a, b) => {
                                    return Number(b) - Number(a)
                                }).map((val, idx) => {
                                    return <div key={val}>
                                        <h1 className={styles.h2}>{val}</h1>
                                        {
                                            Object.keys(pigeonhole.pigeonholeList[val]).map((v, i) => {
                                                return (
                                                    <div className={styles.lefts} key={v}>
                                                        <h3>{v}</h3>
                                                        <ul>
                                                            {
                                                                (pigeonhole.pigeonholeList as any)[val][v].map((item: any, index: number) => {
                                                                    return (
                                                                        <li className={styles.lis} key={index}>
                                                                            <NavLink to={`/regards/${item.id}`}><span>{item.createAt.slice(5, 10)}</span><h2>{item.title}</h2></NavLink>
                                                                        </li>
                                                                    )
                                                                })
                                                            }
                                                        </ul>
                                                    </div>
                                                )
                                            })
                                        }
                                    </div>
                                })
                            }
                        </div>
                    </div>
                    <div className={styles.right}>
                        <div className={styles.sticky}>
                            <RecomMended />
                            <div className={styles.RightBottom}>
                                <h4>文章分类</h4>
                                <ul>
                                    {
                                        pigeonhole.RightList.map((item, index) => {
                                            return <NavLink to={`/article/category/${arr[index]}`} key={index}><span>{item.label}</span><span>共记{item.articleCount}篇文章</span></NavLink>
                                        })
                                    }
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <strong className="site-back-top-basic"></strong>
        </div>
    );
}