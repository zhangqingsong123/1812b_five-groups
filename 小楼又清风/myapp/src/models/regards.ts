import { Effect, ImmerReducer, Reducer, Subscription } from 'umi';
import { ContentItem, RecommendItem, DetailItem, SearchItem, CommentValue } from '@/types';
import { getContent, getComment, getRegardsRecommend, getRegardsDetail, getSearch, postComment } from '@/services';

export interface RegardsModelState {
  regards: any;
  data: Array<ContentItem>;
  count: number;
  recommend: Array<RecommendItem>;
  regardsDetail: Partial<DetailItem>;
  searchItem: Array<SearchItem>;
  IsComment:Partial<CommentValue>;
  // SearchValue:{}
}

export interface RegardsModelType {
  namespace: 'regards';
  state: RegardsModelState;
  effects: {
    getContent: Effect;
    getComment: Effect;
    getRegardsRecommend: Effect;
    getRegardsDetail: Effect;
    getSearch: Effect;
    postComment: Effect;
  };
  reducers: {
    save: Reducer<RegardsModelState>;
    // 启用 immer 之后
    // save: ImmerReducer<IndexModelState>;
  };
}

const RegardsModel: RegardsModelType = {
  namespace: 'regards',
  state: {
    regards: "",
    data: [],
    count: 0,
    recommend: [],
    regardsDetail: {},
    searchItem: [],
    IsComment: {}
  },
  //异步操作
  effects: {
    //内容
    *getContent({ payload }, { call, put }) {
      let result = yield getContent(payload.id)
      if (result.success === true) {
        yield put({
          type: "save",
          payload: { data: result.data }
        })
      }
    },
    //评论
    *getComment({ payload }, { call, put }) {
      let comment = yield getComment(payload.page,payload.pageSize,payload.id)
      if (comment.success === true) {
        yield put({
          type: "save",
          payload: { comment: comment.data[0], count: comment.data[1] }
        })
      }
    },
    //推荐阅读
    *getRegardsRecommend({ payload }, { call, put }) {
      let result = yield getRegardsRecommend();
      if (result.success === true) {
        yield put({
          type: "save",
          payload: { recommend: result.data }
        })
      }
    },
    //详情
    *getRegardsDetail({ payload }, { call, put }) {
      let result = yield getRegardsDetail(payload)
      if (result.success === true) {
        yield put({
          type: "save",
          payload: { regardsDetail: result.data }
        })
      }
    },
    //搜索框
    *getSearch({ payload }, { call, put }) {
      let result = yield getSearch(payload)
      yield put({
        type: "save",
        payload: { searchItem: result.data }
      })
    },
    //回复
    *postComment({ payload }, { call, put }) {
      let result = yield postComment(payload.content, payload.email, payload.name, payload.hostId,payload.url,payload.parentCommentId,payload.replyUserEmail,payload.replyUserName);
      console.log(result.data);
      if (result.success === true) {
        yield put({
          type: "save",
          payload: { IsComment: result.data }
        })
      }
    }
  },
  //同步操作
  reducers: {
    save(state, action) {
      console.log(action);
      return {
        ...state,
        ...action.payload,
      };
    },
    // 启用 immer 之后
    // save(state, action) {
    //   state.name = action.payload;
    // },
  },

};
export default RegardsModel;