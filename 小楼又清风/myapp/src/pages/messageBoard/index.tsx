import React, { useState } from 'react';
import styles from './index.less';
import Recommended from './Recommended';
import { IRootState } from '@/types/models';
import { useSelector } from 'umi';
import CommentList from '../../components/CommentList/CommentList';

const MessageBoard: React.FC = () => {
  let message = useSelector((state: IRootState) => state.message);
  let [id, setId] = useState("8d8b6492-32e5-44e5-b38b-9a479d1a94bd");
  let [url,seturl]=useState("/page/msgboard")

  return (
    <div className={styles.messageBoard}>
      {/* 留言板 */}
        <div className="container">
          <div className={styles.containerI}>
            <div className="markdown">
              <h2
                style={{
                  textAlign: 'center',
                  border: 0,
                  margin: 0,
                }}
              >
                留言板
            </h2>
              <p style={{ textAlign: 'center', margin: 0 }}>
                <strong>请勿灌水 🤖</strong>
              </p>
            </div>
          </div>
        </div>
        <div className={styles.center}>
        <CommentList id={id} count={message.messageCount} url={url}/>
        {/* 推荐阅读 */}
        <Recommended />
      </div>
    </div>
  );
};

export default MessageBoard;
