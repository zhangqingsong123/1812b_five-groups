import { RootArray, RootPigId, RootObject, Roots, RootRead } from '@/types';
import { Effect, Reducer } from 'umi';
import { getPigeonhole, getPigId, getPigRecommend, getRight } from '@/services';

export interface PigeonholeModelState {
  recommend: Roots[];
  pigeonholeList: { [key: string]: RootObject[] };
  RightList: RootArray[];
  pigId: { [key: string]: RootPigId[] };
  pigeonholeCount: number;
}

export interface PigeonholeModelType {
  namespace: 'pigeonhole';
  state: PigeonholeModelState;
  // 异步action，相当于vuex中的action
  effects: {
    getPigeonhole: Effect;
    getPigRecommend: Effect;
    getRight: Effect;
    getPigId: Effect;
  };
  // 同步action，相当于vuex中的mutation
  reducers: {
    save: Reducer<PigeonholeModelState>;
  };
}

const PigeonholeModel: PigeonholeModelType = {
  namespace: 'pigeonhole',
  state: {
    recommend: [],
    RightList: [],
    pigId: {},
    pigeonholeList: {},
    pigeonholeCount: 0,
  },

  effects: {
    *getPigeonhole({ payload }, { call, put }) {
      let result = yield call(getPigeonhole);
      console.log('result...', result);
      if (result.success === true) {
        yield put({
          type: 'save',
          payload: {
            pigeonholeList: result.data,
          },
        });
      }
    },
    *getPigRecommend({ payload }, { call, put }) {
      let result = yield call(getPigRecommend);
      console.log('result.............', result);
      if (result.success === true) {
        yield put({
          type: 'save',
          payload: {
            recommend: result.data,
          },
        });
      }
    },
    *getRight({ payload }, { call, put }) {
      let result = yield call(getRight);
      console.log('result.............', result);
      if (result.success === true) {
        yield put({
          type: 'save',
          payload: {
            RightList: result.data
          },
        });
      }
    },
    *getPigId({ payload }, { call, put }) {
      let result = yield call(getPigId, payload);
      console.log('result.............', result);
      if (result.success === true) {
        yield put({
          type: 'save',
          payload: {
            pigId: result.data,
          },
        });
      }
    },
  },
  reducers: {
    save(state, action) {
      return {
        ...state,
        ...action.payload,
      };
    },
  },
};

export default PigeonholeModel;
