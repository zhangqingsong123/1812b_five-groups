import { getMessageList, getrecommendReadList } from '@/services';
import { MessageItem, recommendReadItem } from '@/types';
import { Effect, ImmerReducer, Reducer, Subscription } from 'umi';
//模块的状态
//接口的类型
export interface MessageModelState {
  // recommend: IArticleItem[];
  //分页接口
  messageList: MessageItem[];
  messageCount: number;
  // 推荐接口
  recommendReadList: recommendReadItem[];
}

export interface messageModelType {
  namespace: 'message';
  state: MessageModelState;
  //异步action,相当于vue action
  effects: {
    //分页
    getMessageList: Effect;
    getrecommendReadList: Effect;
  };
  //同步action 相当于vuex 里面mutation
  reducers: {
    save: Reducer<MessageModelState>;
    // 启用 immer 之后
    // save: ImmerReducer<IndexModelState>;
  };
}

const MessageModel: messageModelType = {
  namespace: 'message',

  state: {
    messageList: [],
    messageCount: 0,
    recommendReadList: [],
  },

  effects: {
    // 评论列表
    *getMessageList({ payload }, { call, put }) {
      let result = yield call(getMessageList, payload);
      console.log('...result', result);
      if (result.success === true) {
        yield put({
          type: 'save',
          payload: {
            messageList: result.data[0],
            messageCount: result.data[1],
          },
        });
      }
    },
    // 推荐阅读
    *getrecommendReadList({ payload }, { call, put }) {
      let result = yield getrecommendReadList();
      console.log('...result', result);
      if (result.success === true) {
        yield put({
          type: 'save',
          payload: {
            recommendReadList: result.data,
          },
        });
      }
    },
  },
  reducers: {
    save(state, action) {
      return {
        ...state,
        ...action.payload,
      };
    },
  },
};

export default MessageModel;
