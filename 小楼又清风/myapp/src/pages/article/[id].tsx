
import HighLight from '@/components/HighLight';
import { IRootState } from '@/types';
import moment from 'moment';
import React, { useEffect, useState } from 'react';
import { IRouteComponentProps, useDispatch, useSelector } from 'umi';
import styles from './detail/detail.less'
const Article: React.FC<IRouteComponentProps<{ id: string }>> = (props) => {
   let id = props.match.params.id
   let { articleDetail, articleComment } = useSelector((state: IRootState) => state.article)
   let dispatch = useDispatch()
   let [commentPage, setcomment] = useState(1)
   //详情页
   useEffect(() => {
      dispatch({
         type: "article/getArticleDetail",
         payload: id
      })
   }, [])
   // 评论列表
   useEffect(() => {
      dispatch({
         type: "article/getArticleCommet",
         payload: {
            id,
            page: commentPage,
         }
      })
   }, [commentPage])
   if (!Object.keys(articleDetail).length) {
      return null
   }
   return <div className={styles.articledetail}>
      <div className={styles.containers}>
         <img src={articleDetail.cover} alt="" />
         <h1>{articleDetail.title}</h1>

         <p>
            发布{moment(articleDetail.publishAt).toNow()}.阅读量{articleDetail.views}
         </p>
         <HighLight>
            <div dangerouslySetInnerHTML={{ __html: articleDetail.html! }}></div>
         </HighLight>
      </div>
      
   </div>
}
export default Article


