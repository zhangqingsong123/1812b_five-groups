
import User from './modules/user';
import Mail from './modules/mail';
import Search from './modules/search';
import Visit from './modules/visit';
import Consumer from './modules/consumer';
import Comment from './modules/comment';
import Article from './modules/article';
import Workbench from './modules/Workbench';
import File from './modules/File';
import Page from './modules/Page';
import Knowledge from './modules/knowledge';
export default {
  user: new User(),
  mail: new Mail(),
  search: new Search(), //搜索记录
  visit: new Visit(), //访问统计
  consumer: new Consumer(), //用户管理
  comment: new Comment(),
  knowledge: new Knowledge(),
  article: new Article(),
  Workbench: new Workbench(), // 工作台
  file: new File(), // 文件管理
  pageI: new Page(),
};
