import React from 'react'
import { IRootState } from '../../../types/models';
import { useEffect, useState } from 'react';
import { RouteComponentProps } from "react-router-dom"
import { useDispatch, useSelector } from 'umi';
import { BrochureLeftDetailBottom } from '../../../types/brochure'
import moment from '@/components/moment'
import HighLight from '../../../components/HighLight'
import Backtop from '../../../components/BackTop/backtop'
import Comment from '../../../components/Comment/index'

import styles from '../../brochure/KonwledgeDetail.less'

interface State {
    title: string,
    parentNodeId: string
    item: BrochureLeftDetailBottom
}
interface Params {
    id: string
}
interface Context {

}
interface Props extends RouteComponentProps<Params, Context, State> {
    title: string,
    parentNodeId: string
    item: BrochureLeftDetailBottom
}
const BrochureDetail: React.FC<Props> = (props) => {
    let [list, setList] = useState([]);
    const dispatch = useDispatch();
    const { knowledgedetails, BrochureLeftDetailListBottom } = useSelector((state: IRootState) => state.brochure);
    const id = props.match.params.id;
    const locationState = props.location.state
    useEffect(() => {
        dispatch({
            type: 'brochure/getKonwledgeDetail',
            payload: id,
        })
    }, [id])
    useEffect(() => {
        dispatch({
            type: 'brochure/BrochureLeftDetailListBottom',
            payload: locationState.parentNodeId
        })
    }, [locationState.parentNodeId])

    useEffect(() => {
        knowledgedetails.toc && setList(JSON.parse(knowledgedetails.toc))
    }, [knowledgedetails])
    return (
        <div className={styles.detailBox}>
            <Backtop />
            <div className={styles.Detail_box} >
                
                <div className={styles.Detail_Left}>
                    <div>
                        <p>
                            <span onClick={() => props.history.push(`/brochure`)}>知识小册/</span>
                            <span > {locationState.title}/</span>
                            {
                                BrochureLeftDetailListBottom.map((item, index) => {
                                    return item.id === id && <span key={index} >{item.title}</span>
                                })
                            }
                        </p>
                    </div>
                    <div className={styles.Detail_LeftBox}>
                        <div className={styles.LeftBoxTop}>
                            <h1>
                                <b>{knowledgedetails.title}</b>
                            </h1>
                            <p>
                                <i>
                                    <span>发布于<time>{moment(knowledgedetails.createAt).format("YYYY-MM-DD HH:mm:ss")}</time></span>
                                    <span> • </span>
                                    <span>阅读量 {knowledgedetails.views}</span>
                                </i>
                            </p>
                        </div>
                        <HighLight>
                            <div dangerouslySetInnerHTML={{ __html: knowledgedetails.html! }}></div>
                        </HighLight>
                        <p>发布于{moment(knowledgedetails.publishAt).format("YYYY-MM-DD HH:mm:ss")} | 版权信息：非商用-署名-自由转载</p>
                        <Comment url=""/>
                    </div>
                </div>
                <div className={styles.Detail_Right}>
                    <div className={styles.Detail_RightBox}>
                        <div className={styles.Detail_RightBoxTop}>
                            <b>{locationState.title}</b>
                            <div className={styles.Detail_RightBoxbox}>
                                {
                                    BrochureLeftDetailListBottom.map((item, index) => {
                                        return <ul key={index}>
                                            <li onClick={() => {
                                                props.history.push({
                                                    pathname: `/brochure/BrochureDetail/${item.id}`,
                                                    state: {
                                                        title: item.title,
                                                        parentNodeId: item.id,
                                                        item: item,
                                                    }
                                                })
                                            }}>{item.title}</li>
                                        </ul>
                                    })
                                }
                            </div>
                        </div>
                        <div className={styles.Detail_RightBoxBottom}>
                            <p>
                                <b>目录</b>
                            </p>
                            <div className={styles.directory_title}>
                                {
                                    list.map((item: any, index) => {
                                        return <div key={index}>
                                            {
                                                item.level === "2" ? <div style={{ paddingLeft: "12px" }} className="level3box"><li>{item.text}</li> </div> : ""
                                            }
                                            {
                                                item.level === "3" ?
                                                    <div className="level_left">
                                                        <div style={{ paddingLeft: "20px" }}>
                                                            <li className={styles.small}>{item.text}</li>
                                                        </div>
                                                    </div> : ""
                                            }
                                        </div>
                                    })
                                }
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}
export default BrochureDetail
