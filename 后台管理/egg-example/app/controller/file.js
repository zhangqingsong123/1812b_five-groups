'use strict';
const fs= require('fs')
const path= require('path')
const savePath = path.resolve(__dirname, '../public');

const Controller = require('egg').Controller;

class FileController extends Controller {
    async saveChunk() {
        const { ctx } = this;
        let stream = await ctx.getFileStream();
        let { index } = stream.fields;
        try {
            let chunkDitPath = path.join(savePath, "/chunk");
            if (!fs.existsSync(chunkDitPath)) {
                fs.mkdirSync(chunkDitPath)
            }
            let filePath = path.join(chunkDitPath, `/chunk_${index}`);
            let file = fs.createWriteStream(filePath);
            stream.pipe(file);
            ctx.body = { "msg": "文件切片存储成功" }
        } catch (err) {
            ctx.body = { "msg": "文件存储失败" }
        }
    }
    async mergeChunk() {
        const { ctx } = this;
        let { total, filename, chunkname } = ctx.request.body;
        let index = 1;
        try {
            fs.writeFileSync(path.join(savePath, `/${filename}`), '');
            while (total >= index) {
                fs.appendFileSync(path.join(savePath, `/${filename}`), fs.readFileSync(path.join(savePath, `/${chunkname}/chunk_${index}`)))
                index++
            }
            ctx.body={"msg":"合并成功"}
        } catch (err) {
            console.log(err);
            ctx.body={
                "msg":"合并失败",
            }
        }

    }
}

module.exports = FileController;