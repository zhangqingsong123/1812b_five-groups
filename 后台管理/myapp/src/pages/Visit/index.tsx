import React, { Key, useEffect, useState } from 'react'
import useStore from '@/context/useStore';
import { Form, Input, Button, Badge, Table } from 'antd';
import { VisitList } from '@/types';
import { observer } from 'mobx-react-lite'
import { ReloadOutlined } from '@ant-design/icons';
import moment from '../../components/Moment'
import styles from "./index.less";

const { useForm } = Form;
interface IForm {
    [key: string]: string | boolean
}
const Visit: React.FC = () => {
    const [form] = useForm();
    const store = useStore();
    const [page, setPage] = useState(1);
    const [pageSize, setpageSize] = useState(12)
    const [params, setParams] = useState<IForm>({});
    const [selectedRowKeys, setSelectedRowKeys] = useState<Key[]>([]);
    const [selectedRows, setselectedRows] = useState<VisitList[]>([]);

    useEffect(() => {
        store.visit.getvisitlist(page, pageSize)
    }, [page, pageSize])

    function onSelectChange(selectedRowKeys: Key[], selectedRows: VisitList[]) {
        setselectedRows(selectedRows);
    }
    const rowSelection = {
        selectedRows,
        onChange: onSelectChange
    }
    const onPageChange = (pageNumber: number,pagesize: number) =>{
        setPage(pageNumber)
        setpageSize(pagesize)
    }


    function submit() {
        let values = form.getFieldsValue();
        console.log(values);
        let params: IForm = {};
        for (let key in values) {
            values[key] && (params[key] = values[key]);
        }
        setParams(params);
    }
    const columns:any = [
        {
            title: 'URL',
            width: 300,
            key: 'url',
            fixed: 'left',
            render: (row: VisitList) => {
                return <a href={row.url}>{row.url}</a>
            }
        },
        {
            title: 'IP',
            dataIndex: 'ip',
        },
        {
            title: '浏览器',
            dataIndex: 'browser',
        },
        {
            title: '内核',
            dataIndex: 'engine',
        },
        {
            title: '操作系统',
            dataIndex: 'os',
        },
        {
            title: '设备',
            dataIndex: 'device',
        },
        {
            title: '地址',
            dataIndex: 'address',
        },
        {
            title: '访问量',
            render: (row: VisitList) => {
                return <Badge count={row.count} style={{ backgroundColor: "#52c41a" }}></Badge>
            }
        },
        {
            title: '访问时间',
            render: (row: VisitList) => {
                return <span>{moment(row.updateAt).format('YYYY/MM/DD hh:mm:ss')}</span>
            }
        },
        {
            title: '操作',
            width: 200,
            key: 'url',
            fixed: 'right',
            render: (row: VisitList) => {
                return <p>
                    <Button onClick={() => store.visit.deleteVisit([row.id],page, pageSize)}>删除</Button>
                </p>
            }
        }
    ]
    return (
        <div className={styles.visitBox}>
            <div className={styles.visitbox}>
                <div className={styles.visittop}>
                    <Form form={form} onFinish={submit}>
                        <div className={styles.visitleft}>
                            <Form.Item name="ip" label="IP：" className={styles.visit}>
                                <Input type="text" placeholder="请输入IP地址" />
                            </Form.Item>
                            <Form.Item name="userAgent" label="UA：" className={styles.visit}>
                                <Input type="text" placeholder="请输入User Agent" />
                            </Form.Item>
                            <Form.Item name="url" label="URL：" className={styles.visit}>
                                <Input type="text" placeholder="请输入URL" />
                            </Form.Item>
                            <Form.Item name="address" label="地址：" className={styles.visit}>
                                <Input type="text" placeholder="请输入地址" />
                            </Form.Item>
                            <Form.Item name="browser" label="浏览器：" className={styles.visit}>
                                <Input type="text" placeholder="请输入浏览器" />
                            </Form.Item>
                            <Form.Item name="engine" label="内核：" className={styles.visit}>
                                <Input type="text" placeholder="请输入内核" />
                            </Form.Item>
                            <Form.Item name="os" label="OS：" className={styles.visit}>
                                <Input type="text" placeholder="请输入操作系统" />
                            </Form.Item>
                            <Form.Item name="device" label="设备：" className={styles.visit}>
                                <Input type="text" placeholder="请输入设备" />
                            </Form.Item>
                        </div>
                        <div className={styles.visitright}>
                            <Button htmlType="submit">搜索</Button>
                            <Button htmlType="reset">重置</Button>
                        </div>
                    </Form>
                </div>
                <div className={styles.visitbottom}>
                    <div className={styles.visitbottombox}>
                        {selectedRowKeys.length ? <section>
                            <Button onClick={() => store.search.deleteSearch(selectedRowKeys as string[],page, params, pageSize)}>删除</Button>
                        </section> : null}
                        <span></span>
                        <ReloadOutlined />
                    </div>
                    <Table 
                    rowSelection={rowSelection} 
                    columns={columns} 
                    dataSource={store.visit.visitlList} 
                    scroll={{ x: 2000 }}
                    rowKey="id"
                    pagination={{
                        total:store.visit.visitCount,
                        defaultPageSize:pageSize,
                        pageSizeOptions:['8','12','24','36'], 
                        showTotal:total=>`共${total}条`,
                        onChange:(pageNumber,pagesize)=>{
                            onPageChange(pageNumber,pagesize!)
                        }
                    }}
                     ></Table>
                </div>
            </div>
        </div>
    )
}

export default observer(Visit)



