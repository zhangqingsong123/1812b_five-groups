import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector, NavLink } from 'umi';
import { RegardsState } from '@/types';
import { Pagination } from 'antd';
import CommentList from "../../components/CommentList/CommentList"
import styles from './regards.less';
import Recommended from '../messageBoard/Recommended';
const Regards: React.FC = () => {
  
  let dispatch = useDispatch();
  const regards = useSelector((state: RegardsState) => state.regards);
  let [id, setId] = useState("a5e81ffe-0ad0-4be9-acca-c0462b1b98a1")

  useEffect(() => {
    dispatch({
      type: 'regards/getContent',
      payload: id,
    });
  }, []);

  useEffect(() => {
    dispatch({
      type: 'regards/getComment',
      payload: { id },
    });
  }, []);

  useEffect(() => {
    dispatch({
      type: 'regards/getRegardsRecommend',
      payload: '',
    });
  }, []);


  return (
    <div>
      <div className="_2eTSIHniNOTwaUvnm4wn7-">
        <div className="container">
          <div className={styles._2L8foJ_rg3AcilvSKA2iq7}>
            <img
              src="https://wipi.oss-cn-shanghai.aliyuncs.com/2020-04-04/despaired-2261021_1280.jpg"
              alt="文章封面"
            />
          </div>
          <div className={styles._1TDituBtews_ULM98bSKdc}>
            <div className={styles.markdown}>
              <h2>
                这世界只有一种英雄主义，就是在看清了生活的真相之后，依然热爱生活。
              </h2>
            </div>
          </div>
        </div>
        <div className={styles._2eTSIHniNOTwaUvnm4wn7}>
          <div className={styles._10wT67E6AAzqcEqLI7jUbZ}>
            <div className={styles.N1BwmgIkZZbNoeNN_FRbs}>
              <CommentList id={id} count={regards.count} url="/page/msgboard" />
            </div>
          </div>
        </div>
      </div>
      <Recommended />
    </div>
  );
};

export default Regards;
