import {request} from 'umi'
//工作台
//文章
export function WorkbenchList(page=1,pageSize=6){
  return request(`/api/article?page=${page}&pageSize=${pageSize}`)
}
export function commentsItemList(page=1,pageSize=6){
 return request(`/api/comment?page=${page}&pageSize=${pageSize}`)
}

export function Updatecomment(id:string,data:{[key:string]:boolean}={}){
  return request(`/api/comment/${id}`,{
      method: 'PATCH',
      data
  })
}
//回复接口
// /api/comment
// export function addcomment(id:string,data:{[key:string]:boolean}={}){
//    return request(`/api/comment${id}`,{
//      method: 'POST',
//      data
//    })
// }
export function DeleteComment(id:string){
    return request(`/api/comment/${id}`,{
       method: 'DELETE' 
    })
}