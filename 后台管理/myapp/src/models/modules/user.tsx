import { Login, Sign } from '@/services';
import { makeAutoObservable } from 'mobx';
import { UserItem, SignItem } from '@/types';
import { setToken, removeToken } from '@/utils/index';
import Cookie from 'js-cookie';
class User {
    userInfo = {};
    isSign = false;
    constructor() {
        makeAutoObservable(this);
    }
    async Login(data: UserItem) {
        let result = await Login(data.name, data.password)
        Cookie.set("name", result.data.name, { expires: 1 });
        console.log(result, "1111");
        if (result.data) {
            this.userInfo = result.data;
            setToken(result.data.token)
        }
        return result.data
    }
    logout() {
        this.userInfo = {};
        removeToken();
    }
    async Sign(data: SignItem) {
        let result = await Sign(data.name, data.password, data.confirm);
        console.log(result);
        this.isSign = result ? true : false
    }
}



export default User;