import React from 'react';
import styles from './index.less';
import { useEffect, useState } from 'react';
import Verification from '../Verification';
import { useDispatch, useSelector } from 'umi';
import { Pagination, message, Button } from 'antd';
import { RegardsState } from '@/types/models';
import {emojis} from '../EmojisFace'
import Expression from '../Expression';

interface CommentAlert {
  url: string;
  id?: string;
}

const Comment: React.FC<CommentAlert> = (props) => {
  let [visible, setVisible] = useState(false);
  let dispatch = useDispatch();
  const { IsComment } = useSelector((state: RegardsState) => state.regards);
  let [CommentValue, setCommentValue] = useState("")
  const [emoticon, setemoticon] = useState(false)

  useEffect(() => {
    console.log('111');
  }, [visible]);

  function changeComment(e: React.ChangeEvent<HTMLTextAreaElement>) {
    setCommentValue(e.target.value)
  }

  function postComment() {
    let name = localStorage.getItem("token");
    let email = localStorage.getItem("email");
    dispatch({
      type: "regards/postComment",
      payload: { content: CommentValue, email: email, name: name, hostId: Number(new Date()) * 1, url: props.url }
    })
    console.log(IsComment);
    if (JSON.stringify(IsComment) != "{}") {
      message.success("发布成功,等待管理员审核");
      setCommentValue("")
    }
  }
  function isLogin() {
    if (localStorage.getItem("token")) {
      setVisible(false)
    } else {
      setVisible(true)
    }
  }

  console.log(emojis);
  
  return (
    <>
      <div className={styles.commentIpt}>
        <div className={styles.commentIII}>
          <div className={styles.textarea}>
            <textarea
              placeholder="请输入评论内容（支持 Markdown）"
              className={styles.antInput}
              style={{
                height: '142px',
                minHeight: '142px',
                maxHeight: '274px',
                resize: 'none',
                overflowY: 'hidden',
              }}
              onChange={(e) => changeComment(e)}
              onClick={() => isLogin()}
            ></textarea>
          </div>
          <footer>
            <Expression/>
            <div>
              <button type="button" className={styles.antBtn} onClick={() => postComment()}>
                <span>发布</span>
              </button>
            </div>
          </footer>
        </div>
      </div>
      {visible ? <Verification visible={visible} setVisible={setVisible} /> : ''}
    </>
  );
};
export default Comment;
