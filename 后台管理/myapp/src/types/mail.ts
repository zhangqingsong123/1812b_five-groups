export interface MailDate{
    page:string;
    pageSize:string;
    from?:string;
    to?:string;
    subject?:string;
}
export interface MailListItem {
  id: string;
  from: string;
  to: string;
  subject: string;
  text?: any;
  html: string;
  createAt: string;
}
export interface ImgDate{
  page:number, 
  pageSize:number,
  type?:number,
  originalname?:number,
}
export interface ImgItem {
  id: string;
  originalname: string;
  filename: string;
  type: string;
  size: number;
  url: string;
  createAt: string;
}