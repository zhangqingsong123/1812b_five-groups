import { request } from 'umi';

// 获取所有文章列表
export function getArticle(
  page = 1,
  params: { [key: string]: string | boolean },
  pageSize = 12,
) {
  return request(`/api/article?page=${page}&pageSize=${pageSize}`, { params });
}

// 更改文章状态(发布或下线)
export function updateArticle(
  id: string,
  data: { [key: string]: boolean } = {},
) {
  return request(`/api/article/${id}`, {
    method: 'PATCH',
    data,
  });
}

// 删除文章
export function deleteArticle(id: string) {
  return request(`/api/article/${id}`, {
    method: 'DELETE',
  });
}
