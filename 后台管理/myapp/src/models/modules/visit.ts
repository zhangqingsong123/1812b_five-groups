import {deleteVisit, getvisitlist} from '@/services';
import {VisitList} from '@/types';
import { makeAutoObservable, runInAction } from "mobx"
import {message} from 'antd';
class Article{
    // 访问统计的表格内容
    visitlList: VisitList[] = [];
    visitCount: number = 0;
    constructor(){
        makeAutoObservable(this);
    }
    // 访问统计的表格内容
    async getvisitlist(page:number,pageSize:number){
        let result = await getvisitlist(page,pageSize);
        if (result.data){
            runInAction(()=>{
                this.visitlList = result.data[0];
                this.visitCount = result.data[1];
            })
        }
    }
    // 访问统计的删除评论
    async deleteVisit(ids:string[],page:number,pageSize:number){
        message.loading('操作中', 0);
        Promise.all(ids.map(id=>deleteVisit(id)))
        .then(res=>{
            message.destroy()
            message.success('删除成功');
            this.getvisitlist(page,pageSize);
        })
        .catch(err=>{
            message.destroy()
            message.error('删除失败');
        })
    }
}

export default Article;
