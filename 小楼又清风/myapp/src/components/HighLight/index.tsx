import hljs from 'highlight.js';
import { copyText } from '@/utils/copy';
import React, { useEffect } from 'react';
import './index.less';


const HighLight: React.FC = (props) => {
  const ref = React.createRef<HTMLDivElement>()
  useEffect(() => {
    let blocks = ref.current!.querySelectorAll("pre code")
    blocks.forEach(block => {
      hljs.highlightElement(block as HTMLElement)
      let copy = document.createElement("button")
      copy.innerHTML = "复制";
      copy.className = "copy-code"
      copy.onclick = function () {
        copyText((block as HTMLElement).innerText)
      }
      block.parentNode!.insertBefore(copy, block)

    })
  }, [props.children])
  return <div ref={ref} className="markdown">{props.children}</div>
}
export default HighLight
