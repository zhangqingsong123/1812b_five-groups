import {getsearchlist,deleteSearch} from '@/services';
import {SearchList} from '@/types';
import { makeAutoObservable, runInAction } from "mobx"
import {message} from 'antd';
class Search{
    // 搜索页面的表格内容
    searchList: SearchList[] = [];
    searchCount: number = 0;
    constructor(){
        makeAutoObservable(this);
    }
    // 搜索页面的表格内容
    async getsearchlist(page:number, params: {[key:string]:string|boolean},pageSize:number){
        let result = await getsearchlist(page,params,pageSize);
        if (result.data){
            runInAction(()=>{
                this.searchList = result.data[0];
                this.searchCount = result.data[1];
            })
        }
    }
    // 搜索页面删除评论
    async deleteSearch(ids:string[],page:number, params: {[key:string]:string|boolean},pageSize:number){
        message.loading('操作中', 0);
        Promise.all(ids.map(id=>deleteSearch(id)))
        .then(res=>{
            message.destroy()
            message.success('删除成功');
            this.getsearchlist(page,params,pageSize);
        })
        .catch(err=>{
            message.destroy()
            message.error('删除失败');
        })
    }
}

export default Search;