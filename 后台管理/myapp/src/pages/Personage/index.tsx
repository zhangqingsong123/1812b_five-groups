import React, { useState, useEffect } from 'react'
import styles from './index.less'
import { Tabs, Form, Input, Button, Avatar, Drawer, Card, Col, Row, Pagination, Upload, message } from 'antd';
const { Meta } = Card;
import { UserOutlined } from '@ant-design/icons';
import useStore from '@/context/useStore';
import { observer } from 'mobx-react-lite';

const { TabPane } = Tabs;
const { useForm } = Form;

const Personage: React.FC = () => {
    const [form] = useForm();
    const onFinish = (values: any) => {
        console.log('Success:', values);
    };
    const store = useStore();
    const [visible, setVisible] = useState(false);
    const [page, setPage] = useState(1);
    const [pageSize, setPageSize] = useState(12);
    useEffect(() => {
        store.mail.getImg({ page, pageSize })
    }, [page])
    const showDefaultDrawer = () => {
        setVisible(true);
    };
    const onFinishFailed = (errorInfo: any) => {
        console.log('Failed:', errorInfo);
    };
    function submit() {
        let values = form.getFieldsValue();
        store.mail.getImg({ page, pageSize, originalname: values.originalname, type: values.type })
    }
    console.log(store.mail.ImgList);

    const props = {
        name: 'file',
        action: 'https://www.mocky.io/v2/5cc8019d300000980a055e76',
        headers: {
            authorization: 'authorization-text',
        },
        onChange(info:any) {
            if (info.file.status !== 'uploading') {
                console.log(info.file, info.fileList);
            }
            if (info.file.status === 'done') {
                message.success(`${info.file.name} file uploaded successfully`);
            } else if (info.file.status === 'error') {
                message.error(`${info.file.name} file upload failed.`);
            }
        },
    };

    const onClose = () => {
        setVisible(false);
    };
    function onReset() {
        form.resetFields();
    };
    return (
        <div className={styles.Personage}>
            <div className={styles.PersonageHeader}>

            </div>
            <div className={styles.PersonageMain}>
                <div className={styles.PersonageLeft}>
                    <ul className={styles.PersonageLeftContent}>
                        <li>系统概全</li>
                        <li>累计发表了6篇文章</li>
                        <li>累计创建了6个分类</li>
                        <li>累计创建了4个标签</li>
                        <li>累计上传了24个文件</li>
                        <li>累计获得了6个评论</li>
                    </ul>
                </div>
                <div className={styles.PersonageRight}>
                    <div className={styles.PersonageRightHeader}>
                        个人资料
                    </div>
                    <div className={styles.PersonageRightContent}>
                        <div>
                            <Tabs defaultActiveKey="1">
                                <TabPane tab="基本设置" key="1">
                                    <div onClick={showDefaultDrawer} className="IsshowDrawer">
                                        <Avatar size={64} icon={<UserOutlined />} className={styles.Avatar} />
                                    </div>
                                    <Drawer
                                        width="786px"
                                        onClose={onClose}
                                        visible={visible}

                                    >
                                        <div className={styles.DrawerHeader}>文件选择</div>
                                        <div className={styles.DrawerBody}>
                                            <Form form={form} onFinish={submit}>
                                                <div className={styles.MailSearchTop}>
                                                    <Form.Item label="文件名" name="originalname" className={styles.MailSearchTopItem}>
                                                        <Input type="text" placeholder="请输入发件名" />
                                                    </Form.Item>
                                                    <Form.Item label="文件类" name="type" className={styles.MailSearchTopItem}>
                                                        <Input type="text" placeholder="请输入收件类" />
                                                    </Form.Item>
                                                </div>
                                                <div className={styles.MailSearchBottom}>
                                                    <Button type="primary" htmlType="submit">
                                                        搜索
                                                    </Button>
                                                    <Button htmlType="button" onClick={onReset}>
                                                        重置
                                                    </Button>
                                                </div>
                                            </Form>
                                        </div>
                                        <div className="site-card-wrapper">
                                            <Upload {...props} className={styles.Upload}>
                                                <Button className={styles.UploadButton}>上传文件</Button>
                                            </Upload>
                                            <Row gutter={8}>
                                                {
                                                    store.mail.ImgList.map(item => {
                                                        return <Col span={2} className={styles.Row_Card}>
                                                            <Card
                                                                style={{ width: 110, height: 180 }}
                                                                hoverable
                                                                cover={<img src={item.url} alt="example" />}
                                                            >
                                                                <Meta title={item.originalname} />
                                                            </Card>,
                                                    </Col>
                                                    })
                                                }
                                            </Row>
                                            <Pagination defaultCurrent={1} total={store.mail.Imgcounst - 1} onChange={e => setPage(e)} />
                                        </div>,
                                    </Drawer>
                                    <Form
                                        className={styles.PersonageFrom}
                                        name="basic"
                                        labelCol={{ span: 8 }}
                                        wrapperCol={{ span: 16 }}
                                        initialValues={{ remember: true }}
                                        onFinish={onFinish}
                                        onFinishFailed={onFinishFailed}
                                    >
                                        <Form.Item
                                            label="用户名"
                                            name="username"
                                        >
                                            <Input />
                                        </Form.Item>

                                        <Form.Item
                                            label="邮箱"
                                            name="email"
                                        >
                                            <Input />
                                        </Form.Item>
                                        <Form.Item wrapperCol={{ offset: 8, span: 16 }}>
                                            <Button type="primary" htmlType="submit">
                                                保存
                                            </Button>
                                        </Form.Item>
                                    </Form>
                                </TabPane>
                                <TabPane tab="更新密码" key="2">
                                    <Form
                                        name="basic"
                                        labelCol={{ span: 8 }}
                                        wrapperCol={{ span: 16 }}
                                        initialValues={{ remember: true }}
                                        onFinish={onFinish}
                                        onFinishFailed={onFinishFailed}
                                    >
                                        <Form.Item
                                            label="原密码"
                                            name="password"
                                            rules={[{ required: true, message: 'Please input your password!' }]}
                                        >
                                            <Input.Password />
                                        </Form.Item>
                                        <Form.Item
                                            label="新密码"
                                            name="password"
                                            rules={[{ required: true, message: 'Please input your password!' }]}
                                        >
                                            <Input.Password />
                                        </Form.Item>
                                        <Form.Item
                                            label="确认密码"
                                            name="password"
                                            rules={[{ required: true, message: 'Please input your password!' }]}
                                        >
                                            <Input.Password />
                                        </Form.Item>
                                        <Form.Item wrapperCol={{ offset: 8, span: 16 }}>
                                            <Button type="primary" htmlType="submit">
                                                更新
                                            </Button>
                                        </Form.Item>
                                    </Form>
                                </TabPane>
                            </Tabs>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default observer(Personage)
