import { defineConfig } from 'umi';

export default defineConfig({
  nodeModulesTransform: {
    type: 'none',
  },
  fastRefresh: {},
  antd: {
  },
  dva: {
    immer: true,
    hmr: true,
  },

  locale: {},
  //配置服务器上的子路径
  publicPath: process.env.NODE_ENV === 'production' ? '/1812B/zhangqingsong/fantasticit/' : '/',
  //配置路由上的子路径
  base:"/1812B/zhangqingsong/fantasticit/",
  //配置路由懒加载

  //文件名+hash后缀
  hash:true

});
