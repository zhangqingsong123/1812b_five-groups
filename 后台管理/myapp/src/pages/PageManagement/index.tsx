import React, { useEffect, useState, Key } from 'react';
import useStore from '@/context/useStore';
import { PageData } from '@/types';
import { observer } from 'mobx-react-lite';
import './index.less';
import { ReloadOutlined, PlusOutlined } from '@ant-design/icons';
import { Form, Input, Button, Row, Table, Select, Badge } from 'antd';

interface IForm {
  [key: string]: string | boolean;
}

const { useForm } = Form;
const PageManagement: React.FC = () => {
  const [form] = useForm();
  const store = useStore();
  const [page, setPage] = useState(1);
  const [pageSize, setpageSize] = useState(12);
  const [params, setParams] = useState<IForm>({});
  const [selectedRowKeys, setSelectedRowKeys] = useState<Key[]>([]);

  useEffect(() => {
    store.pageI.getPage(page, pageSize, params);
  }, [page, params, pageSize]);

  function submit() {
    let values = form.getFieldsValue();
    let params: IForm = {};
    for (let key in values) {
      values[key] && (params[key] = values[key]);
    }
    setParams(params);
  }

  const columns = [
    {
      title: '名称',
      dataIndex: 'name',
    },
    {
      title: '路径',
      dataIndex: 'path',
    },
    {
      title: '顺序',
      dataIndex: 'order',
    },
    {
      title: '阅读量',
      render: (row: PageData) => (
        <Badge
          count={row.views}
          style={{ backgroundColor: '#52c41a' }}
          showZero
        />
      ),
    },
    {
      title: '状态',
      render: (row: PageData) => {
        return row.status == 'publish' ? (
          <Badge status="success" text="已发布" />
        ) : (
          <Badge status="warning" text="草稿" />
        );
      },
    },
    {
      title: '发布时间',
      width: '200px',
      dataIndex: 'createAt',
    },
    {
      title: '操作',
      width: '308px',
      render: (row: PageData) => (
        <p>
          <Button>编辑</Button>
          <span> | </span>
          {row.status == 'publish' ? (
            <Button
              onClick={() =>
                store.pageI.updatePage(
                  [row.id],
                  {
                    // status:'publish'
                  },
                  page,
                  pageSize,
                )
              }
            >
              下线
            </Button>
          ) : (
            <Button
              onClick={() =>
                store.pageI.updatePage(
                  [row.id],
                  {
                    // status:'draft'
                  },
                  page,
                  pageSize,
                )
              }
            >
              发布
            </Button>
          )}
          <span> | </span>
          <Button>查看访问</Button>
          <span> | </span>
          <Button
            onClick={() =>
              store.pageI.deletePage([row.id], params, page, pageSize)
            }
          >
            删除
          </Button>
        </p>
      ),
    },
  ];

  function onSelectChange(selectedRowKeys: Key[], items: PageData[]) {
    setSelectedRowKeys(selectedRowKeys);
  }
  const rowSelection = {
    selectedRowKeys,
    onChange: onSelectChange,
  };

  return (
    <div className="page">
      <Form
        className="sPYrkftgglgcqyg8EHw1"
        style={{ padding: '24px 12px' }}
        onFinish={submit}
        form={form}
      >
        <Row
          style={{ marginLeft: '-12px', marginRight: ' -12px', rowGap: ' 0px' }}
        >
          <Form.Item
            label="名称："
            style={{ paddingLeft: '3%', paddingRight: '3%' }}
          >
            <Input placeholder="请输入页面名称" style={{ width: 165 }} />
          </Form.Item>
          <Form.Item
            label="路径："
            style={{ paddingLeft: '3%', paddingRight: '3%' }}
          >
            <Input placeholder="请输入页面路径" style={{ width: 165 }} />
          </Form.Item>
          <Form.Item
            label="状态："
            style={{ paddingLeft: '3%', paddingRight: '3%' }}
          >
            <Select style={{ width: 180 }}>
              <option value="">已发布</option>
              <option value="">草稿</option>
            </Select>
          </Form.Item>
        </Row>
        <Row>
          <div className="btn">
            <Button htmlType="submit">搜索</Button>
            <Button htmlType="reset">重置</Button>
          </div>
        </Row>
      </Form>

      <div style={{ background: '#fff', padding: '24px 12px' }}>
        <div className="add">
          <div className="show">
            {selectedRowKeys.length ? (
              <section>
                <Button
                  onClick={() =>
                    store.pageI.updatePage(
                      selectedRowKeys as string[],
                      {},
                      page,
                      pageSize,
                    )
                  }
                >
                  <span>发布</span>
                </Button>
                <Button
                  onClick={() =>
                    store.pageI.updatePage(
                      selectedRowKeys as string[],
                      {},
                      page,
                      pageSize,
                    )
                  }
                >
                  <span>下线</span>
                </Button>
                <Button
                  onClick={() =>
                    store.pageI.deletePage(
                      selectedRowKeys as string[],
                      params,
                      page,
                      pageSize,
                    )
                  }
                >
                  <span>删除</span>
                </Button>
              </section>
            ) : null}
          </div>
          <div>
            <Button type="primary">
              <PlusOutlined />
              <span>新建</span>
            </Button>
            <span style={{ marginLeft: '8px' }}>
              <ReloadOutlined />
            </span>
            <span style={{ marginLeft: '8px' }}>
              <ReloadOutlined />
            </span>
          </div>
        </div>
        <Table
          columns={columns}
          rowSelection={rowSelection}
          dataSource={store.pageI.pageList}
          rowKey="id"
        />
      </div>
    </div>
  );
};

export default observer(PageManagement);
