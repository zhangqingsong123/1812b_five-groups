import React, { Component } from 'react'
import styles from "./index.less"
import { useState, useEffect } from 'react'
import { NavLink, useDispatch, useSelector } from 'umi'
import { RootState } from '@/types';
import moment from 'moment';

export default function RecomMended() {
    let [page, setPage] = useState(1);
    const dispatch = useDispatch();
    const pigeonhole = useSelector((state: RootState) => state.pigeonhole)
    console.log(pigeonhole.pigeonholeList)
    let [arr, serarr] = useState(["fe", "be", "reading", "linux", "leetcode", "news"])

    useEffect(() => {
        dispatch({
            type: 'pigeonhole/getPigRecommend',
            payload: page
        })
    }, [page]);

    return <div className={styles.RightTop}>
        <h4>推荐阅读</h4>
        <div className={styles.ant}>
            {
                pigeonhole.recommend.map((item, index) => {
                    return <ul key={index}>
                        <NavLink to={`/regards/${item.id}`}>{item.title}
                            <span>{moment(item.createAt, "YYYYMMDD").fromNow()}</span>
                        </NavLink>
                    </ul>
                })
            }
        </div>
    </div>
}
