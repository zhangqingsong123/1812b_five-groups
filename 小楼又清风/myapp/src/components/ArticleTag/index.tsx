import { IRootState } from '@/types'
import classNames from 'classnames'
import React, { useEffect } from 'react'
import { NavLink, useDispatch, useSelector } from 'umi'
import styles from './index.less'

const ArticleTagList: React.FC = () => {
  let { ArticleTag } = useSelector((state: IRootState) => state.article)
  let dispatch = useDispatch()
  useEffect(() => {
    dispatch({
      type: "article/getArticleTag"
    })
  }, [])
  return <div className={classNames(styles.lowrigh)}>
    <div className={styles.articletop}>
      <span>文章标签</span>
    </div>
    <div className={styles.articlenext}>
      <ul className={styles.ul}>
        {
          ArticleTag.map(item => {
            return <li className={styles.li} key={item.id}>
              <NavLink to={`/article/tag/` + item.value}>{item.label} [{item.articleCount}]</NavLink>
            </li>
          })
        }
      </ul>
    </div>
  </div>
}

export default ArticleTagList