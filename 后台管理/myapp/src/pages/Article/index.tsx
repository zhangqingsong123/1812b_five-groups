import React, { useEffect, useState, Key } from 'react';
import useStore from '@/context/useStore';
import { Link } from 'umi';
import { ReloadOutlined, PlusOutlined } from '@ant-design/icons';
import { Form, Input, Button, Row, Table, Select, Badge, Tag } from 'antd';
import { RootObject } from '@/types';
import { observer } from 'mobx-react-lite';
import './index.less';

interface IForm {
  [key: string]: string | boolean;
}

const { useForm } = Form;
const Article: React.FC = () => {
  const [form] = useForm();
  const store = useStore();
  const [page, setPage] = useState(1);
  const [params, setParams] = useState<IForm>({});
  const [selectedRowKeys, setSelectedRowKeys] = useState<Key[]>([]);

  useEffect(() => {
    store.article.getArticle(page, params);
  }, [page, params]);

  function submit() {
    let values = form.getFieldsValue();
    let params: IForm = {};
    for (let key in values) {
      values[key] && (params[key] = values[key]);
    }
    setParams(params);
  }

  const columns: any = [
    {
      title: '标题',
      dataIndex: 'title',
      fixed: 'left',
    },
    {
      title: '状态',
      render: (row: RootObject) => {
        return row.status == 'publish' ? (
          <Badge status="success" text="已发布" />
        ) : (
          <Badge status="warning" text="草稿" />
        );
      },
    },
    {
      title: '分类',
      render: (row: RootObject) =>
        row.category ? <Tag color="green">{row.category?.label}</Tag> : '',
    },
    {
      title: '标签',
      render: (row: RootObject) =>
        row.tags ? (
          <span>
            {row.tags?.map((item, index) => {
              return (
                <Tag
                  color="geekblue"
                  key={index}
                  style={{ marginRight: '5px' }}
                >
                  {item.label}
                </Tag>
              );
            })}
          </span>
        ) : (
          ''
        ),
    },
    {
      title: '阅读量',
      render: (row: RootObject) => (
        <Badge
          showZero
          count={row.views}
          style={{ backgroundColor: '#52c41a' }}
        />
      ),
    },
    {
      title: '喜欢数',
      render: (row: RootObject) => (
        <Badge
          showZero
          count={row.likes}
          style={{ backgroundColor: ' rgb(235, 47, 150)' }}
        />
      ),
    },
    {
      title: '发布时间',
      width: '200px',
      dataIndex: 'createAt',
    },
    {
      title: '操作',
      width: '308px',
      fixed: 'right',
      render: (row: RootObject) => (
        <p>
          <Link to="/NewArticles">
            {' '}
            <Button>编辑</Button>
          </Link>

          <span> | </span>
          {row.isRecommended ? (
            <Button
              onClick={() =>
                store.article.updateArticle(
                  [row.id],
                  { isRecommended: false },
                  page,
                  params,
                )
              }
            >
              撤销推荐
            </Button>
          ) : (
            <Button
              onClick={() =>
                store.article.updateArticle(
                  [row.id],
                  { isRecommended: true },
                  page,
                  params,
                )
              }
            >
              首焦推荐
            </Button>
          )}

          <span> | </span>
          <Button>查看访问</Button>
          <span> | </span>
          <Button
            onClick={() => store.article.deleteArticle([row.id], page, params)}
          >
            删除
          </Button>
        </p>
      ),
    },
  ];

  function onSelectChange(selectedRowKeys: Key[], items: RootObject[]) {
    setSelectedRowKeys(selectedRowKeys);
  }
  const rowSelection = {
    selectedRowKeys,
    onChange: onSelectChange,
  };

  return (
    <div className="Article">
      <Form
        className="sPYrkftgglgcqyg8EHw1"
        style={{ padding: '24px 12px' }}
        onFinish={submit}
        form={form}
      >
        <Row
          style={{ marginLeft: '-12px', marginRight: ' -12px', rowGap: ' 0px' }}
        >
          <Form.Item
            name="title"
            label="标题："
            style={{ paddingLeft: '3%', paddingRight: '3%' }}
          >
            <Input placeholder="请输入文章标题" style={{ width: 165 }} />
          </Form.Item>
          <Form.Item
            name="isCommentable"
            label="状态："
            style={{ paddingLeft: '3%', paddingRight: '3%' }}
          >
            <Select style={{ width: 180 }}>
              <option value="true">已发布</option>
              <option value="false">草稿</option>
            </Select>
          </Form.Item>
          <Form.Item
            name="label"
            label="分类："
            style={{ paddingLeft: '12px', paddingRight: '12px' }}
          >
            <Select style={{ width: 180 }}>
              <option value="leetcode">leetcode</option>
              <option value="leading">前端</option>
              <option value="after">后端</option>
              <option value="one">123</option>
            </Select>
          </Form.Item>
        </Row>
        <Row>
          <div className="btn">
            <Button htmlType="submit">搜索</Button>
            <Button htmlType="reset">重置</Button>
          </div>
        </Row>
      </Form>
      <div style={{ background: '#fff', padding: '24px 12px' }}>
        <div className="add">
          <div className="show">
            {selectedRowKeys.length ? (
              <section>
                <Button>
                  <span>发布</span>
                </Button>
                <Button>
                  <span>草稿</span>
                </Button>
                <Button
                  onClick={() =>
                    store.article.updateArticle(
                      selectedRowKeys as string[],
                      {
                        isRecommended: true,
                      },
                      page,
                      params,
                    )
                  }
                >
                  <span>首焦推荐</span>
                </Button>
                <Button
                  onClick={() =>
                    store.article.updateArticle(
                      selectedRowKeys as string[],
                      {
                        isRecommended: false,
                      },
                      page,
                      params,
                    )
                  }
                >
                  <span>撤销首焦</span>
                </Button>
                <Button
                  onClick={() =>
                    store.article.deleteArticle(
                      selectedRowKeys as string[],
                      page,
                      params,
                    )
                  }
                >
                  <span>删除</span>
                </Button>
              </section>
            ) : null}
          </div>
          <div>
            <Link to="/NewArticles">
              <Button type="primary">
                <PlusOutlined />
                <span>新建</span>
              </Button>
            </Link>
            <span style={{ marginLeft: '8px' }}>
              <ReloadOutlined />
            </span>
          </div>
        </div>
        <Table
          columns={columns}
          rowSelection={rowSelection}
          dataSource={store.article.articleList}
          rowKey="id"
          scroll={{ x: 1450 }}
          pagination={{
            onChange: (page) => setPage(page),
            defaultCurrent: 1,
            showSizeChanger: true,
            total: store.article.articleCount,
          }}
        />
      </div>
    </div>
  );
};

export default observer(Article);
