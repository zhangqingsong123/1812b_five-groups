import {request} from 'umi'

export function getMailList(page:string,pageSize:string,from?:string,to?:string,subject?:string){
    return request('/api/smtp',{
        params:{
            page,
            pageSize,
            from,
            to,
            subject
        }
    })
}

export function getImg(page:number,pageSize:number,originalname?:number,type?:number){
    return request('/api/file',{
        params:{
            page,
            pageSize,
            originalname,
            type
        }
    })
}
