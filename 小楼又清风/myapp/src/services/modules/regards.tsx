import { request } from 'umi'
//内容的页面
export function getContent(id: string) {
    return request(`/api/page/${id}/views`, {
        method: "post"
    })
}
//评论数据
export function getComment(page: number, pageSize = 6, id: string) {
    return request(`/api/comment/host/${id}`, {
        params: {
            page,
            pageSize
        },
        method: "GET"
    })
}
//推荐阅读
export function getRegardsRecommend() {
    return request("/api/article/recommend")
}
//详情页面的数据
export function getRegardsDetail(id: string) {
    console.log(id, "151");
    return request(`/api/article/${id}/views`, {
        method: "POST",
    })
}
//回复和发表评论的数据
export function postComment(content: string, email: string, name: string, hostId: string, url: string, parentCommentId?: string, replyUserEmail?: string, replyUserName?: string) {
    console.log(content, email, name, hostId, url, parentCommentId, replyUserEmail, replyUserName);
    return request('/api/comment', {
        data: {
            content,
            email,
            hostId,
            name,
            url,
            parentCommentId,
            replyUserEmail,
            replyUserName
        },
        method: "POST",
    })
}
//搜索
export function getSearch(keyword: string) {
    return request(`/api/search/article?keyword=${keyword}`)
}