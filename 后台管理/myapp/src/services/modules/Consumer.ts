import {request} from 'umi'
import {ConsumerList} from '@/types'

//用户管理页面 内容列表
export function getconsumerlist( page=1 ,params:{[key: string]: string|boolean} , pageSize = 12){
    return request(`/api/user?page=${page}&pageSize=${pageSize}`, { params})
}

// 用户管理页面 （启用、禁用|授权、解除授权）
export function updateconsumer(data:ConsumerList){
    return request(`/api/user/update`,{
        method: 'POST',
        data,
    })
}
