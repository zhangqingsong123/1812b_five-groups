import React, { Key, useEffect, useState } from 'react'
import useStore from '@/context/useStore';
import { Form, Input, Button, Table, Badge } from 'antd';
import { SearchList } from '@/types';
import { observer } from 'mobx-react-lite'
import { ReloadOutlined } from '@ant-design/icons';
import moment from '../../components/Moment'
import styles from "./index.less";

interface IForm {
    [key: string]: string | boolean
}
const { useForm } = Form;
const Search: React.FC = () => {
    const [form] = useForm();
    const store = useStore();
    const [page, setPage] = useState(1);
    const [pageSize, setpageSize] = useState(12)
    const [params, setParams] = useState<IForm>({})
    const [selectedRowKeys, setSelectedRowKeys] = useState<Key[]>([]);
    const [selectedRows, setselectedRows] = useState<SearchList[]>([]);

    useEffect(() => {
        store.search.getsearchlist(page, params,pageSize)
    }, [page, params,pageSize])

    function onSelectChange(selectedRowKeys: Key[], selectedRows: SearchList[]) {
        setselectedRows(selectedRows);
    }
    const rowSelection = {
        selectedRows,
        onChange: onSelectChange
    }
    
    const onPageChange = (pageNumber: number,pagesize: number) =>{
        setPage(pageNumber)
        setpageSize(pagesize)
    }

    function submit() {
        let values = form.getFieldsValue();
        console.log(values);
        let params: IForm = {};
        for (let key in values) {
            values[key] && (params[key] = values[key]);
        }
        setParams(params);
    }

    const columns = [
        {
            title: '搜索词',
            dataIndex: 'keyword',
        },
        {
            title: '搜索量',
            render: (row: SearchList) => {
                return <Badge count={row.count} style={{ backgroundColor: "#52c41a" }}></Badge>
            }
        },
        {
            title: '搜索时间',
            render: (row: SearchList) => {
                return <span>{moment(row.updateAt).format('YYYY/MM/DD hh:mm:ss')}</span>
            }
        },
        {
            title: '操作',
            render: (row: SearchList) => {
                return <p>
                    <Button onClick={() => store.search.deleteSearch([row.id],page, params,pageSize)}>删除</Button>
                </p>
            }
        }
    ]

    return (
        <div className={styles.searchBox}>
            <div className={styles.searchbox}>
                <div className={styles.searchtop}>
                    <Form form={form} onFinish={submit}>
                        <div className={styles.searchleft}>
                            <Form.Item name="type" label="类型：" className={styles.search}>
                                <Input type="text" placeholder="请输入搜索类型" />
                            </Form.Item>
                            <Form.Item name="terms" label="搜索词：" className={styles.search}>
                                <Input type="text" placeholder="请输入搜索词" />
                            </Form.Item>
                            <Form.Item name="volume" label="搜索量：" className={styles.search}>
                                <Input type="text" placeholder="请输入搜索量" />
                            </Form.Item>
                        </div>
                        <div className={styles.searchright}>
                            <Button htmlType="submit">搜索</Button>
                            <Button htmlType="reset">重置</Button>
                        </div>
                    </Form>
                </div>
                <div className={styles.searchbottom}>
                    <div className={styles.searchbottombox}>
                        {selectedRowKeys.length ? <section>
                            <Button onClick={() => store.search.deleteSearch(selectedRowKeys as string[],page, params, pageSize)}>删除</Button>
                        </section> : null}
                        <span></span>
                        <ReloadOutlined />
                    </div>
                    <Table 
                    rowSelection={rowSelection} 
                    columns={columns} 
                    dataSource={store.search.searchList} 
                    rowKey="id"
                    pagination={{
                        total:store.search.searchCount,
                        defaultPageSize:pageSize,
                        pageSizeOptions:['8','12','24','36'], 
                        showTotal:total => `共 ${total} 条`,
                        onChange:(pageNumber,pagesize)=>{
                            onPageChange(pageNumber,pagesize!)
                        }
                    }}
                    ></Table>
                </div>
            </div>
        </div>
    )
}

export default observer(Search)
