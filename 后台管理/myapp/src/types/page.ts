export interface PageData {
  id: string;
  cover: string;
  name: string;
  path: string;
  order: number;
  content: string;
  html: string;
  toc: string;
  status: string;
  publishAt: string;
  views: number;
  createAt: string;
  updateAt: string;
}