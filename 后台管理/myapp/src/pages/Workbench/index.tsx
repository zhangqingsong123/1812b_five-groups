import Canvas from '@/components/Canvas';
import WorkbenchHeader from '@/components/WorkbenchHeader';  //导航
import React from 'react'

import styles from './index.less'

const Workbench: React.FC = () => {
    return (
        <div className={styles.canva}>
           <div><Canvas></Canvas></div> 
      <WorkbenchHeader></WorkbenchHeader>
        </div>
    )
}

export default Workbench
