import monent from 'moment'
// 时间
monent.updateLocale('en',{
    relativeTime:{
        future:"·%s",
        past:"%s",
        s:"几秒前",
        ss:'%d秒前',
        m:'几分钟前',
        mm:'%d分钟前',
        h:'几小时前',
        hh:'%d小时前',
        d:'几天前',
        dd:'%d天前',
        M:'大约%d月前',
        MM:'%d月前',
        y:'几年前',
        yy:'%d年前'
    }
})
export default monent;