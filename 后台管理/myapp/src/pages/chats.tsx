import React from 'react'
import { useEffect, useRef, useState } from 'react'
import io, { Socket } from 'socket.io-client'
import md5 from 'md5'
import styles from './chat.less'
interface IMessage {
    title?: string;
    message: string;
}
const chats: React.FC = () => {
    let [username, setUsername] = useState("")
    let [content, setcontent] = useState("")
    let [message, setmessage] = useState<IMessage[]>([])
    let [flag, setFlag] = useState(true)
    const instance = useRef<Socket>()
    useEffect(() => {
        // 建立后台连接
        instance.current = io('ws://127.0.0.1:3000');
        // Socket events
        // Whenever the server emits 'login', log the login message

        instance.current.on('login', (data) => {
            // Display the welcome message
            // const message = 'Welcome to Socket.IO Chat – ';
            setmessage(messages => [...messages, {
                message: `
            Welcome to Socket.IO Chat – there s ${data} participant
            `}])
            // setMessages(messages=>[...messages, {
            //     message: `Welcome to Socket.IO Chat – `
            // },{
            //     message: `there's ${data.numUsers} participant`
            // }])
        });

        // Whenever the server emits 'new message', update the chat body
        instance.current.on('new message', (data) => {
            setmessage(messages => [...messages, {
                title: data.username,
                message: data.message,
            }])
            // setMessages(messages=>[...messages, {
            //     title: data.username,
            //     message: data.message
            // }])
        });

        // Whenever the server emits 'user joined', log it in the chat body
        instance.current.on('user joined', (data) => {
            // setMessages(messages=>[...messages, {
            //     message: `${data.username} joined`
            // }, { 
            //     message: `there's ${data.numUsers} participant`
            // }])
        });

        // Whenever the server emits 'user left', log it in the chat body
        instance.current.on('user left', (data) => {
            // setMessages(messages=>[...messages, {
            //     message: `${data.username} left`
            // }])
        });

        // Whenever the server emits 'typing', show the typing message
        instance.current.on('typing', (data) => {
            // setMessages(messages=>[...messages, {
            //     title: data.username,
            //     message: `is typing`
            // }])
        });
        // Whenever the server emits 'stop typing', kill the typing message
        instance.current.on('stop typing', (data) => {
            // setMessages(messages=>{
            //     let index= messages.findIndex(item=>{
            //         return (item.title === data.username) && (item.message === 'is typing');
            //     })
            //     if (index > -1) {
            //         messages.splice(index, 1);
            //     }
            //     return [...messages];
            // });
        });

        instance.current.on('disconnect', () => {
            // setMessages(messages=>[...messages, {
            //     message: 'you have been disconnected'
            // }])
        });

        instance.current.on('reconnect', () => {
            // setMessages(messages=>[...messages, {
            //     message: 'you have been reconnected'
            // }])
            // if (username) {
            //     instance.current!.emit('add user', username);
            // }
        });

        instance.current.on('reconnect_error', () => {
            // setMessages(messages=>[...messages, {
            //     message: 'attempt to reconnect has failed'
            // }])
        });
    }, [])
    //加入聊天室
    function joinChatRoom(e: React.KeyboardEvent) {
        if (e.keyCode == 13 && username) {
            setFlag(false);
            instance.current!.emit("add user", username);
        }
    }
    //用户发送信息
    function fafafa(e: React.KeyboardEvent) {
        if (e.keyCode == 13 && username) {
          setmessage(message=>[
              ...message,{
                  title:username,
                  message:content
              }
          ])     
            
        }
        instance.current!.emit("new message", content);
        setcontent("")
    }
    return <div className={styles.wrap}>
        <p>聊天页面</p>
        {

            flag ? <div>
                <p>What`s your nickname?</p>
                <input type="text" value={username}
                    onChange={e => setUsername(e.target.value)}
                    onKeyDown={joinChatRoom}
                />
            </div> : <div>
                <div>
                    {
                        message.map((item, index) => {
                            return <p>
                                {item.title && <span key={index}>{item.title}</span>}
                                <span>{item.message}</span>
                            </p>
                        })
                    }
                </div>
                {/* 用户发消息 */}
                <input
                    placeholder="Typeing here..."
                    type="text" value={content}
                    onChange={e => setcontent(e.target.value)}
                    onKeyDown={fafafa}
                />
            </div>
        }
    </div>
}
export default chats