import React, { useEffect, useState } from 'react'
import styles from "./index.less"
import { Empty, Button, Card, Popconfirm, message } from "antd"
import { EditOutlined, CloudUploadOutlined, CloudDownloadOutlined, DeleteOutlined, SettingOutlined } from '@ant-design/icons';
import useStore from '@/context/useStore';
import { observer } from 'mobx-react-lite';

const { Meta } = Card;

const Knowledge: React.FC = () => {

    const store = useStore()
    const [page, setPage] = useState(1)
    const [pageSize, setPageSize] = useState(12)
    const [uploadFlag, setUIploadFlag] = useState(false)
    const [params, setParams] = useState({});

    useEffect(() => {
        store.knowledge.getKnowledgeList(page, pageSize, params)
    }, [params])

    return (
        <div className={styles.searchBox}>
            <div className={styles.searchbox}>
                <div className={styles.searchtop}>
                    <div className={styles.searchleft}>
                        <div>
                            <span>名称 :</span>
                            <input type="text" placeholder="请输入知识库名称" />
                        </div>
                        <div>
                            <span>状态 :</span>
                            <select>
                                <option value="volvo"></option>
                                <option value="已发布">已发布</option>
                                <option value="草稿">草稿</option>
                            </select>
                        </div>
                    </div>
                    <div className={styles.searchright}>
                        <button>搜索</button>
                        <button>重置</button>
                    </div>

                </div>
                <div className={styles.searchbottom}>
                    {
                        store.knowledge.knowledgeList ?
                            <div className={styles.card_box}>
                                {
                                    store.knowledge.knowledgeList.map((item => {
                                        return <div className={styles.card_content}>
                                            <Card
                                                cover={<img alt="example" src={item.cover} />}
                                                actions={[
                                                    <EditOutlined key="edit" />,
                                                    uploadFlag ? <CloudUploadOutlined /> : <CloudDownloadOutlined />,
                                                    <SettingOutlined key="setting" />,
                                                    <Popconfirm
                                                        title="确认删除？"
                                                        onConfirm={() => { store.knowledge.deleteKnowledgeCard([item.id]) }}
                                                        onCancel={() => { message.error('取消删除') }}
                                                        okText="确认"
                                                        cancelText="取消"
                                                    >
                                                        <DeleteOutlined />
                                                    </Popconfirm>

                                                ]}
                                            >
                                                <Meta title={item.title} description={item.summary} />
                                            </Card>
                                        </div>
                                    }))
                                }
                            </div> : <Empty image={Empty.PRESENTED_IMAGE_SIMPLE} />
                    }
                </div>
            </div>
        </div>
    )
}

export default observer(Knowledge)
