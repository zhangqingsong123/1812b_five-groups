
import { essaystate } from '@/types';
import React, { useEffect } from 'react'
import { NavLink ,useDispatch,useSelector} from 'umi'
import styles from './category.less'; // 等于启用了css-module
const classNames = require('classnames');

const Category: React.FC = () => {
    let dispatch =useDispatch()
    let {ArticleLabel}=useSelector((state:essaystate)=>state.category)
   
    
    useEffect(() => {
      dispatch({
          type: 'category/getArticleLabel'
      }) 
    }, [])
    return (
        <div className={classNames(styles.categorylist)}>
            <div className="header">
           <div className={styles.oder}> <span>文章分类</span></div>
               {
                ArticleLabel.map(item=>{
                       return <p className={styles.acti}>
                            <NavLink key={item.id} to={`/article/category/`+item.value}>
                          
                          {item.label}</NavLink> <span>共计{item.articleCount}篇文章</span>
                       </p>
                   })
               }
            </div>
        </div>
    )
}
export default Category
