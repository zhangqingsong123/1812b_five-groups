import { essaystate } from '@/types';
import React, { useEffect } from 'react'
import { NavLink, useDispatch, useSelector } from 'umi'
import styles from './category.less'; // 等于启用了css-module


const Category: React.FC = () => {
    let dispatch = useDispatch()
    let { ArticleLabel } = useSelector((state: essaystate) => state.category)


    useEffect(() => {
        dispatch({
            type: 'category/getArticleLabel'
        })
    }, [])
    return (
        <div className={styles.category}>
            <div className="head">
                <NavLink to={`/article`}>所有</NavLink>
                {
                    ArticleLabel.map(item => {
                        return <NavLink key={item.id} to={`/article/category/` + item.value}>

                            {item.label}</NavLink>
                    })
                }
            </div>
        </div>
    )
}
export default Category
