import React, { useEffect } from 'react'
import Viewer from 'viewerjs'
import 'viewerjs/dist/viewer.css'
const ImageView: React.FC = (props) => {
    let dom = React.createRef<HTMLDivElement>()
    useEffect(() => {
        let viewer = new Viewer(dom.current!)
        let ob = new MutationObserver(() => {
            viewer.update()
        })

        ob.observe(dom.current!, {
            attributes: true,
            childList: true,
            subtree: true,
        })
        return ()=>{
            viewer.destroy()
            ob.disconnect()
        }
    }, [])


    return <div ref={dom}>{props.children}</div>
}
export default ImageView