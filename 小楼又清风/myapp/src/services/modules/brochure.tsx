import { request } from 'umi'

export function getrecommend() {
    return request('/api/article/recommend');
}
export function getBrochureList(page: number, pageSize = 12, status = 'publish') {
    return request('/api/knowledge', {
        params: {
            page,
            pageSize,
            status,
        },
    })
}
export function getRecommendRead(articleStatus = 'publish') {
    return request('/api/category', {
        params: {
            articleStatus
        },
    })
}

export function getBrochureDetail(page: number, pageSize = 6, status = 'publish') {
    return request('/api/knowledge', {
        params: {
            page,
            pageSize,
            status,
        }
    })
}
export function getBrochureLeftDetailList(id:string) {
    return request(`/api/knowledge/${id}`, {
    })
}


// 第二层跳详情
export function getKonwledgeDetail(id:string){
    return request(`/api/knowledge/${id}/views`,{
        params: {
            id,
        },
        method: 'POST',
    })
}
