export interface SearchList{
    id: string;
    type: string;
    keyword: string;
    count: number;
    createAt: string;
    updateAt: string;
}

