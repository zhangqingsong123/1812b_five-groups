import { getArticle, deleteArticle, updateArticle } from '@/services';
import { RootObject } from '@/types';
import { makeAutoObservable, runInAction } from 'mobx';
import { message } from 'antd';

class Article {
  articleList: RootObject[] = [];
  articleCount: number = 0;
  constructor() {
    makeAutoObservable(this);
  }

  // 分页获取所有文章
  async getArticle(page: number, params: { [key: string]: string | boolean }) {
    let result = await getArticle(page, params);
    if (result.data) {
      runInAction(() => {
        this.articleList = result.data[0];
        this.articleCount = result.data[1];
      });
    }
  }

  // 批量更改文章状态
  async updateArticle(
    ids: string[],
    status: { [key: string]: boolean },
    page: number,
    params: { [key: string]: string | boolean },
  ) {
    message.loading('操作中', 0);
    Promise.all(ids.map((id) => updateArticle(id, status)))
      .then((res) => {
        message.destroy();
        message.success('操作成功');
        this.getArticle(page, params);
      })
      .catch((err) => {
        message.destroy();
        message.error('操作失败');
      });
  }

  // 批量删除文章
  async deleteArticle(
    ids: string[],
    page: number,
    params: { [key: string]: string | boolean },
  ) {
    message.loading('操作中', 0);
    Promise.all(ids.map((id) => deleteArticle(id)))
      .then((res) => {
        message.destroy();
        message.success('删除成功');
        this.getArticle(page, params);
      })
      .catch((err) => {
        message.destroy();
        message.error('删除失败');
      });
  }
}

export default Article;
