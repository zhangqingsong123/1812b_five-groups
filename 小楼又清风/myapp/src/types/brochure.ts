export interface IBrochureItem {
  id: string;
  parentId?: any;
  order: number;
  title: string;
  cover: string;
  summary: string;
  content?: any;
  html?: any;
  toc?: any;
  status: string;
  views: number;
  likes: number;
  isCommentable: boolean;
  publishAt: string;
  createAt: string;
  updateAt: string;
}


export interface IRecommendRead {
  id: string;
  label: string;
  value: string;
  createAt: string;
  updateAt: string;
  articleCount: number;
}


// 左侧跳详情
export interface LeftDetail {
  id: string;
  parentId?: any;
  order: number;
  title: string;
  cover: string;
  summary: string;
  content?: any;
  html?: any;
  toc?: any;
  status: string;
  views: number;
  likes: number;
  isCommentable: boolean;
  publishAt: string;
  createAt: string;
  updateAt: string;
  children: Child[];
}

interface Child {
  id: string;
  parentId: string;
  order: number;
  title: string;
  cover?: any;
  summary?: any;
  content: string;
  html: string;
  toc: string;
  status: string;
  views: number;
  likes: number;
  isCommentable: boolean;
  publishAt: string;
  createAt: string;
  updateAt: string;
}

export interface BrochureDetail {
  id: string;
  parentId?: any;
  order: number;
  title: string;
  cover: string;
  summary: string;
  content?: any;
  html?: any;
  toc?: any;
  status: string;
  views: number;
  likes: number;
  isCommentable: boolean;
  publishAt: string;
  createAt: string;
  updateAt: string;
}



export interface BrochureLeftDetail {
  id: string;
  parentId?: any;
  order: number;
  title: string;
  cover: string;
  summary: string;
  content?: any;
  html?: any;
  toc?: any;
  status: string;
  views: number;
  likes: number;
  isCommentable: boolean;
  publishAt: string;
  createAt: string;
  updateAt: string;
  children: Child[];
}

export interface BrochureLeftDetailBottom{
  id: string;
  parentId: string;
  order: number;
  title: string;
  cover?: any;
  summary?: any;
  content: string;
  html: string;
  toc: string;
  status: string;
  views: number;
  likes: number;
  isCommentable: boolean;
  publishAt: string;
  createAt: string;
  updateAt: string;
}


export interface KonwledgeDetail {
  id: string;
  parentId: string;
  order: number;
  title: string;
  cover?: any;
  summary?: any;
  content: string;
  html: string;
  toc: string;
  status: string;
  views: number;
  likes: number;
  isCommentable: boolean;
  publishAt: string;
  createAt: string;
  updateAt: string;
}