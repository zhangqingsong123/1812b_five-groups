import { request } from 'umi';

//获取评论列表
export function getMessageList({ page, pageSize = 6, id }: {page: number; pageSize?: number;id: string;}) {
  return request(`/api/comment/host/${id}`, {
    //表示请求地址栏上面的查询参数
    params: {
      page,
      pageSize,
    },
    //表示请求体里面传递的数据
    data: {},
  });
}

//获取推荐阅读列表
export function getrecommendReadList() {
  return request('/api/article/recommend');
}
