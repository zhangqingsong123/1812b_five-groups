import React from 'react'
import { BackTop } from 'antd';
// 回到顶部
export default function Backtop() {
    return (
        <div>
            <BackTop />
            <strong className="site-back-top-basic"></strong>
        </div>
    )
}

