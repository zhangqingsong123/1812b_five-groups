import useStore from '@/context/useStore';
import { commentitemList, commentsItem } from '@/types';
import { Table, Badge, Popover, Button ,Modal,Input} from 'antd';
import { List, Typography, Divider } from 'antd';
import React, { useEffect, useState } from 'react'
import {observer} from 'mobx-react-lite'
import styles from './index.less'

const Tables: React.FC = () => {
    let [page,setPage]=useState()
    const [isModalVisible, setIsModalVisible] = useState(false);
    const [reyValue,SetReValue]=useState("")
    let store = useStore()
    useEffect(() => {
        store.Workbench.commentsItemList(page)
        
    }, [page])

    function showModal(){
        setIsModalVisible(true);
    }
    const handleOk = () => {
        setIsModalVisible(false);
      };
    
      const handleCancel = () => {
        setIsModalVisible(false);
      };
    return (
        <div>
            <List

              
                dataSource={store.Workbench.commentsItem}
                renderItem={item => (
                    <List.Item>
                        <Typography.Text>{item.name}在 <Popover content={<iframe src={'https://creation.shbwyz.com' + item.url}></iframe>} title="页面预览">
                            <span>文章</span>
                        </Popover>评论 <Popover content={item.content} title="评论详情-原始内容"><span>查看内容</span></Popover> {item.pass ? <Badge status="success" text="通过" /> : <Badge status="warning" text="未通过" />}</Typography.Text>
                        <div>
                            <Button className={styles.s} key="list-loadmore-edit" onClick={() => store.Workbench.Updatecomment([item.id], { pass: true })}>通过</Button>
                            <Button className={styles.s} key="list-loadmore-more" onClick={() => store.Workbench.Updatecomment([item.id], { pass: false })}>拒绝</Button>
                            <Button className={styles.s} key="list-loadmore-more" onClick={showModal}>回复</Button>
                            <Button className={styles.s} key="list-loadmore-more" onClick={() => store.Workbench.DeleteComment([item.id])}>删除</Button>
                        </div>
                    </List.Item>
                )}
            />
            <Modal
             title="回复评论"
             visible={isModalVisible}
             onOk={handleOk} 
             onCancel={handleCancel}
             >
               <Input.TextArea 
               className={styles.inputtext} 
               placeholder="支持markdown" 
               value={reyValue} 
               onChange={(e)=>SetReValue(e.target.value)}>
                   
               </Input.TextArea>
            </Modal>
        </div>
    )

}

export default observer(Tables) 
