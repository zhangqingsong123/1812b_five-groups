import styles from './index.less';
import React, { useEffect, useState } from 'react';
import * as echarts from 'echarts';//引入echarts
import "./index.css"

export default function IndexPage() {
    const [option, setoption] = useState({
        title: {
            text: '每周访问指标',
            
        },
        xAxis: {
            type: 'category',
            data: ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun']//Y轴文本
        },
        yAxis: {
            type: 'value'
        },
        legend: {
            data: ['预购队列', '最新成交价']
        },
        series: [{
            data: [100, 150, 200, 250, 300, 350, 400],//数据
            type: 'bar',//柱形图
            backgroundStyle: {
                color: "red"
            },
            name: '预购队列',

        }, {
            data: [150, 230, 224, 218, 135, 147, 260],//数据
            type: 'line',//line===折线
            borderLine: "red",
            name: '最新成交价',
        }]

    })

    useEffect(function () {
        let dom = document.getElementById('divs')
        // 绑定插入的元素标签
        var myChart = echarts.init(dom);
        // 添加数据
        myChart.setOption(option);
    }, [])
    return (
        <div className={styles.container}>
            <div style={{ width: '100%', height: '100%' }}>
                <h1 className={styles.title}>面板导航</h1>
                <div id="divs" style={{ width: '1260px', height: '250px' }}></div>
            </div>
        </div>
    );
}
