import { Spin } from 'antd'
import styles from './index.less'
import React from 'react'

const Loading = () => {
    return <div className={styles.loading}><Spin size="large"/></div>    
}

export default Loading