import { makeAutoObservable, runInAction } from 'mobx'
import { getMailList, getImg } from '@/services/modules/Mail'
import { MailDate, MailListItem, ImgDate, ImgItem } from '@/types/mail'

class Mail {
    MailList: Array<MailListItem> = []
    ImgList:Array<ImgItem>= []
    Imgcounst: number = 0
    count: number = 0
    constructor() {
        makeAutoObservable(this)
    }
    async getMailList(date: MailDate) {
        let result = await getMailList(date.page, date.pageSize, date.from, date.to, date.subject)
        if (result.data) {
            runInAction(() => {
                this.MailList = result.data[0];
                this.count = result.data[1]
            })
        }
    }
    async getImg(date:ImgDate) {
        let result = await getImg(date.page, date.pageSize,date.originalname,date.type);
        console.log(result.data[0]);
        if (result.data) {
            runInAction(() => {
                this.ImgList = result.data[0];
                this.Imgcounst = result.data[1];
            })
        }
        console.log(this.ImgList,this.Imgcounst);
    }
}   

export default Mail