import { request } from 'umi';

//登录
export function Login(name: string, password: string) {
  console.log(name, password);

  return request('/api/auth/login', {
    method: 'POST',
    data: {
      name,
      password,
    },
  });
}

export function Sign(name: string, password: string, confirm: string) {
  console.log(name, password, confirm);
  return request('/api/user/register', {
    method: 'POST',
    data: {
      name,
      password,
      confirm,
    },
  });
}
